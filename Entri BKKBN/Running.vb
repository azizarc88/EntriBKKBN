﻿Public Class Running
    Dim first As Boolean = True
    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_NOACTIVATE As Integer = &H8000000
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Const WS_EX_WINDOWEDGE As Integer = &H100
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_NOACTIVATE Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST Or WS_EX_WINDOWEDGE
            Return Result
        End Get
    End Property

    Public Sub LoadInfo(teks As String)
        Threading.Thread.CurrentThread.Priority = Threading.ThreadPriority.Highest
        Application.DoEvents()
        LCur.Text = teks
        Application.DoEvents()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BKeluar.Click
        Try
            FUtama.trmainkanmusikeror.Abort()
        Catch ex As Exception
            FUtama.trmainkanmusikeror = New Threading.Thread(AddressOf FUtama.MainkanMusikEror)
            FUtama.trmainkanmusikeror.Abort()
        End Try

        Try
            FUtama.tr.Abort()
            FUtama.tfind.Abort()
        Catch ex As Exception
            FUtama.tr.Resume()
            FUtama.tr.Abort()

            FUtama.tfind.Resume()
            FUtama.tfind.Abort()
        End Try

        Try
            FUtama.tfind.Abort()
        Catch ex As Exception
            FUtama.tfind = New Threading.Thread(AddressOf FUtama.findwindow)
        End Try
        FUtama.Close()
    End Sub

    Private Sub Running_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Location = New Point(My.Computer.Screen.WorkingArea.Width - Me.Size.Width, My.Computer.Screen.WorkingArea.Height - Me.Size.Height)
        Me.BackColor = Color.FromArgb(-15784898)
        ProgressBar1.Maximum = FUtama.REF.Count * 10
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles BLanjutkan.Click
        FUtama.tfind.Resume()
        FUtama.tr.Resume()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles BHentikan.Click
        Try
            FUtama.trmainkanmusikeror.Abort()
        Catch ex As Exception
        End Try
        Try
            FUtama.tfind.Suspend()
            FUtama.tr.Suspend()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub IntegerInput1_ValueChanged(sender As Object, e As EventArgs) Handles TBKecepatan.ValueChanged
        If first = False Then
            FUtama.interpal = TBKecepatan.Value
        End If
    End Sub

    Private Sub Running_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        first = False
    End Sub

    Private Sub BKembali_Click(sender As Object, e As EventArgs) Handles BKembali.Click
        Try
            Form2.CBKendali.SelectedIndex = (LPosisi.Text.Substring(LPosisi.Text.LastIndexOfAny("-") + 2, (LPosisi.Text.LastIndexOfAny(",") - 4) - LPosisi.Text.LastIndexOfAny("-") + 2)) - 1
        Catch ex As Exception
        End Try

        FUtama.Timer1.Enabled = False
        Try
            FUtama.tr.Abort()
            FUtama.trmainkanmusikeror.Abort()
        Catch ex As Exception
            FUtama.tr.Resume()
            FUtama.tr.Abort()

            FUtama.trmainkanmusikeror = New Threading.Thread(AddressOf FUtama.MainkanMusikEror)
            FUtama.trmainkanmusikeror.Abort()
        End Try

        Try
            FUtama.tfind.Abort()
        Catch ex As Exception
            FUtama.tfind = New Threading.Thread(AddressOf FUtama.findwindow)
        End Try
        My.Computer.Audio.Stop()
        FUtama.Show()
        FUtama.Focus()
        Me.Close()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        TBKecepatan.Value = 10
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        TBKecepatan.Value = 500
    End Sub

    Private Sub LKendali_Click(sender As Object, e As EventArgs) Handles LKendali.Click
        Clipboard.SetText(LKendali.Text)
    End Sub
End Class