﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Running
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TBKecepatan = New System.Windows.Forms.NumericUpDown()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.BHentikan = New System.Windows.Forms.Button()
        Me.BLanjutkan = New System.Windows.Forms.Button()
        Me.BKembali = New System.Windows.Forms.Button()
        Me.BKeluar = New System.Windows.Forms.Button()
        Me.LHook = New System.Windows.Forms.Label()
        Me.LKendali = New System.Windows.Forms.Label()
        Me.LPosisi = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LCur = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        CType(Me.TBKecepatan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TBKecepatan
        '
        Me.TBKecepatan.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.TBKecepatan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBKecepatan.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.TBKecepatan.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.TBKecepatan.Location = New System.Drawing.Point(70, 94)
        Me.TBKecepatan.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.TBKecepatan.Name = "TBKecepatan"
        Me.TBKecepatan.Size = New System.Drawing.Size(57, 20)
        Me.TBKecepatan.TabIndex = 3
        Me.TBKecepatan.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TBKecepatan.ThousandsSeparator = True
        Me.TBKecepatan.Value = New Decimal(New Integer() {150, 0, 0, 0})
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.Button5.Location = New System.Drawing.Point(134, 89)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(52, 27)
        Me.Button5.TabIndex = 1
        Me.Button5.Text = "Lambat"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.Button4.Location = New System.Drawing.Point(192, 89)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(46, 27)
        Me.Button4.TabIndex = 1
        Me.Button4.Text = "Cepat"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'BHentikan
        '
        Me.BHentikan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BHentikan.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.BHentikan.Location = New System.Drawing.Point(244, 89)
        Me.BHentikan.Name = "BHentikan"
        Me.BHentikan.Size = New System.Drawing.Size(66, 27)
        Me.BHentikan.TabIndex = 1
        Me.BHentikan.Text = "Hentikan"
        Me.BHentikan.UseVisualStyleBackColor = True
        '
        'BLanjutkan
        '
        Me.BLanjutkan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BLanjutkan.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.BLanjutkan.Location = New System.Drawing.Point(244, 56)
        Me.BLanjutkan.Name = "BLanjutkan"
        Me.BLanjutkan.Size = New System.Drawing.Size(66, 27)
        Me.BLanjutkan.TabIndex = 1
        Me.BLanjutkan.Text = "Lanjutkan"
        Me.BLanjutkan.UseVisualStyleBackColor = True
        '
        'BKembali
        '
        Me.BKembali.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BKembali.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.BKembali.Location = New System.Drawing.Point(316, 56)
        Me.BKembali.Name = "BKembali"
        Me.BKembali.Size = New System.Drawing.Size(55, 27)
        Me.BKembali.TabIndex = 1
        Me.BKembali.Text = "Kembali"
        Me.BKembali.UseVisualStyleBackColor = True
        '
        'BKeluar
        '
        Me.BKeluar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BKeluar.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.BKeluar.Location = New System.Drawing.Point(316, 89)
        Me.BKeluar.Name = "BKeluar"
        Me.BKeluar.Size = New System.Drawing.Size(55, 27)
        Me.BKeluar.TabIndex = 1
        Me.BKeluar.Text = "Keluar"
        Me.BKeluar.UseVisualStyleBackColor = True
        '
        'LHook
        '
        Me.LHook.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.LHook.Location = New System.Drawing.Point(279, 28)
        Me.LHook.Margin = New System.Windows.Forms.Padding(4)
        Me.LHook.Name = "LHook"
        Me.LHook.Size = New System.Drawing.Size(95, 14)
        Me.LHook.TabIndex = 0
        Me.LHook.Text = "Posisi :"
        Me.LHook.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LKendali
        '
        Me.LKendali.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.LKendali.Location = New System.Drawing.Point(76, 28)
        Me.LKendali.Margin = New System.Windows.Forms.Padding(4)
        Me.LKendali.Name = "LKendali"
        Me.LKendali.Size = New System.Drawing.Size(83, 14)
        Me.LKendali.TabIndex = 0
        Me.LKendali.Text = "Posisi :"
        Me.LKendali.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LPosisi
        '
        Me.LPosisi.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.LPosisi.Location = New System.Drawing.Point(76, 6)
        Me.LPosisi.Margin = New System.Windows.Forms.Padding(4)
        Me.LPosisi.Name = "LPosisi"
        Me.LPosisi.Size = New System.Drawing.Size(86, 14)
        Me.LPosisi.TabIndex = 0
        Me.LPosisi.Text = "Posisi :"
        Me.LPosisi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(206, 28)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 14)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Hook :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(3, 28)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "No Kendali :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(3, 6)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 14)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Posisi :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LCur
        '
        Me.LCur.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.LCur.Location = New System.Drawing.Point(170, 6)
        Me.LCur.Margin = New System.Windows.Forms.Padding(4)
        Me.LCur.Name = "LCur"
        Me.LCur.Size = New System.Drawing.Size(204, 14)
        Me.LCur.TabIndex = 0
        Me.LCur.Text = "hh"
        Me.LCur.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(3, 96)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Kecepatan"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(93, Byte), Integer))
        Me.ProgressBar1.Location = New System.Drawing.Point(6, 58)
        Me.ProgressBar1.MarqueeAnimationSpeed = 1000
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(232, 21)
        Me.ProgressBar1.TabIndex = 4
        '
        'Running
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(377, 122)
        Me.ControlBox = False
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.TBKecepatan)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BHentikan)
        Me.Controls.Add(Me.LCur)
        Me.Controls.Add(Me.BLanjutkan)
        Me.Controls.Add(Me.BKembali)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.BKeluar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.LHook)
        Me.Controls.Add(Me.LPosisi)
        Me.Controls.Add(Me.LKendali)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Running"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.TopMost = True
        CType(Me.TBKecepatan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BKeluar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents BLanjutkan As Button
    Friend WithEvents BHentikan As Button
    Friend WithEvents LCur As Label
    Friend WithEvents LPosisi As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents LKendali As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents BKembali As Button
    Friend WithEvents LHook As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Button5 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents TBKecepatan As NumericUpDown
    Friend WithEvents ProgressBar1 As ProgressBar
End Class
