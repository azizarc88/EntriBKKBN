﻿Imports System.ComponentModel
Imports System.Net
Imports Transitions
Imports MouseKeyboardLibrary
Imports MessageHandle
Imports System.IO

Public Class FUtama
    Public AlamatAppData As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Izarc Software\Entri Data BKKBN"
    Dim debugup As String
    Dim onbersihkan As Boolean

    Private WithEvents keyboardHook As New KeyboardHook

    Public REF As List(Of String) = New List(Of String)
    Dim KREF As List(Of String) = New List(Of String)
    Dim NR As List(Of String) = New List(Of String)
    Dim NK As List(Of String) = New List(Of String)

    Dim NIK1 As List(Of String) = New List(Of String)
    Dim NAMA1 As List(Of String) = New List(Of String)
    Dim TL1 As List(Of String) = New List(Of String)
    Dim KD1 As List(Of String) = New List(Of String)

    Dim NIK2 As List(Of String) = New List(Of String)
    Dim NAMA2 As List(Of String) = New List(Of String)
    Dim TL2 As List(Of String) = New List(Of String)
    Dim KD2 As List(Of String) = New List(Of String)

    Dim NIK3 As List(Of String) = New List(Of String)
    Dim NAMA3 As List(Of String) = New List(Of String)
    Dim TL3 As List(Of String) = New List(Of String)
    Dim KD3 As List(Of String) = New List(Of String)

    Dim NIK4 As List(Of String) = New List(Of String)
    Dim NAMA4 As List(Of String) = New List(Of String)
    Dim TL4 As List(Of String) = New List(Of String)
    Dim KD4 As List(Of String) = New List(Of String)

    Dim NIK5 As List(Of String) = New List(Of String)
    Dim NAMA5 As List(Of String) = New List(Of String)
    Dim TL5 As List(Of String) = New List(Of String)
    Dim KD5 As List(Of String) = New List(Of String)

    Dim NIK6 As List(Of String) = New List(Of String)
    Dim NAMA6 As List(Of String) = New List(Of String)
    Dim TL6 As List(Of String) = New List(Of String)
    Dim KD6 As List(Of String) = New List(Of String)

    Dim NIK7 As List(Of String) = New List(Of String)
    Dim NAMA7 As List(Of String) = New List(Of String)
    Dim TL7 As List(Of String) = New List(Of String)
    Dim KD7 As List(Of String) = New List(Of String)

    Dim UsiaKawinSuami As List(Of String) = New List(Of String)
    Dim UsiaKawinIstri As List(Of String) = New List(Of String)
    Dim JumlahAnak As List(Of String) = New List(Of String)
    Dim BerKB As List(Of String) = New List(Of String)
    Dim MetodKontrasepsi As List(Of String) = New List(Of String)
    Dim LamaTKB As List(Of String) = New List(Of String)
    Dim LamaBKB As List(Of String) = New List(Of String)
    Dim PnyaAnakLagi As List(Of String) = New List(Of String)
    Dim AlasanTidakKB As List(Of String) = New List(Of String)
    Dim TmpatLayananKB As List(Of String) = New List(Of String)
    Dim KodePembangunan As List(Of String) = New List(Of String)
    Dim Luas As List(Of String) = New List(Of String)
    Dim Orang As List(Of String) = New List(Of String)
    Public a As Integer = 0
    Public alamatdata As New IO.DirectoryInfo(AlamatAppData)
    Public namafile As String = "Data Keluarga - Bundle 1.dft"
    Public mulaiw As DateTime

    Public Sub TransText(control As Control, teks As String, Optional lama As Integer = 1000)
        Dim t As Transition = New Transition(New TransitionType_Linear(lama))
        t.add(control, "Text", teks)
        t.run()
    End Sub

    Private Sub Bersihkan()
        REF.Clear()
        KREF.Clear()
        NR.Clear()
        NK.Clear()

        NIK1.Clear()
        NAMA1.Clear()
        TL1.Clear()
        KD1.Clear()

        NIK2.Clear()
        NAMA2.Clear()
        TL2.Clear()
        KD2.Clear()

        NIK3.Clear()
        NAMA3.Clear()
        TL3.Clear()
        KD3.Clear()

        NIK4.Clear()
        NAMA4.Clear()
        TL4.Clear()
        KD4.Clear()

        NIK5.Clear()
        NAMA5.Clear()
        TL5.Clear()
        KD5.Clear()

        NIK6.Clear()
        NAMA6.Clear()
        TL6.Clear()
        KD6.Clear()

        NIK7.Clear()
        NAMA7.Clear()
        TL7.Clear()
        KD7.Clear()

        UsiaKawinSuami.Clear()
        UsiaKawinIstri.Clear()
        JumlahAnak.Clear()
        BerKB.Clear()
        MetodKontrasepsi.Clear()
        LamaTKB.Clear()
        LamaBKB.Clear()
        PnyaAnakLagi.Clear()
        AlasanTidakKB.Clear()
        TmpatLayananKB.Clear()
        KodePembangunan.Clear()
        Luas.Clear()
        Orang.Clear()
    End Sub

    Dim baruin As Boolean = False
    Dim ls1 As New List(Of TextBox)
    Dim ls2 As New List(Of TextBox)
    Dim ls3 As New List(Of TextBox)
    Dim ls4 As New List(Of TextBox)
    Dim ls5 As New List(Of TextBox)

    Dim lsni As New List(Of TextBox)
    Dim lsn As New List(Of TextBox)
    Dim lst As New List(Of TextBox)
    Dim lsk As New List(Of TextBox)

    Private Sub PindahPosisi(ByVal tombol As Keys, ByVal posisi As Integer)
        Dim data As String()
        Dim tdata As String()
        Dim tdata2 As String()
        ReDim Preserve data(9)
        ReDim Preserve tdata(9)
        ReDim Preserve tdata2(9)

        If tombol = Keys.Up And posisi <> 6 Then
            LagiAturIsi = True
            tdata2 = {ls1(posisi).Text, ls2(posisi).Text, ls3(posisi).Text, ls4(posisi).Text, ls5(posisi).Text, lsn(posisi).Text, lst(posisi).Text, lsk(posisi).Text, lsni(posisi).Text}
            tdata = {ls1(posisi + 1).Text, ls2(posisi + 1).Text, ls3(posisi + 1).Text, ls4(posisi + 1).Text, ls5(posisi + 1).Text, lsn(posisi + 1).Text, lst(posisi + 1).Text, lsk(posisi + 1).Text, lsni(posisi + 1).Text}
            ls1(posisi + 1).Text = tdata2(0)
            ls2(posisi + 1).Text = tdata2(1)
            ls3(posisi + 1).Text = tdata2(2)
            ls4(posisi + 1).Text = tdata2(3)
            ls5(posisi + 1).Text = tdata2(4)
            lsn(posisi + 1).Text = tdata2(5)
            lst(posisi + 1).Text = tdata2(6)
            lsk(posisi + 1).Text = tdata2(7)
            lsni(posisi + 1).Text = tdata2(8)

            ls1(posisi).Text = tdata(0)
            ls2(posisi).Text = tdata(1)
            ls3(posisi).Text = tdata(2)
            ls4(posisi).Text = tdata(3)
            ls5(posisi).Text = tdata(4)
            lsn(posisi).Text = tdata(5)
            lst(posisi).Text = tdata(6)
            lsk(posisi).Text = tdata(7)
            lsni(posisi).Text = tdata(8)
            lsn(posisi + 1).Focus()
            LagiAturIsi = False
            MainkanMusikExtra()
        ElseIf tombol = Keys.Down And posisi <> 0 Then
            LagiAturIsi = True
            tdata2 = {ls1(posisi).Text, ls2(posisi).Text, ls3(posisi).Text, ls4(posisi).Text, ls5(posisi).Text, lsn(posisi).Text, lst(posisi).Text, lsk(posisi).Text, lsni(posisi).Text}
            tdata = {ls1(posisi - 1).Text, ls2(posisi - 1).Text, ls3(posisi - 1).Text, ls4(posisi - 1).Text, ls5(posisi - 1).Text, lsn(posisi - 1).Text, lst(posisi - 1).Text, lsk(posisi - 1).Text, lsni(posisi - 1).Text}
            ls1(posisi - 1).Text = tdata2(0)
            ls2(posisi - 1).Text = tdata2(1)
            ls3(posisi - 1).Text = tdata2(2)
            ls4(posisi - 1).Text = tdata2(3)
            ls5(posisi - 1).Text = tdata2(4)
            lsn(posisi - 1).Text = tdata2(5)
            lst(posisi - 1).Text = tdata2(6)
            lsk(posisi - 1).Text = tdata2(7)
            lsni(posisi - 1).Text = tdata2(8)

            ls1(posisi).Text = tdata(0)
            ls2(posisi).Text = tdata(1)
            ls3(posisi).Text = tdata(2)
            ls4(posisi).Text = tdata(3)
            ls5(posisi).Text = tdata(4)
            lsn(posisi).Text = tdata(5)
            lst(posisi).Text = tdata(6)
            lsk(posisi).Text = tdata(7)
            lsni(posisi).Text = tdata(8)
            lsn(posisi - 1).Focus()
            LagiAturIsi = False
            MainkanMusikExtra()
        End If
    End Sub

    Private Sub PosisikanTanggal()
        Dim indeks As Integer = 0
        Dim belumterisisemua As Boolean

        For Each item As TextBox In lst
            If item.TextLength < 6 And lsn(indeks).Text <> "" Then
                item.Focus()
                belumterisisemua = True
            End If
            indeks += 1
        Next

        If Not belumterisisemua Then
            TBKode1.Focus()
        End If
    End Sub

    Private Sub PosisikanKode()
        Dim indeks As Integer = 0
        Dim belumterisisemua As Boolean

        For Each item As TextBox In lsk
            If item.TextLength < 7 And lsn(indeks).Text <> "" Then
                item.Focus()
                belumterisisemua = True
            End If
            indeks += 1
        Next

        If Not belumterisisemua Then
            TBKawinSu.Focus()
        End If
    End Sub

    Private Sub BersihkanControl()
        onbersihkan = True

        ReDim Preserve kl(7)
        kl(0) = 3
        kl(1) = 3
        kl(2) = 3
        kl(3) = 3
        kl(4) = 3
        kl(5) = 3
        kl(6) = 3

        If baruin Then
            TBKendali.Text = ""
            baruin = False
        Else
            TBKendali.Text = TentukanKendali(TBKendali.Text)
        End If

        TBNoRumah.Clear()
        TBNoKeluarga.Clear()

        TBNik1.Clear()
        TBNama1.Clear()
        TBTanggal1.Clear()
        TBKode1.Clear()

        TBNik2.Clear()
        TBNama2.Clear()
        TBTanggal2.Clear()
        TBKode2.Clear()

        TBNik3.Clear()
        TBNama3.Clear()
        TBTanggal3.Clear()
        TBKode3.Clear()

        TBNik4.Clear()
        TBNama4.Clear()
        TBTanggal4.Clear()
        TBKode4.Clear()

        TBNik5.Clear()
        TBNama5.Clear()
        TBTanggal5.Clear()
        TBKode5.Clear()

        TBNik6.Clear()
        TBNama6.Clear()
        TBTanggal6.Clear()
        TBKode6.Clear()

        TBNik7.Clear()
        TBNama7.Clear()
        TBTanggal7.Clear()
        TBKode7.Clear()

        TBKawinSu.Clear()
        TBKawinIs.Clear()
        TBJumlahAnak.Clear()
        TBKb.Clear()
        TBKontra.Clear()
        TBLamaTKb.Clear()
        TBLamaBKb.Clear()
        TBPunyaAnak.Clear()
        TBAlasanTidakKb.Clear()
        TBTmpatKb.Clear()
        TBPembangunan.Clear()
        TBLuasRumah.Clear()
        TBOrang.Clear()

        T11.Clear()
        T12.Clear()
        T13.Clear()
        T14.Clear()
        T15.Clear()

        T21.Clear()
        T22.Clear()
        T23.Clear()
        T24.Clear()
        T25.Clear()

        T31.Clear()
        T32.Clear()
        T33.Clear()
        T34.Clear()
        T35.Clear()

        T41.Clear()
        T42.Clear()
        T43.Clear()
        T44.Clear()
        T45.Clear()

        T51.Clear()
        T52.Clear()
        T53.Clear()
        T54.Clear()
        T55.Clear()

        T61.Clear()
        T62.Clear()
        T63.Clear()
        T64.Clear()
        T65.Clear()

        T71.Clear()
        T72.Clear()
        T73.Clear()
        T74.Clear()
        T75.Clear()

        TP1.Clear()
        TP2.Clear()

        BFix.Visible = False
        onbersihkan = False
    End Sub

    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Private Shared Function ShowWindow(ByVal hWnd As IntPtr, ByVal flags As ShowWindowEnum) As Integer
    End Function

    Private Enum ShowWindowEnum
        Hide = 0
        ShowNormal = 1
        ShowMinimized = 2
        ShowMaximized = 3
        Maximize = 3
        ShowNormalNoActivate = 4
        Show = 5
        Minimize = 6
        ShowMinNoActivate = 7
        ShowNoActivate = 8
        Restore = 9
        ShowDefault = 10
        ForceMinimized = 11
    End Enum

    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Public Shared Function SetForegroundWindow(ByVal hwnd As IntPtr) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Public Shared Function SetActiveWindow(ByVal hwnd As Integer) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Private Shared Function FindWindow(ClassName As [String], WindowName As [String]) As Integer
    End Function

    Public trmainkanmusikeror As Threading.Thread
    Dim adaeror As Boolean

    Private Sub herehere()
        Dim hWnd As Integer = 0
        hWnd = FindWindow(Nothing, "Terjadi Kesalahan")

        If hWnd > 0 Then
            Try
                trmainkanmusikeror.Start()
            Catch ex As Exception
            End Try

            Try
                tr.Abort()
            Catch ex As Exception
            End Try

            Try
                tfind.Abort()
            Catch ex As Exception
            End Try
            Exit Sub
        End If
        adaeror = False
    End Sub

    Dim trkonfirmasi1 As New Threading.Thread(AddressOf adakonfirmasi)
    Dim trkonfirmasi2 As New Threading.Thread(AddressOf adakonfirmasi2)

    Public sudah As Boolean = False

    Private Sub adakonfirmasi()
        Dim a As Integer = 0
        sudah = False
        While sudah = False
            cadakonfirmasi()
            Threading.Thread.Sleep(50)
        End While
    End Sub

    Private Sub wait()
        While sudah = False

        End While
    End Sub

    Private Sub cadakonfirmasi()
        Dim hWnd As Integer = 0
        hWnd = FindWindow(Nothing, "Konfirmasi")

        If hWnd > 0 Then
            sudah = True
        End If
    End Sub

    Private Sub adakonfirmasi2()
        Dim a As Integer = 0
        sudah = False
        While sudah = False
            cadakonfirmasi2()
            Threading.Thread.Sleep(50)
        End While
    End Sub

    Private Sub cadakonfirmasi2()
        Dim hWnd As Integer = 0
        hWnd = FindWindow(Nothing, "Microsoft Excel")

        If hWnd > 0 Then
            sudah = True
        End If
    End Sub


    Public Sub findwindow()
        Dim a As Integer = 0
        While a < 1
            herehere()
            Threading.Thread.Sleep(100)
        End While
    End Sub

    Public Function BringWindowToFront() As Boolean
        Dim bProcess As Process
        Try
            bProcess = Process.GetProcessesByName("excel")(0)
        Catch ex As Exception
            KotakPesan.Show(Form2, "Silakan buka dulu file excel untuk pengentrian data, dan posisikan ke sheet FORMULIR.")
            Return False
        End Try
        If bProcess IsNot Nothing Then
            Dim handle As IntPtr = bProcess.MainWindowHandle
            Dim hwnd As Integer = CInt(bProcess.MainWindowHandle)
            SetForegroundWindow(handle)
            SetActiveWindow(hwnd)
            ShowWindow(handle, ShowWindowEnum.ShowMaximized)
            SetActiveWindow(CInt(bProcess.MainWindowHandle))
        Else
            KotakPesan.Show(Form2, "Silakan buka dulu file excel untuk pengentrian data, dan posisikan ke sheet FORMULIR.")
            Return False
        End If
        Return True
    End Function

    Public tidakdariload As Boolean = True

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For index = Now.Year To 1996 Step -1
            CBTahunPendataan.Items.Add(index)
        Next
        CBTahunPendataan.Text = Now.Year
        keyboardHook.Start()
        lst.Add(TBTanggal7)
        lst.Add(TBTanggal6)
        lst.Add(TBTanggal5)
        lst.Add(TBTanggal4)
        lst.Add(TBTanggal3)
        lst.Add(TBTanggal2)
        lst.Add(TBTanggal1)

        lsn.Add(TBNama7)
        lsn.Add(TBNama6)
        lsn.Add(TBNama5)
        lsn.Add(TBNama4)
        lsn.Add(TBNama3)
        lsn.Add(TBNama2)
        lsn.Add(TBNama1)

        lsk.Add(TBKode7)
        lsk.Add(TBKode6)
        lsk.Add(TBKode5)
        lsk.Add(TBKode4)
        lsk.Add(TBKode3)
        lsk.Add(TBKode2)
        lsk.Add(TBKode1)

        ls1.Add(T71)
        ls1.Add(T61)
        ls1.Add(T51)
        ls1.Add(T41)
        ls1.Add(T31)
        ls1.Add(T21)
        ls1.Add(T11)

        ls2.Add(T72)
        ls2.Add(T62)
        ls2.Add(T52)
        ls2.Add(T42)
        ls2.Add(T32)
        ls2.Add(T22)
        ls2.Add(T12)

        ls3.Add(T73)
        ls3.Add(T63)
        ls3.Add(T53)
        ls3.Add(T43)
        ls3.Add(T33)
        ls3.Add(T23)
        ls3.Add(T13)

        ls4.Add(T74)
        ls4.Add(T64)
        ls4.Add(T54)
        ls4.Add(T44)
        ls4.Add(T34)
        ls4.Add(T24)
        ls4.Add(T14)

        ls5.Add(T75)
        ls5.Add(T65)
        ls5.Add(T55)
        ls5.Add(T45)
        ls5.Add(T35)
        ls5.Add(T25)
        ls5.Add(T15)

        lsni.Add(TBNik7)
        lsni.Add(TBNik6)
        lsni.Add(TBNik5)
        lsni.Add(TBNik4)
        lsni.Add(TBNik3)
        lsni.Add(TBNik2)
        lsni.Add(TBNik1)

        Dim dirinfo As DirectoryInfo
        Dim allFiles() As FileInfo

        My.Computer.FileSystem.CreateDirectory(AlamatAppData)
        dirinfo = New DirectoryInfo(AlamatAppData)
        allFiles = dirinfo.GetFiles("*.dft")
        Array.Sort(allFiles, New clsCompareFileInfo)
        For Each fl As FileInfo In allFiles
            FPilihData.CBData.Items.Add(fl.ToString)
        Next

        'Dim folderdata As IO.FileInfo() = alamatdata.GetFiles("*.dft")

        'For Each item As IO.FileInfo In folderdata
        '    FPilihData.CBData.Items.Add(item.ToString)
        'Next

        If Not IO.File.Exists(AlamatAppData + "\Pengaturan lagi.ini") Then
            If KotakPesan.Show(Me, "Ini adalah aplikasi untuk memudahkan pengentrian data BKKBN." + Environment.NewLine + Environment.NewLine + "Penting bagi Anda untuk melihat tutorialnya (baru) terlebih dahulu, ingin melihat tutorial ?", "Selamat Datang", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Informasi) = KotakPesan.HasilPesan.Ya Then
                System.Diagnostics.Process.Start(CurDir() + "\Tutor")
                System.Diagnostics.Process.Start(CurDir() + "\Tutor\Bantuan.txt")
                Using Berkas As New IO.StreamWriter(AlamatAppData + "\" + "Pengaturan lagi.ini")
                    Berkas.WriteLine("Entri Data BKKBN")
                End Using
            End If
        End If

        If FPilihData.CBData.Items.Count > 1 Then
            FPilihData.ShowDialog(Me)
        ElseIf FPilihData.CBData.Items.Count = 1 Then
            namafile = FPilihData.CBData.Items(0)
        End If

        tidakdariload = False

        Try
            BacaData()
            TBKendali.Text = TentukanKendali(REF(REF.Count - 1))
            LInfo.Text = "Data terakhir: Kendali=" + REF(REF.Count - 1) + ", NR=" + NR(REF.Count - 1) + ", NK=" + NK(REF.Count - 1) + ", KK=" + NAMA1(REF.Count - 1)
        Catch ex As Exception
            LInfo.Text = "Data terakhir: Kendali=" + "Data Baru" + ", NR=" + "Data Baru" + ", NK=Data Baru" + ", KK=Data Baru"
        End Try

        ToolStripStatusLabel1.Text = namafile


        ReDim Preserve kl(7)
        kl(0) = 3
        kl(1) = 3
        kl(2) = 3
        kl(3) = 3
        kl(4) = 3
        kl(5) = 3
        kl(6) = 3
    End Sub

    Private Sub button2_Click(sender As Object, e As EventArgs) Handles BKeluar.Click
        Me.Close()
    End Sub

    Sub BacaData()
        Bersihkan()
        Dim sem As String
        Try
            Using Berkas As New IO.StreamReader(AlamatAppData + "\" + namafile)
                Dim id As String = "EROR BACA"
                While Not Berkas.EndOfStream
                    sem = Berkas.ReadLine

                    If sem.Contains("[REF]") Then
                        REF.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KREF]") Then
                        KREF.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NR]") Then
                        NR.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NK]") Then
                        NK.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NIK1]") Then
                        NIK1.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NAMA1]") Then
                        NAMA1.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[TL1]") Then
                        TL1.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KD1]") Then
                        KD1.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NIK2]") Then
                        NIK2.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NAMA2]") Then
                        NAMA2.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[TL2]") Then
                        TL2.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KD2]") Then
                        KD2.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NIK3]") Then
                        NIK3.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NAMA3]") Then
                        NAMA3.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[TL3]") Then
                        TL3.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KD3]") Then
                        KD3.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NIK4]") Then
                        NIK4.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NAMA4]") Then
                        NAMA4.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[TL4]") Then
                        TL4.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KD4]") Then
                        KD4.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NIK5]") Then
                        NIK5.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NAMA5]") Then
                        NAMA5.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[TL5]") Then
                        TL5.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KD5]") Then
                        KD5.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NIK6]") Then
                        NIK6.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NAMA6]") Then
                        NAMA6.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[TL6]") Then
                        TL6.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KD6]") Then
                        KD6.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NIK7]") Then
                        NIK7.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[NAMA7]") Then
                        NAMA7.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[TL7]") Then
                        TL7.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KD7]") Then
                        KD7.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[UsiaKawinSuami]") Then
                        UsiaKawinSuami.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[UsiaKawinIstri]") Then
                        UsiaKawinIstri.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[JumlahAnak]") Then
                        JumlahAnak.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[BerKB]") Then
                        BerKB.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[MetodKontrasepsi]") Then
                        MetodKontrasepsi.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[LamaTKB]") Then
                        LamaTKB.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[LamaBKB]") Then
                        LamaBKB.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[PnyaAnakLagi]") Then
                        PnyaAnakLagi.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[AlasanTidakKB]") Then
                        AlasanTidakKB.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[TmpatLayananKB]") Then
                        TmpatLayananKB.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[KodePembangunan]") Then
                        KodePembangunan.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[Luas]") Then
                        Luas.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    ElseIf sem.Contains("[Orang]") Then
                        Orang.Add(sem.Substring(sem.LastIndexOf("]") + 1))
                    End If
                End While
            End Using

            If KREF.Count <> REF.Count Then
                For Each kendali As String In REF
                    KREF.Add("Tidak Ada")
                Next
            End If

            CBKRef.Items.Clear()
            lNoKeluarga.Clear()
            CBKRef.Items.Add("Tidak Ada")
            Dim abc As Integer = 0
            For Each Kendali As String In REF
                CBKRef.Items.Add(Kendali)
                lNoKeluarga.Add(NK(abc))
                abc += 1
            Next
            CBKRef.SelectedIndex = 0
        Catch ex As Exception
        End Try
    End Sub

    Private Sub MuatKendali()
        CBKendali.Items.Clear()
        For Each data As String In REF
            CBKendali.Items.Add(data)
        Next
    End Sub

    Private Function TentukanUmur(nilai As String) As Integer
        Dim hasil, tahun As Integer
        Dim a As Integer = Integer.Parse(nilai.Substring(4, 2))
        Dim stahun As String = Now.Year
        Dim dgit2 As Integer = Integer.Parse(stahun.Substring(2, 2))

        If a <= dgit2 Then
            If a < 10 Then
                tahun = Integer.Parse("200" + a.ToString)
            ElseIf a >= 10
                tahun = Integer.Parse("20" + a.ToString)
            End If
        Else
            tahun = Integer.Parse("19" + a.ToString)
        End If

        hasil = Integer.Parse(TahunPendataan) - tahun
        Return hasil
    End Function

    Private Function MinimalkanMissingValue() As Boolean
        If TBNama1.Text = "" Then
            BX1.PerformClick()
        End If
        If TBNama2.Text = "" Then
            BX2.PerformClick()
        End If
        If TBNama3.Text = "" Then
            BX3.PerformClick()
        End If
        If TBNama4.Text = "" Then
            BX4.PerformClick()
        End If
        If TBNama5.Text = "" Then
            BX5.PerformClick()
        End If
        If TBNama6.Text = "" Then
            BX6.PerformClick()
        End If
        If TBNama7.Text = "" Then
            BX7.PerformClick()
        End If

        If TBKendali.Text.Length <> 8 Then
            KotakPesan.Show(Me, "No Kendali harus berdigit 8 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKendali.Focus()
            Return False
        ElseIf TBNoRumah.Text.Length <> 4 Then
            KotakPesan.Show(Me, "No Rumah harus berdigit 4 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBNoRumah.Focus()
            Return False
        ElseIf TBNoKeluarga.Text.Length <> 3 Then
            KotakPesan.Show(Me, "No Keluarga harus berdigit 3 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBNoKeluarga.Focus()
            Return False
        ElseIf TBNama1.Text <> "" And TBTanggal1.Text.Length <> 6 Then
            KotakPesan.Show(Me, "Tanggal Lahir harus berdigit 6 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal1.Focus()
            Return False
        ElseIf TBNama2.Text <> "" And TBTanggal2.Text.Length <> 6 Then
            KotakPesan.Show(Me, "Tanggal Lahir harus berdigit 6 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal2.Focus()
            Return False
        ElseIf TBNama3.Text <> "" And TBTanggal3.Text.Length <> 6 Then
            KotakPesan.Show(Me, "Tanggal Lahir harus berdigit 6 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal3.Focus()
            Return False
        ElseIf TBNama4.Text <> "" And TBTanggal4.Text.Length <> 6 Then
            KotakPesan.Show(Me, "Tanggal Lahir harus berdigit 6 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal4.Focus()
            Return False
        ElseIf TBNama5.Text <> "" And TBTanggal5.Text.Length <> 6 Then
            KotakPesan.Show(Me, "Tanggal Lahir harus berdigit 6 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal5.Focus()
            Return False
        ElseIf TBNama6.Text <> "" And TBTanggal6.Text.Length <> 6 Then
            KotakPesan.Show(Me, "Tanggal Lahir harus berdigit 6 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal6.Focus()
            Return False
        ElseIf TBNama7.Text <> "" And TBTanggal7.Text.Length <> 6 Then
            KotakPesan.Show(Me, "Tanggal Lahir harus berdigit 6 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal7.Focus()
            Return False
        ElseIf TBNama1.Text.Length > 0 And TBKode1.Text.Length <> 7 Then
            KotakPesan.Show(Me, "Kode Anggota Keluarga harus berdigit 7 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKode1.Focus()
            Return False
        ElseIf TBNama2.Text.Length > 0 And TBKode2.Text.Length <> 7 Then
            KotakPesan.Show(Me, "Kode Anggota Keluarga harus berdigit 7 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKode2.Focus()
            Return False
        ElseIf TBNama3.Text.Length > 0 And TBKode3.Text.Length <> 7 Then
            KotakPesan.Show(Me, "Kode Anggota Keluarga harus berdigit 7 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKode3.Focus()
            Return False
        ElseIf TBNama4.Text.Length > 0 And TBKode4.Text.Length <> 7 Then
            KotakPesan.Show(Me, "Kode Anggota Keluarga harus berdigit 7 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKode4.Focus()
            Return False
        ElseIf TBNama5.Text.Length > 0 And TBKode5.Text.Length <> 7 Then
            KotakPesan.Show(Me, "Kode Anggota Keluarga harus berdigit 7 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKode5.Focus()
            Return False
        ElseIf TBNama6.Text.Length > 0 And TBKode6.Text.Length <> 7 Then
            KotakPesan.Show(Me, "Kode Anggota Keluarga harus berdigit 7 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKode6.Focus()
            Return False
        ElseIf TBNama7.Text.Length > 0 And TBKode7.Text.Length <> 7 Then
            KotakPesan.Show(Me, "Kode Anggota Keluarga harus berdigit 7 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKode7.Focus()
            Return False
        ElseIf TBJumlahAnak.Text.Length <> 8 Then
            KotakPesan.Show(Me, "Jumlah Anak harus berdigit 8 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBJumlahAnak.Focus()
            Return False
        ElseIf TBLamaBKb.Text > 11 Then
            KotakPesan.Show(Me, "Cek lagi pada bagian bulan di lama ber kb, jika lebih dari 11 bulan, jadikan 1 tahun. Tapi biar kamu ga bingung, klik aja tombol Fix di sebelah kanan Bulan ber KB.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            BFix.Visible = True
            Return False
        ElseIf TBPembangunan.Text.Length <> 26 Then
            KotakPesan.Show(Me, "Pembangunan !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TP2.Focus()
            Return False
        ElseIf TBKb.Text > 2 Then
            KotakPesan.Show(Me, "Indeks Kesertaan ber-KB kelebihan !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKb.Focus()
            Return False
        ElseIf TBKontra.Text > 7 Then
            KotakPesan.Show(Me, "Indeks metode kontrasepsi kelebihan !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBKontra.Focus()
            Return False
        ElseIf TBPunyaAnak.Text > 2 Then
            KotakPesan.Show(Me, "Indeks ingin punya anak kelebihan !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBPunyaAnak.Focus()
            Return False
        ElseIf TBAlasanTidakKb.Text > 7 Then
            KotakPesan.Show(Me, "Indeks alasan tidak KB kelebihan !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBAlasanTidakKb.Focus()
            Return False
        ElseIf TBTmpatKb.Text > 13 Then
            KotakPesan.Show(Me, "Tempat KB !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTmpatKb.Focus()
            Return False
        ElseIf TBLuasRumah.Text < 10 Then
            KotakPesan.Show(Me, "Luas Rumah !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBLuasRumah.Focus()
            Return False
        ElseIf LU1.Text < 0 Then
            KotakPesan.Show(Me, "Harap di cek lagi untuk tanggal lahir ini. " + TentukanUmur(TBTanggal1.Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal1.Focus()
            Return False
        ElseIf LU2.Text < 0 Then
            KotakPesan.Show(Me, "Harap di cek lagi untuk tanggal lahir ini. " + TentukanUmur(TBTanggal2.Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal2.Focus()
            Return False
        ElseIf LU3.Text < 0 Then
            KotakPesan.Show(Me, "Harap di cek lagi untuk tanggal lahir ini. " + TentukanUmur(TBTanggal3.Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal3.Focus()
            Return False
        ElseIf LU4.Text < 0 Then
            KotakPesan.Show(Me, "Harap di cek lagi untuk tanggal lahir ini. " + TentukanUmur(TBTanggal4.Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal4.Focus()
            Return False
        ElseIf LU5.Text < 0 Then
            KotakPesan.Show(Me, "Harap di cek lagi untuk tanggal lahir ini. " + TentukanUmur(TBTanggal5.Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal5.Focus()
            Return False
        ElseIf LU6.Text < 0 Then
            KotakPesan.Show(Me, "Harap di cek lagi untuk tanggal lahir ini. " + TentukanUmur(TBTanggal6.Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal6.Focus()
            Return False
        ElseIf LU7.Text < 0 Then
            KotakPesan.Show(Me, "Harap di cek lagi untuk tanggal lahir ini. " + TentukanUmur(TBTanggal7.Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBTanggal7.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub ErorValue()
        Dim daftarkode As New List(Of TextBox)
        Dim daftartl As New List(Of TextBox)
        Dim daftarhubungan As New List(Of String)
        Dim adaistri As Boolean = False
        daftarkode.Add(TBKode1)
        daftarkode.Add(TBKode2)
        daftarkode.Add(TBKode3)
        daftarkode.Add(TBKode4)
        daftarkode.Add(TBKode5)
        daftarkode.Add(TBKode6)
        daftarkode.Add(TBKode7)
        daftartl.Add(TBTanggal1)
        daftartl.Add(TBTanggal2)
        daftartl.Add(TBTanggal3)
        daftartl.Add(TBTanggal4)
        daftartl.Add(TBTanggal5)
        daftartl.Add(TBTanggal6)
        daftartl.Add(TBTanggal7)
        Dim sementara As String
        Dim a As Integer = 0

        For Each hub As TextBox In daftarkode
            Try
                daftarhubungan.Add(hub.Text.Substring(0, 1))
            Catch ex As Exception
            End Try
        Next

        Dim jumlahkk As Integer = 0

        For Each h As String In daftarhubungan
            If h = 0 Then
                jumlahkk += 1
            End If
        Next

        If CBKRef.Text = "Tidak Ada" Then
            If jumlahkk <> 1 Then
                KotakPesan.Show(Me, "Kode Hubungan dengan KK !, minimal dan maksimal harus ada 1 kepala keluarga.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                batalkan = True
                Exit Sub
            End If
        Else
            If jumlahkk > 0 Then
                KotakPesan.Show(Me, "Kode Hubungan dengan KK !, pada referensi ke no kendali yang lain, tidak boleh ada KK.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                batalkan = True
                Exit Sub
            End If
        End If

        If TBKawinIs.Text < 10 Then
            KotakPesan.Show(Me, "Usia kawin istri harus lebih dari 9 tahun", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            batalkan = True
            TBKawinIs.Focus()
            Exit Sub
        End If

        If TBKawinSu.Text < 10 Then
            KotakPesan.Show(Me, "Usia kawin suami harus lebih dari 9 tahun", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            batalkan = True
            TBKawinSu.Focus()
            Exit Sub
        End If

        For Each item As TextBox In daftarkode
            If item.Text <> "" Then
                If item.Text.Substring(0, 1) = "0" And item.Text.Substring(1, 1) = "1" And item.Text.Substring(5, 1) = "1" Then
                    sementara = item.Text.Substring(0, 5) + "2" + item.Text.Substring(6, 1)
                    item.Text = sementara
                End If

                If item.Text.Substring(0, 1) = "1" And item.Text.Substring(5, 1) = "0" Then
                    KotakPesan.Show(Me, "Sebaiknya disini di cek, Istri tapi berstatus belum kawin ?, cek juga umurnya.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    batalkan = True
                    item.Focus()
                    Exit Sub
                End If

                If item.Text.Substring(0, 1) = "1" And item.Text.Substring(1, 1) = "0" Then
                    KotakPesan.Show(Me, "Istri tapi kelaminya laki laki ?.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    batalkan = True
                    item.Focus()
                    Exit Sub
                End If

                If item.Text.Substring(5, 1) > 0 Then
                    If item.Text.Substring(1, 1) = "1" Then
                        If TBKawinIs.Text > TentukanUmur(daftartl(a).Text) Then
                            KotakPesan.Show(Me, "Usia kawin istri tidak boleh lebih besar dari umurnya.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                            batalkan = True
                            TBKawinIs.Focus()
                            Exit Sub
                        End If
                    Else
                        If TBKawinSu.Text > TentukanUmur(daftartl(a).Text) Then
                            KotakPesan.Show(Me, "Usia kawin suami tidak boleh lebih besar dari umurnya.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                            batalkan = True
                            TBKawinSu.Focus()
                            Exit Sub
                        End If
                    End If
                End If

                If CBKRef.Text <> "Tidak Ada" And item.Text.Substring(5, 1) = "1" Then
                    KotakPesan.Show(Me, "Semua individu tidak boleh ada yang berstatus kawin.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    batalkan = True
                    item.Focus()
                    Exit Sub
                End If

                If item.Text.Substring(0, 1) = "2" And item.Text.Substring(5, 1) = "1" Then
                    sementara = item.Text.Substring(0, 5) + "0" + item.Text.Substring(6, 1)
                    item.Text = sementara
                End If

                If item.Text.Substring(0, 1) = "3" And item.Text.Substring(5, 1) = "1" Then
                    sementara = item.Text.Substring(0, 5) + "0" + item.Text.Substring(6, 1)
                    item.Text = sementara
                End If

                If item.Text.Substring(5, 1) > 0 And TentukanUmur(daftartl(a).Text) < 11 Then
                    sementara = item.Text.Substring(0, 5) + "0" + item.Text.Substring(6, 1)
                    item.Text = sementara
                End If

                If item.Text.Substring(0, 1) = "1" Then
                    adaistri = True
                End If

                If TentukanUmur(daftartl(a).Text) < 5 And item.Text.Substring(3, 1) <> 9 Then
                    KotakPesan.Show(Me, "Umur " + TentukanUmur(daftartl(a).Text).ToString + " tahun, tidak boleh diisi lebih dari Tidak/BELUM SEKOLAH.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    item.Focus()
                    item.SelectionStart = 3
                    item.SelectionLength = 1
                    batalkan = True
                    Exit Sub
                ElseIf TentukanUmur(daftartl(a).Text) < 6 And item.Text.Substring(3, 1) >= 0 And item.Text.Substring(3, 1) <> 9 Then
                    KotakPesan.Show(Me, "Umur " + TentukanUmur(daftartl(a).Text).ToString + " tahun, wajib diisi Tidak/BELUM SEKOLAH.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    item.Focus()
                    item.SelectionStart = 3
                    item.SelectionLength = 1
                    batalkan = True
                    Exit Sub
                ElseIf TentukanUmur(daftartl(a).Text) < 12 And item.Text.Substring(3, 1) > 3 And item.Text.Substring(3, 1) <> 9 Then
                    KotakPesan.Show(Me, "Umur " + TentukanUmur(daftartl(a).Text).ToString + " tahun, tidak boleh diisi lebih dari MASIH SLTP/MTSN.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    item.Focus()
                    item.SelectionStart = 3
                    item.SelectionLength = 1
                    batalkan = True
                    Exit Sub
                ElseIf TentukanUmur(daftartl(a).Text) < 15 And item.Text.Substring(3, 1) > 5 And item.Text.Substring(3, 1) <> 9 Then
                    KotakPesan.Show(Me, "Umur " + TentukanUmur(daftartl(a).Text).ToString + " tahun, tidak boleh diisi lebih dari MASIH SLTA/MA.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    item.Focus()
                    item.SelectionStart = 3
                    item.SelectionLength = 1
                    batalkan = True
                    Exit Sub
                End If

                If TentukanUmur(daftartl(a).Text) < 10 And item.Text.Substring(4, 1) <> 9 Then
                    KotakPesan.Show(Me, "Umur " + TentukanUmur(daftartl(a).Text).ToString + " tahun, tidak boleh diisi bekerja, melainkan harus diisi TIDAK/BELUM BEKERJA.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    item.Focus()
                    item.SelectionStart = 4
                    item.SelectionLength = 1
                    batalkan = True
                    Exit Sub
                End If

                If kl(a) <> 3 Then
                    If kl(a) = 0 And item.Text.Substring(1, 1) = "1" Then
                        KotakPesan.Show(Me, "Individu no " + (a + 1).ToString + " berkelamin perempuan ?, saya rasa ada salah di sini, harap di pastikan bahwa NIK tidak salah ketik, jika perempuan pasti tanggal di nik akan lebih dari 40.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                        batalkan = True
                        item.Focus()
                        item.SelectionStart = 1
                        item.SelectionLength = 1
                        Exit Sub
                    ElseIf kl(a) = 1 And item.Text.Substring(1, 1) = "0" Then
                        KotakPesan.Show(Me, "Individu no " + (a + 1).ToString + " berkelamin laki laki ?, saya rasa ada salah di sini, harap di pastikan bahwa NIK tidak salah ketik, jika laki-laki pasti tanggal di nik tidak akan lebih dari 40.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                        batalkan = True
                        item.Focus()
                        item.SelectionStart = 1
                        item.SelectionLength = 1
                        Exit Sub
                    End If
                End If
            End If
            a += 1
        Next

        a = 0
        For Each item As TextBox In daftarkode
            If item.Text <> "" Then
                If adaistri And item.Text.Substring(0, 1) = 0 And item.Text.Substring(5, 1) <> 1 Then
                    KotakPesan.Show(Me, "Tidak boleh ada istri jika kepala keluarga belum kawin.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    item.Focus()
                    item.SelectionStart = 5
                    item.SelectionLength = 1
                    batalkan = True
                    Exit Sub
                End If

                If item.Text.Substring(0, 1) = "0" And item.Text.Substring(1, 1) = "0" And item.Text.Substring(5, 1) = "1" Then
                    If Not adaistri Then
                        If TentukanUmur(daftartl(a).Text) > 20 Then
                            sementara = item.Text.Substring(0, 5) + "2" + item.Text.Substring(6, 1)
                            item.Text = sementara
                        Else
                            sementara = item.Text.Substring(0, 5) + "0" + item.Text.Substring(6, 1)
                            item.Text = sementara
                        End If
                    End If
                End If
            End If
            a += 1
        Next
        batalkan = False
    End Sub

    Private Function KodeBenar() As Boolean
        Dim daftar As New List(Of TextBox)
        Dim acuan As Integer() = {3, 1, 6, 9, 9, 2, 3}
        daftar.Add(TBKode1)
        daftar.Add(TBKode2)
        daftar.Add(TBKode3)
        daftar.Add(TBKode4)
        daftar.Add(TBKode5)
        daftar.Add(TBKode6)
        daftar.Add(TBKode7)

        Dim inddd As Integer = 0
        For Each nilai As TextBox In daftar
            inddd = 0
            For Each kar As Char In nilai.Text
                If Integer.Parse(kar) > acuan(inddd) Then
                    KotakPesan.Show(Me, "Kode yang anda masukan terlihat tidak benar. Pada bagian Kode Keluarga poin ke " + (inddd + 1).ToString + ", pilihan hanya sampai " + acuan(inddd).ToString + " (" + (acuan(inddd) + 1).ToString + " pilihan), bukan sampai " + kar + " (" + (Integer.Parse(kar) + 1).ToString + " pilihan).", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    nilai.Focus()
                    nilai.SelectionStart = inddd
                    nilai.SelectionLength = 1
                    Return False
                End If
                inddd += 1
            Next
        Next
        Return True
    End Function

    Private Function KodePemBenar() As Boolean
        Dim acuan As Integer() = {1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 3, 3, 3, 3, 3, 3, 3, 3}
        Dim inddd As Integer = 0

        For Each kar As Char In TBPembangunan.Text
            If Integer.Parse(kar) > acuan(inddd) Then
                KotakPesan.Show(Me, "Kode yang anda masukan terlihat tidak benar. Pada bagian Pembangunan Keluarga poin ke " + (inddd + 1).ToString + ", pilihan hanya sampai " + acuan(inddd).ToString + " (" + (acuan(inddd) + 1).ToString + " pilihan), bukan sampai " + kar + " (" + (Integer.Parse(kar) + 1).ToString + " pilihan).", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                If inddd < 18 Then
                    TP1.Focus()
                    TP1.SelectionStart = inddd
                    TP1.SelectionLength = 1
                Else
                    TP2.Focus()
                    TP2.SelectionStart = inddd - 18
                    TP2.SelectionLength = 1
                End If
                Return False
            End If
            inddd += 1
        Next
        Return True
    End Function

    Private Function LamaKawinValid() As Boolean
        Dim daftarkode As New List(Of TextBox)
        Dim daftartl As New List(Of TextBox)
        daftarkode.Add(TBKode1)
        daftarkode.Add(TBKode2)
        daftarkode.Add(TBKode3)
        daftarkode.Add(TBKode4)
        daftarkode.Add(TBKode5)
        daftarkode.Add(TBKode6)
        daftarkode.Add(TBKode7)
        daftartl.Add(TBTanggal1)
        daftartl.Add(TBTanggal2)
        daftartl.Add(TBTanggal3)
        daftartl.Add(TBTanggal4)
        daftartl.Add(TBTanggal5)
        daftartl.Add(TBTanggal6)
        daftartl.Add(TBTanggal7)

        Dim ind As Integer = 0
        For Each kk As TextBox In daftarkode
            If kk.Text <> "" Then
                If kk.Text.ToString.Substring(0, 1) = 0 And kk.Text.Substring(5, 1) = 1 Then
                    If Integer.Parse(TBKawinSu.Text) > TentukanUmur(daftartl(ind).Text) Then
                        KotakPesan.Show(Me, "Emang ada ?, orang sudah berstatus kawin sejak dalam kandungan ? -_-, Lama usia kawin suami lebih tinggi dari umurnya, ini salah. " + TBKawinSu.Text + " dan " + TentukanUmur(daftartl(ind).Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                        TBKawinSu.Focus()
                        Return False
                    End If
                ElseIf kk.Text.ToString.Substring(0, 1) = 1 And kk.Text.Substring(5, 1) = 1 Then
                    If Integer.Parse(TBKawinIs.Text) > TentukanUmur(daftartl(ind).Text) Then
                        KotakPesan.Show(Me, "Emang ada ?, orang sudah berstatus kawin sejak dalam kandungan ? -_-, Lama usia kawin istri lebih tinggi dari umurnya, ini salah. " + TBKawinIs.Text + " dan " + TentukanUmur(daftartl(ind).Text).ToString, "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                        TBKawinIs.Focus()
                        Return False
                    End If
                End If
            End If
            ind += 1
        Next
        Return True
    End Function

    Public Sub UpdateKendali(nilai As String)
        TBKendali.Text = TentukanKendali(nilai)
    End Sub

    Public Sub UbahSemua()
        If IO.File.Exists(AlamatAppData + "\" + namafile) Then
            Using Berkas As New IO.StreamWriter(AlamatAppData + "\" + namafile)
                Berkas.WriteLine("#Daftar Entri Data Keluarga")
                Dim ii As Integer
                For Each kendali As String In REF
                    Berkas.WriteLine("[Mulai]")
                    Berkas.WriteLine(" [REF]" + kendali)
                    Berkas.WriteLine(" [KREF]" + KREF(ii))
                    Berkas.WriteLine("  [Kependudukan]")
                    Berkas.WriteLine("   [NR]" + NR(ii))
                    Berkas.WriteLine("   [NK]" + NK(ii))
                    Berkas.WriteLine("   [NIK1]" + NIK1(ii))
                    Berkas.WriteLine("    [NAMA1]" + NAMA1(ii))
                    Berkas.WriteLine("    [TL1]" + TL1(ii))
                    Berkas.WriteLine("    [KD1]" + KD1(ii))
                    Berkas.WriteLine("   [NIK2]" + NIK2(ii))
                    Berkas.WriteLine("    [NAMA2]" + NAMA2(ii))
                    Berkas.WriteLine("    [TL2]" + TL2(ii))
                    Berkas.WriteLine("    [KD2]" + KD2(ii))
                    Berkas.WriteLine("   [NIK3]" + NIK3(ii))
                    Berkas.WriteLine("    [NAMA3]" + NAMA3(ii))
                    Berkas.WriteLine("    [TL3]" + TL3(ii))
                    Berkas.WriteLine("    [KD3]" + KD3(ii))
                    Berkas.WriteLine("   [NIK4]" + NIK4(ii))
                    Berkas.WriteLine("    [NAMA4]" + NAMA4(ii))
                    Berkas.WriteLine("    [TL4]" + TL4(ii))
                    Berkas.WriteLine("    [KD4]" + KD4(ii))
                    Berkas.WriteLine("   [NIK5]" + NIK5(ii))
                    Berkas.WriteLine("    [NAMA5]" + NAMA5(ii))
                    Berkas.WriteLine("    [TL5]" + TL5(ii))
                    Berkas.WriteLine("    [KD5]" + KD5(ii))
                    Berkas.WriteLine("   [NIK6]" + NIK6(ii))
                    Berkas.WriteLine("    [NAMA6]" + NAMA6(ii))
                    Berkas.WriteLine("    [TL6]" + TL6(ii))
                    Berkas.WriteLine("    [KD6]" + KD6(ii))
                    Berkas.WriteLine("   [NIK7]" + NIK7(ii))
                    Berkas.WriteLine("    [NAMA7]" + NAMA7(ii))
                    Berkas.WriteLine("    [TL7]" + TL7(ii))
                    Berkas.WriteLine("    [KD7]" + KD7(ii))
                    Berkas.WriteLine("  [Keluarga Berencana]")
                    Berkas.WriteLine("   [UsiaKawinSuami]" + UsiaKawinSuami(ii))
                    Berkas.WriteLine("   [UsiaKawinIstri]" + UsiaKawinIstri(ii))
                    Berkas.WriteLine("   [JumlahAnak]" + JumlahAnak(ii))
                    Berkas.WriteLine("   [BerKB]" + BerKB(ii))
                    Berkas.WriteLine("   [MetodKontrasepsi]" + MetodKontrasepsi(ii))
                    Berkas.WriteLine("   [LamaTKB]" + LamaTKB(ii))
                    Berkas.WriteLine("   [LamaBKB]" + LamaBKB(ii))
                    Berkas.WriteLine("   [PnyaAnakLagi]" + PnyaAnakLagi(ii))
                    Berkas.WriteLine("   [AlasanTidakKB]" + AlasanTidakKB(ii))
                    Berkas.WriteLine("   [TmpatLayananKB]" + TmpatLayananKB(ii))
                    Berkas.WriteLine("  [Pembangunan Keluarga]")
                    Berkas.WriteLine("   [KodePembangunan]" + KodePembangunan(ii))
                    Berkas.WriteLine("   [Luas]" + Luas(ii))
                    Berkas.WriteLine("   [Orang]" + Orang(ii))
                    Berkas.WriteLine("[Akhir]")
                    Berkas.WriteLine("")
                    ii += 1
                Next
            End Using

            If KotakPesan.Show(Me, "Data sudah diubah, kembali ke mode Tambah ?", "Entri BKKBN", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) = KotakPesan.HasilPesan.Ya Then
                FEditKendali.Close()
            End If
            BacaData()
        Else
            KotakPesan.Show(Me, "Berkas " + namafile + " hilang !", "Berkas Tidak Ditemukan", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Eror)
        End If
    End Sub

    Dim batalkan As Boolean
    Dim hitungwaktu As Boolean = False

    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BTambah.Click
        If tidakdariload Then
            KotakPesan.Show(Me, "Hahaha :D, saya tau apa yang kamu lakuin :D, dan saya juga tau apa yang harus aku lakuin wkaka :P.", "Show Window ?", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            Exit Sub
        End If

        If MinimalkanMissingValue() = False Then
            Exit Sub
        End If
        ErorValue()
        If batalkan Then
            Exit Sub
        End If
        If KodeBenar() = False Then
            Exit Sub
        End If
        If KodePemBenar() = False Then
            Exit Sub
        End If
        If LamaKawinValid() = False Then
            Exit Sub
        End If
        Application.DoEvents()

        Dim NamaBerkas As String = namafile
        If BTambah.Text.Contains("Tambah") Then
            My.Computer.FileSystem.CreateDirectory(AlamatAppData)
            If Not IO.File.Exists(AlamatAppData + "\" + NamaBerkas) Then
                FPilihData.CBData.Items.Add(NamaBerkas)
                Using Berkas As New IO.StreamWriter(AlamatAppData + "\" + NamaBerkas)
                    Berkas.WriteLine("#Daftar Entri Data Keluarga")
                    Berkas.WriteLine("[Mulai]")
                    Berkas.WriteLine(" [REF]" + TBKendali.Text)
                    Berkas.WriteLine(" [KREF]" + CBKRef.Text)
                    Berkas.WriteLine("  [Kependudukan]")
                    Berkas.WriteLine("   [NR]" + TBNoRumah.Text)
                    Berkas.WriteLine("   [NK]" + TBNoKeluarga.Text)
                    Berkas.WriteLine("   [NIK1]" + TBNik1.Text)
                    Berkas.WriteLine("    [NAMA1]" + TBNama1.Text.ToLower)
                    Berkas.WriteLine("    [TL1]" + TBTanggal1.Text)
                    Berkas.WriteLine("    [KD1]" + TBKode1.Text)
                    Berkas.WriteLine("   [NIK2]" + TBNik2.Text)
                    Berkas.WriteLine("    [NAMA2]" + TBNama2.Text.ToLower)
                    Berkas.WriteLine("    [TL2]" + TBTanggal2.Text)
                    Berkas.WriteLine("    [KD2]" + TBKode2.Text)
                    Berkas.WriteLine("   [NIK3]" + TBNik3.Text)
                    Berkas.WriteLine("    [NAMA3]" + TBNama3.Text.ToLower)
                    Berkas.WriteLine("    [TL3]" + TBTanggal3.Text)
                    Berkas.WriteLine("    [KD3]" + TBKode3.Text)
                    Berkas.WriteLine("   [NIK4]" + TBNik4.Text)
                    Berkas.WriteLine("    [NAMA4]" + TBNama4.Text.ToLower)
                    Berkas.WriteLine("    [TL4]" + TBTanggal4.Text)
                    Berkas.WriteLine("    [KD4]" + TBKode4.Text)
                    Berkas.WriteLine("   [NIK5]" + TBNik5.Text)
                    Berkas.WriteLine("    [NAMA5]" + TBNama5.Text.ToLower)
                    Berkas.WriteLine("    [TL5]" + TBTanggal5.Text)
                    Berkas.WriteLine("    [KD5]" + TBKode5.Text)
                    Berkas.WriteLine("   [NIK6]" + TBNik6.Text)
                    Berkas.WriteLine("    [NAMA6]" + TBNama6.Text.ToLower)
                    Berkas.WriteLine("    [TL6]" + TBTanggal6.Text)
                    Berkas.WriteLine("    [KD6]" + TBKode6.Text)
                    Berkas.WriteLine("   [NIK7]" + TBNik7.Text)
                    Berkas.WriteLine("    [NAMA7]" + TBNama7.Text.ToLower)
                    Berkas.WriteLine("    [TL7]" + TBTanggal7.Text)
                    Berkas.WriteLine("    [KD7]" + TBKode7.Text)
                    Berkas.WriteLine("  [Keluarga Berencana]")
                    Berkas.WriteLine("   [UsiaKawinSuami]" + TBKawinSu.Text)
                    Berkas.WriteLine("   [UsiaKawinIstri]" + TBKawinIs.Text)
                    Berkas.WriteLine("   [JumlahAnak]" + TBJumlahAnak.Text)
                    Berkas.WriteLine("   [BerKB]" + TBKb.Text)
                    Berkas.WriteLine("   [MetodKontrasepsi]" + TBKontra.Text)
                    Berkas.WriteLine("   [LamaTKB]" + TBLamaTKb.Text)
                    Berkas.WriteLine("   [LamaBKB]" + TBLamaBKb.Text)
                    Berkas.WriteLine("   [PnyaAnakLagi]" + TBPunyaAnak.Text)
                    Berkas.WriteLine("   [AlasanTidakKB]" + TBAlasanTidakKb.Text)
                    Berkas.WriteLine("   [TmpatLayananKB]" + TBTmpatKb.Text)
                    Berkas.WriteLine("  [Pembangunan Keluarga]")
                    Berkas.WriteLine("   [KodePembangunan]" + TBPembangunan.Text)
                    Berkas.WriteLine("   [Luas]" + TBLuasRumah.Text)
                    Berkas.WriteLine("   [Orang]" + TBOrang.Text)
                    Berkas.WriteLine("[Akhir]")
                    Berkas.WriteLine("")
                End Using
            Else
                Using Berkas As New IO.StreamWriter(AlamatAppData + "\" + NamaBerkas, True)
                    Berkas.WriteLine("[Mulai]")
                    Berkas.WriteLine(" [REF]" + TBKendali.Text)
                    Berkas.WriteLine(" [KREF]" + CBKRef.Text)
                    Berkas.WriteLine("  [Kependudukan]")
                    Berkas.WriteLine("   [NR]" + TBNoRumah.Text)
                    Berkas.WriteLine("   [NK]" + TBNoKeluarga.Text)
                    Berkas.WriteLine("   [NIK1]" + TBNik1.Text)
                    Berkas.WriteLine("    [NAMA1]" + TBNama1.Text.ToLower)
                    Berkas.WriteLine("    [TL1]" + TBTanggal1.Text)
                    Berkas.WriteLine("    [KD1]" + TBKode1.Text)
                    Berkas.WriteLine("   [NIK2]" + TBNik2.Text)
                    Berkas.WriteLine("    [NAMA2]" + TBNama2.Text.ToLower)
                    Berkas.WriteLine("    [TL2]" + TBTanggal2.Text)
                    Berkas.WriteLine("    [KD2]" + TBKode2.Text)
                    Berkas.WriteLine("   [NIK3]" + TBNik3.Text)
                    Berkas.WriteLine("    [NAMA3]" + TBNama3.Text.ToLower)
                    Berkas.WriteLine("    [TL3]" + TBTanggal3.Text)
                    Berkas.WriteLine("    [KD3]" + TBKode3.Text)
                    Berkas.WriteLine("   [NIK4]" + TBNik4.Text)
                    Berkas.WriteLine("    [NAMA4]" + TBNama4.Text.ToLower)
                    Berkas.WriteLine("    [TL4]" + TBTanggal4.Text)
                    Berkas.WriteLine("    [KD4]" + TBKode4.Text)
                    Berkas.WriteLine("   [NIK5]" + TBNik5.Text)
                    Berkas.WriteLine("    [NAMA5]" + TBNama5.Text.ToLower)
                    Berkas.WriteLine("    [TL5]" + TBTanggal5.Text)
                    Berkas.WriteLine("    [KD5]" + TBKode5.Text)
                    Berkas.WriteLine("   [NIK6]" + TBNik6.Text)
                    Berkas.WriteLine("    [NAMA6]" + TBNama6.Text.ToLower)
                    Berkas.WriteLine("    [TL6]" + TBTanggal6.Text)
                    Berkas.WriteLine("    [KD6]" + TBKode6.Text)
                    Berkas.WriteLine("   [NIK7]" + TBNik7.Text)
                    Berkas.WriteLine("    [NAMA7]" + TBNama7.Text.ToLower)
                    Berkas.WriteLine("    [TL7]" + TBTanggal7.Text)
                    Berkas.WriteLine("    [KD7]" + TBKode7.Text)
                    Berkas.WriteLine("  [Keluarga Berencana]")
                    Berkas.WriteLine("   [UsiaKawinSuami]" + TBKawinSu.Text)
                    Berkas.WriteLine("   [UsiaKawinIstri]" + TBKawinIs.Text)
                    Berkas.WriteLine("   [JumlahAnak]" + TBJumlahAnak.Text)
                    Berkas.WriteLine("   [BerKB]" + TBKb.Text)
                    Berkas.WriteLine("   [MetodKontrasepsi]" + TBKontra.Text)
                    Berkas.WriteLine("   [LamaTKB]" + TBLamaTKb.Text)
                    Berkas.WriteLine("   [LamaBKB]" + TBLamaBKb.Text)
                    Berkas.WriteLine("   [PnyaAnakLagi]" + TBPunyaAnak.Text)
                    Berkas.WriteLine("   [AlasanTidakKB]" + TBAlasanTidakKb.Text)
                    Berkas.WriteLine("   [TmpatLayananKB]" + TBTmpatKb.Text)
                    Berkas.WriteLine("  [Pembangunan Keluarga]")
                    Berkas.WriteLine("   [KodePembangunan]" + TBPembangunan.Text)
                    Berkas.WriteLine("   [Luas]" + TBLuasRumah.Text)
                    Berkas.WriteLine("   [Orang]" + TBOrang.Text)
                    Berkas.WriteLine("[Akhir]")
                    Berkas.WriteLine("")
                End Using
            End If
            Dim detik As Double
            Dim h As String = ""

            If Not IO.File.Exists(AlamatAppData + "\" + NamaBerkas) Then
                If KotakPesan.Show(Me, "Coba Lagi menambahkan data, terdapat system Eror, takutnya udah nambah data banyak tapi data malah ngga tersimpan.", "Pengecek Eror", KotakPesan.TombolPesan.Ulangi_Batal, KotakPesan.Warna.Peringatan) = KotakPesan.HasilPesan.Ulangi Then
                    BTambah.PerformClick()
                    Exit Sub
                Else
                    Exit Sub
                End If
            End If


            detik = (Now.Subtract(mulaiw).TotalSeconds)
            Try
                h = detik.ToString.Remove(detik.ToString.LastIndexOf(","))
            Catch ex As Exception
            End Try

            LInfo.Text = "Data terakhir: Kendali=" + TBKendali.Text + ", NR=" + TBNoRumah.Text + ", NK=" + TBNoKeluarga.Text + ", KK=" + TBNama1.Text + "      Waktu 1 data: " + h + " detik."

            If Not TBNoRumah.Text = "9999" Then
                Dim isian As String()

                ReDim Preserve isian(3)

                isian(0) = TBNoRumah.Text
                isian(1) = TP2.Text
                isian(2) = TBLuasRumah.Text

                If Not norumahsama Then
                    If Not lPembangunan.Contains(isian) Then
                        lPembangunan.Add(isian)
                    End If
                End If

            End If

            lNoKeluarga.Add(TBNoKeluarga.Text)

            CBKRef.Items.Add(TBKendali.Text)
            CBKRef.SelectedIndex = 0

            BacaData()
            BersihkanControl()
        Else
            If IO.File.Exists(AlamatAppData + "\" + NamaBerkas) Then
                Using Berkas As New IO.StreamWriter(AlamatAppData + "\" + NamaBerkas)
                    Berkas.WriteLine("#Daftar Entri Data Keluarga")
                    Dim ii As Integer
                    For Each kendali As String In REF
                        If ii <> indeksdiubah Then
                            Berkas.WriteLine("[Mulai]")
                            Berkas.WriteLine(" [REF]" + kendali)
                            Berkas.WriteLine(" [KREF]" + KREF(ii))
                            Berkas.WriteLine("  [Kependudukan]")
                            Berkas.WriteLine("   [NR]" + NR(ii))
                            Berkas.WriteLine("   [NK]" + NK(ii))
                            Berkas.WriteLine("   [NIK1]" + NIK1(ii))
                            Berkas.WriteLine("    [NAMA1]" + NAMA1(ii))
                            Berkas.WriteLine("    [TL1]" + TL1(ii))
                            Berkas.WriteLine("    [KD1]" + KD1(ii))
                            Berkas.WriteLine("   [NIK2]" + NIK2(ii))
                            Berkas.WriteLine("    [NAMA2]" + NAMA2(ii))
                            Berkas.WriteLine("    [TL2]" + TL2(ii))
                            Berkas.WriteLine("    [KD2]" + KD2(ii))
                            Berkas.WriteLine("   [NIK3]" + NIK3(ii))
                            Berkas.WriteLine("    [NAMA3]" + NAMA3(ii))
                            Berkas.WriteLine("    [TL3]" + TL3(ii))
                            Berkas.WriteLine("    [KD3]" + KD3(ii))
                            Berkas.WriteLine("   [NIK4]" + NIK4(ii))
                            Berkas.WriteLine("    [NAMA4]" + NAMA4(ii))
                            Berkas.WriteLine("    [TL4]" + TL4(ii))
                            Berkas.WriteLine("    [KD4]" + KD4(ii))
                            Berkas.WriteLine("   [NIK5]" + NIK5(ii))
                            Berkas.WriteLine("    [NAMA5]" + NAMA5(ii))
                            Berkas.WriteLine("    [TL5]" + TL5(ii))
                            Berkas.WriteLine("    [KD5]" + KD5(ii))
                            Berkas.WriteLine("   [NIK6]" + NIK6(ii))
                            Berkas.WriteLine("    [NAMA6]" + NAMA6(ii))
                            Berkas.WriteLine("    [TL6]" + TL6(ii))
                            Berkas.WriteLine("    [KD6]" + KD6(ii))
                            Berkas.WriteLine("   [NIK7]" + NIK7(ii))
                            Berkas.WriteLine("    [NAMA7]" + NAMA7(ii))
                            Berkas.WriteLine("    [TL7]" + TL7(ii))
                            Berkas.WriteLine("    [KD7]" + KD7(ii))
                            Berkas.WriteLine("  [Keluarga Berencana]")
                            Berkas.WriteLine("   [UsiaKawinSuami]" + UsiaKawinSuami(ii))
                            Berkas.WriteLine("   [UsiaKawinIstri]" + UsiaKawinIstri(ii))
                            Berkas.WriteLine("   [JumlahAnak]" + JumlahAnak(ii))
                            Berkas.WriteLine("   [BerKB]" + BerKB(ii))
                            Berkas.WriteLine("   [MetodKontrasepsi]" + MetodKontrasepsi(ii))
                            Berkas.WriteLine("   [LamaTKB]" + LamaTKB(ii))
                            Berkas.WriteLine("   [LamaBKB]" + LamaBKB(ii))
                            Berkas.WriteLine("   [PnyaAnakLagi]" + PnyaAnakLagi(ii))
                            Berkas.WriteLine("   [AlasanTidakKB]" + AlasanTidakKB(ii))
                            Berkas.WriteLine("   [TmpatLayananKB]" + TmpatLayananKB(ii))
                            Berkas.WriteLine("  [Pembangunan Keluarga]")
                            Berkas.WriteLine("   [KodePembangunan]" + KodePembangunan(ii))
                            Berkas.WriteLine("   [Luas]" + Luas(ii))
                            Berkas.WriteLine("   [Orang]" + Orang(ii))
                            Berkas.WriteLine("[Akhir]")
                            Berkas.WriteLine("")
                        Else
                            Berkas.WriteLine("[Mulai]")
                            Berkas.WriteLine(" [REF]" + TBKendali.Text)
                            Berkas.WriteLine(" [KREF]" + CBKRef.Text)
                            Berkas.WriteLine("  [Kependudukan]")
                            Berkas.WriteLine("   [NR]" + TBNoRumah.Text)
                            Berkas.WriteLine("   [NK]" + TBNoKeluarga.Text)
                            Berkas.WriteLine("   [NIK1]" + TBNik1.Text)
                            Berkas.WriteLine("    [NAMA1]" + TBNama1.Text.ToLower)
                            Berkas.WriteLine("    [TL1]" + TBTanggal1.Text)
                            Berkas.WriteLine("    [KD1]" + TBKode1.Text)
                            Berkas.WriteLine("   [NIK2]" + TBNik2.Text)
                            Berkas.WriteLine("    [NAMA2]" + TBNama2.Text.ToLower)
                            Berkas.WriteLine("    [TL2]" + TBTanggal2.Text)
                            Berkas.WriteLine("    [KD2]" + TBKode2.Text)
                            Berkas.WriteLine("   [NIK3]" + TBNik3.Text)
                            Berkas.WriteLine("    [NAMA3]" + TBNama3.Text.ToLower)
                            Berkas.WriteLine("    [TL3]" + TBTanggal3.Text)
                            Berkas.WriteLine("    [KD3]" + TBKode3.Text)
                            Berkas.WriteLine("   [NIK4]" + TBNik4.Text)
                            Berkas.WriteLine("    [NAMA4]" + TBNama4.Text.ToLower)
                            Berkas.WriteLine("    [TL4]" + TBTanggal4.Text)
                            Berkas.WriteLine("    [KD4]" + TBKode4.Text)
                            Berkas.WriteLine("   [NIK5]" + TBNik5.Text)
                            Berkas.WriteLine("    [NAMA5]" + TBNama5.Text.ToLower)
                            Berkas.WriteLine("    [TL5]" + TBTanggal5.Text)
                            Berkas.WriteLine("    [KD5]" + TBKode5.Text)
                            Berkas.WriteLine("   [NIK6]" + TBNik6.Text)
                            Berkas.WriteLine("    [NAMA6]" + TBNama6.Text.ToLower)
                            Berkas.WriteLine("    [TL6]" + TBTanggal6.Text)
                            Berkas.WriteLine("    [KD6]" + TBKode6.Text)
                            Berkas.WriteLine("   [NIK7]" + TBNik7.Text)
                            Berkas.WriteLine("    [NAMA7]" + TBNama7.Text.ToLower)
                            Berkas.WriteLine("    [TL7]" + TBTanggal7.Text)
                            Berkas.WriteLine("    [KD7]" + TBKode7.Text)
                            Berkas.WriteLine("  [Keluarga Berencana]")
                            Berkas.WriteLine("   [UsiaKawinSuami]" + TBKawinSu.Text)
                            Berkas.WriteLine("   [UsiaKawinIstri]" + TBKawinIs.Text)
                            Berkas.WriteLine("   [JumlahAnak]" + TBJumlahAnak.Text)
                            Berkas.WriteLine("   [BerKB]" + TBKb.Text)
                            Berkas.WriteLine("   [MetodKontrasepsi]" + TBKontra.Text)
                            Berkas.WriteLine("   [LamaTKB]" + TBLamaTKb.Text)
                            Berkas.WriteLine("   [LamaBKB]" + TBLamaBKb.Text)
                            Berkas.WriteLine("   [PnyaAnakLagi]" + TBPunyaAnak.Text)
                            Berkas.WriteLine("   [AlasanTidakKB]" + TBAlasanTidakKb.Text)
                            Berkas.WriteLine("   [TmpatLayananKB]" + TBTmpatKb.Text)
                            Berkas.WriteLine("  [Pembangunan Keluarga]")
                            Berkas.WriteLine("   [KodePembangunan]" + TBPembangunan.Text)
                            Berkas.WriteLine("   [Luas]" + TBLuasRumah.Text)
                            Berkas.WriteLine("   [Orang]" + TBOrang.Text)
                            Berkas.WriteLine("[Akhir]")
                            Berkas.WriteLine("")
                        End If
                        ii += 1
                    Next
                End Using

                CBKendali.Focus()

                If KotakPesan.Show(Me, "Data sudah diubah, kembali ke mode Tambah ?", "Entri BKKBN", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) = KotakPesan.HasilPesan.Ya Then
                    CBKRef.SelectedIndex = 0
                    AturTampilan(False)
                End If
                BacaData()
            Else
                KotakPesan.Show(Me, "Berkas " + namafile + " hilang !", "Berkas Tidak Ditemukan", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Eror)
            End If
        End If
        hitungwaktu = False
        norumahsama = False
        keyboardHook.Stop()
        keyboardHook.Start()
    End Sub

    Dim lPembangunan As New List(Of String())
    Dim lNoKeluarga As New List(Of String)

    Private Sub MainkanMusik()
        If Not LagiAturIsi Then
            My.Computer.Audio.Play(CurDir() + "\Resources\perc-xylophone6-C6.wav", AudioPlayMode.Background)
        End If
    End Sub

    Private Sub MainkanMusikExtra()
        My.Computer.Audio.Play(CurDir() + "\Resources\click-oilcan1.wav", AudioPlayMode.Background)
    End Sub

    Private Sub MainkanMusikSelesai()
        My.Computer.Audio.Play(CurDir() + "\Resources\Complete.wav", AudioPlayMode.BackgroundLoop)
    End Sub

    Public Sub MainkanMusikEror()
        Dim a As Integer = 0
        While a < 1
            My.Computer.Audio.Play(CurDir() + "\Resources\Error.wav", AudioPlayMode.WaitToComplete)
            Threading.Thread.Sleep(1000)
        End While
    End Sub

    Private Sub TBRef_TextChanged(sender As Object, e As EventArgs) Handles TBKendali.TextChanged
        If TBKendali.TextLength = 8 Then
            TBNoRumah.Focus()
            MainkanMusik()
        End If
    End Sub

    Dim norumahsama As Boolean

    Private Sub TBNoRumah_TextChanged(sender As Object, e As EventArgs) Handles TBNoRumah.TextChanged
        If hitungwaktu = False Then
            mulaiw = Now
            hitungwaktu = True
        End If

        Dim sementara As String

        If TBNoRumah.TextLength = 4 Then
            norumahsama = False
            If lPembangunan.Count <> 0 Then
                For Each item As String() In lPembangunan
                    If item(0) = TBNoRumah.Text Then
                        If item(1).Substring(7, 1) = "0" Then
                            sementara = item(1).Substring(0, 7) + "2"
                            TP2.Text = sementara
                        Else
                            TP2.Text = item(1)
                        End If
                        TBLuasRumah.Text = item(2)
                        norumahsama = True
                    End If
                Next
            End If

            If Not norumahsama Then
                MainkanMusik()
                TP2.Text = ""
                TBLuasRumah.Text = ""
            End If

            TBNoKeluarga.Focus()
        End If
    End Sub

    Private Sub TBNoKeluarga_TextChanged(sender As Object, e As EventArgs) Handles TBNoKeluarga.TextChanged
        Dim totalnol As Integer = 0
        If TBNoKeluarga.Text.Contains(".") Then
            Dim sudah As Boolean = False
            For Each huruf As Char In NK(NK.Count - 1)
                If huruf = "0" And sudah = False Then
                    totalnol += 1
                Else
                    sudah = True
                End If
            Next

            Dim n As Integer = Integer.Parse(NK(NK.Count - 1))
            Dim nol As String = ""
            n += 1

            For index = 1 To totalnol
                nol += "0"
            Next
            TBNoKeluarga.Text = nol + n.ToString
        End If



        If TBNoKeluarga.TextLength = 3 Then
            T11.Focus()
            MainkanMusik()

            If lNoKeluarga.Contains(TBNoKeluarga.Text) And CBKRef.Text = "Tidak Ada" Then
                If Not BTambah.Text.Contains("Tambah") And TBNoKeluarga.Text <> NK(indeksdiubah) Then
                    KotakPesan.Show(Me, "Harap di bagian No Urut Keluarga di cek lagi, jika ternyata sudah benar, silakan abaikan pesan ini.", "Pengecek Eror", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    TBNoKeluarga.Focus()
                ElseIf BTambah.Text.Contains("Tambah")
                    KotakPesan.Show(Me, "Harap di bagian No Urut Keluarga di cek lagi, jika ternyata sudah benar, silakan abaikan pesan ini.", "Pengecek Eror", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                    TBNoKeluarga.Focus()
                End If
            End If
        End If
    End Sub

    Private Function tentukankelamin(nilai As String) As Integer
        If nilai.Length = 16 Then
            If Integer.Parse(nilai.Substring(6, 2)) > 40 Then
                Return 1
            Else
                Return 0
            End If
        End If
        Return 3
    End Function

    Private Function TentukanTL(nilai As String)
        Dim ta, b, th As Integer
        Dim hasil As String = ""
        If nilai.Length > 11 Then
            If Integer.Parse(nilai.Substring(6, 2)) > 40 Then
                ta = Integer.Parse(nilai.Substring(6, 2)) - 40
            Else
                ta = Integer.Parse(nilai.Substring(6, 2))
            End If

            b = Integer.Parse(nilai.Substring(8, 2))
            th = Integer.Parse(nilai.Substring(10, 2))

            If ta = 0 Or ta > 31 Then
                KotakPesan.Show(Me, "Periksa NIK yang kamu masukin (Tanggal) !, nik yang benar adalah (jika individu perempuan dan tanggal > 40 dan tanggal < 72) atau (jika laki laki dan tanggal > 0 dan tanggal < 32)." + Environment.NewLine + Environment.NewLine + "NB: Tanggal = digit ke 7, 2 dari nik" + Environment.NewLine + Environment.NewLine + "Atau, tanggal lahir di nik disamakan dengan kolom tanggal lahir.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                Exit Function
            End If

            If ta < 10 Then
                hasil = "0" + ta.ToString
            Else
                hasil = ta.ToString
            End If

            If b = 0 Or b > 12 Then
                KotakPesan.Show(Me, "Periksa NIK yang kamu masukin (Bulan) !, nik yang benar adalah (bulan > 0 dan bulan < 13)." + Environment.NewLine + Environment.NewLine + "NB: Bulan = digit ke 8, 2 dari nik" + Environment.NewLine + Environment.NewLine + "Atau, tanggal lahir di nik disamakan dengan kolom tanggal lahir.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                Exit Function
            End If

            If b < 10 Then
                hasil += "0" + b.ToString
            Else
                hasil += b.ToString
            End If

            If th > Now.Year.ToString.Substring(2, 2) And th < 15 Then
                KotakPesan.Show(Me, "Periksa NIK yang kamu masukin (Tahun) !, nik yang benar adalah (tahun > 1900 dan tahun < tahun_sekarang + 1 tahun)." + Environment.NewLine + Environment.NewLine + "NB: Tahun = digit ke 8, 2 dari nik" + Environment.NewLine + Environment.NewLine + "Atau, tanggal lahir di nik disamakan dengan kolom tanggal lahir.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
                Exit Function
            End If

            If th < 10 Then
                hasil += "0" + th.ToString
            Else
                hasil += th.ToString
            End If
            aktivpindah = False
        End If
        Return hasil
    End Function

    Dim kl As Integer()

    Private Sub TBNik1_TextChanged(sender As Object, e As EventArgs) Handles TBNik1.TextChanged
        If TBNik1.TextLength = 16 Then
            TBTanggal1.Text = TentukanTL(TBNik1.Text)
            kl(0) = tentukankelamin(TBNik1.Text)

            T11.Text = TBNik1.Text.Substring(0, 6)
            T12.Text = TBNik1.Text.Substring(6, 2)
            T13.Text = TBNik1.Text.Substring(8, 2)
            T14.Text = TBNik1.Text.Substring(10, 2)
            T15.Text = TBNik1.Text.Substring(12, 4)
        End If
    End Sub

    Private Sub TBNik2_TextChanged(sender As Object, e As EventArgs) Handles TBNik2.TextChanged
        If TBNik2.TextLength = 16 Then
            TBTanggal2.Text = TentukanTL(TBNik2.Text)
            kl(1) = tentukankelamin(TBNik2.Text)

            T21.Text = TBNik2.Text.Substring(0, 6)
            T22.Text = TBNik2.Text.Substring(6, 2)
            T23.Text = TBNik2.Text.Substring(8, 2)
            T24.Text = TBNik2.Text.Substring(10, 2)
            T25.Text = TBNik2.Text.Substring(12, 4)
        End If
    End Sub

    Private Sub TBNik3_TextChanged(sender As Object, e As EventArgs) Handles TBNik3.TextChanged
        If TBNik3.TextLength = 16 Then
            TBTanggal3.Text = TentukanTL(TBNik3.Text)
            kl(2) = tentukankelamin(TBNik3.Text)

            T31.Text = TBNik3.Text.Substring(0, 6)
            T32.Text = TBNik3.Text.Substring(6, 2)
            T33.Text = TBNik3.Text.Substring(8, 2)
            T34.Text = TBNik3.Text.Substring(10, 2)
            T35.Text = TBNik3.Text.Substring(12, 4)
        End If
    End Sub

    Private Sub TBNik4_TextChanged(sender As Object, e As EventArgs) Handles TBNik4.TextChanged
        If TBNik4.TextLength = 16 Then
            TBTanggal4.Text = TentukanTL(TBNik4.Text)
            kl(3) = tentukankelamin(TBNik4.Text)

            T41.Text = TBNik4.Text.Substring(0, 6)
            T42.Text = TBNik4.Text.Substring(6, 2)
            T43.Text = TBNik4.Text.Substring(8, 2)
            T44.Text = TBNik4.Text.Substring(10, 2)
            T45.Text = TBNik4.Text.Substring(12, 4)
        End If
    End Sub

    Private Sub TBNik5_TextChanged(sender As Object, e As EventArgs) Handles TBNik5.TextChanged
        If TBNik5.TextLength = 16 Then
            TBTanggal5.Text = TentukanTL(TBNik5.Text)
            kl(4) = tentukankelamin(TBNik5.Text)

            T51.Text = TBNik5.Text.Substring(0, 6)
            T52.Text = TBNik5.Text.Substring(6, 2)
            T53.Text = TBNik5.Text.Substring(8, 2)
            T54.Text = TBNik5.Text.Substring(10, 2)
            T55.Text = TBNik5.Text.Substring(12, 4)
        End If
    End Sub

    Private Sub TBNik6_TextChanged(sender As Object, e As EventArgs) Handles TBNik6.TextChanged
        If TBNik6.TextLength = 16 Then
            TBTanggal6.Text = TentukanTL(TBNik6.Text)
            kl(5) = tentukankelamin(TBNik6.Text)

            T61.Text = TBNik6.Text.Substring(0, 6)
            T62.Text = TBNik6.Text.Substring(6, 2)
            T63.Text = TBNik6.Text.Substring(8, 2)
            T64.Text = TBNik6.Text.Substring(10, 2)
            T65.Text = TBNik6.Text.Substring(12, 4)
        End If
    End Sub

    Private Sub TBNik7_TextChanged(sender As Object, e As EventArgs) Handles TBNik7.TextChanged
        If TBNik7.TextLength = 16 Then
            TBTanggal7.Text = TentukanTL(TBNik7.Text)
            kl(6) = tentukankelamin(TBNik7.Text)

            T71.Text = TBNik7.Text.Substring(0, 6)
            T72.Text = TBNik7.Text.Substring(6, 2)
            T73.Text = TBNik7.Text.Substring(8, 2)
            T74.Text = TBNik7.Text.Substring(10, 2)
            T75.Text = TBNik7.Text.Substring(12, 4)
        End If
    End Sub

    Dim aktivpindah As Boolean = True

    Private Sub TBTanggal1_TextChanged(sender As Object, e As EventArgs) Handles TBTanggal1.TextChanged
        Try
            LU1.Text = TentukanUmur(TBTanggal1.Text)
        Catch ex As Exception
            LU1.Text = "0"
        End Try

        If TBTanggal1.TextLength = 6 And aktivpindah = True Then
            MainkanMusik()
            PosisikanTanggal()
        End If
        aktivpindah = True
    End Sub

    Private Sub TBTanggal2_TextChanged(sender As Object, e As EventArgs) Handles TBTanggal2.TextChanged
        Try
            LU2.Text = TentukanUmur(TBTanggal2.Text)
        Catch ex As Exception
            LU2.Text = "0"
        End Try

        If TBTanggal2.TextLength = 6 And aktivpindah = True Then
            MainkanMusik()
            PosisikanTanggal()
        End If
        aktivpindah = True
    End Sub

    Private Sub TBTanggal3_TextChanged(sender As Object, e As EventArgs) Handles TBTanggal3.TextChanged
        Try
            LU3.Text = TentukanUmur(TBTanggal3.Text)
        Catch ex As Exception
            LU3.Text = "0"
        End Try

        If TBTanggal3.TextLength = 6 And aktivpindah = True Then
            MainkanMusik()
            PosisikanTanggal()
        End If
        aktivpindah = True
    End Sub

    Private Sub TBTanggal4_TextChanged(sender As Object, e As EventArgs) Handles TBTanggal4.TextChanged
        Try
            LU4.Text = TentukanUmur(TBTanggal4.Text)
        Catch ex As Exception
            LU4.Text = "0"
        End Try

        If TBTanggal4.TextLength = 6 And aktivpindah = True Then
            MainkanMusik()
            PosisikanTanggal()
        End If
        aktivpindah = True
    End Sub

    Private Sub TBTanggal5_TextChanged(sender As Object, e As EventArgs) Handles TBTanggal5.TextChanged
        Try
            LU5.Text = TentukanUmur(TBTanggal5.Text)
        Catch ex As Exception
            LU5.Text = "0"
        End Try

        If TBTanggal5.TextLength = 6 And aktivpindah = True Then
            MainkanMusik()
            PosisikanTanggal()
        End If
        aktivpindah = True
    End Sub

    Private Sub TBTanggal6_TextChanged(sender As Object, e As EventArgs) Handles TBTanggal6.TextChanged
        Try
            LU6.Text = TentukanUmur(TBTanggal6.Text)
        Catch ex As Exception
            LU6.Text = "0"
        End Try

        If TBTanggal6.TextLength = 6 And aktivpindah = True Then
            MainkanMusik()
            PosisikanTanggal()
        End If
        aktivpindah = True
    End Sub

    Private Sub TBTanggal7_TextChanged(sender As Object, e As EventArgs) Handles TBTanggal7.TextChanged
        Try
            LU7.Text = TentukanUmur(TBTanggal7.Text)
        Catch ex As Exception
            LU7.Text = "0"
        End Try

        If TBTanggal7.TextLength = 6 And aktivpindah = True Then
            PosisikanTanggal()
            MainkanMusik()
        End If
        aktivpindah = True
    End Sub

    Private Sub TBKode1_TextChanged(sender As Object, e As EventArgs) Handles TBKode1.TextChanged
        If TBKode1.TextLength = 7 Then
            MainkanMusik()
            PosisikanKode()
        End If
    End Sub

    Private Sub TBKode2_TextChanged(sender As Object, e As EventArgs) Handles TBKode2.TextChanged
        If TBKode2.TextLength = 7 Then
            MainkanMusik()
            PosisikanKode()
        End If
    End Sub

    Private Sub TBKode3_TextChanged(sender As Object, e As EventArgs) Handles TBKode3.TextChanged
        If TBKode3.TextLength = 7 Then
            MainkanMusik()
            PosisikanKode()
        End If
    End Sub

    Private Sub TBKode4_TextChanged(sender As Object, e As EventArgs) Handles TBKode4.TextChanged
        If TBKode4.TextLength = 7 Then
            MainkanMusik()
            PosisikanKode()
        End If
    End Sub

    Private Sub TBKode5_TextChanged(sender As Object, e As EventArgs) Handles TBKode5.TextChanged
        If TBKode5.TextLength = 7 Then
            MainkanMusik()
            PosisikanKode()
        End If
    End Sub

    Private Sub TBKode6_TextChanged(sender As Object, e As EventArgs) Handles TBKode6.TextChanged
        If TBKode6.TextLength = 7 Then
            MainkanMusik()
            PosisikanKode()
        End If
    End Sub

    Private Sub TBKode7_TextChanged(sender As Object, e As EventArgs) Handles TBKode7.TextChanged
        If TBKode7.TextLength = 7 Then
            PosisikanKode()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBKawinSu_TextChanged(sender As Object, e As EventArgs) Handles TBKawinSu.TextChanged
        If TBKawinSu.TextLength = 2 Then
            TBKawinIs.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBKawinIs_TextChanged(sender As Object, e As EventArgs) Handles TBKawinIs.TextChanged
        If TBKawinIs.TextLength = 2 Then
            TBJumlahAnak.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBJumlahAnak_TextChanged(sender As Object, e As EventArgs) Handles TBJumlahAnak.TextChanged
        If TBJumlahAnak.TextLength = 8 Then
            TBKb.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBKb_TextChanged(sender As Object, e As EventArgs) Handles TBKb.TextChanged
        If TBKb.TextLength = 1 Then
            TBKontra.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBKontra_TextChanged(sender As Object, e As EventArgs) Handles TBKontra.TextChanged
        If TBKontra.TextLength = 1 Then
            TBLamaTKb.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBLamaTKb_TextChanged(sender As Object, e As EventArgs) Handles TBLamaTKb.TextChanged
        If TBLamaTKb.TextLength = 2 Then
            TBLamaBKb.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBLamaBKb_TextChanged(sender As Object, e As EventArgs) Handles TBLamaBKb.TextChanged
        If TBLamaBKb.TextLength = 2 Then
            TBPunyaAnak.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBPunyaAnak_TextChanged(sender As Object, e As EventArgs) Handles TBPunyaAnak.TextChanged
        If TBPunyaAnak.TextLength = 1 Then
            TBAlasanTidakKb.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBAlasanTidakKb_TextChanged(sender As Object, e As EventArgs) Handles TBAlasanTidakKb.TextChanged
        If TBAlasanTidakKb.TextLength = 1 Then
            TBTmpatKb.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBPembangunan_TextChanged(sender As Object, e As EventArgs) Handles TBPembangunan.TextChanged
        If TBPembangunan.TextLength = 26 Then
            TP1.Text = TBPembangunan.Text.Substring(0, 18)
            TP2.Text = TBPembangunan.Text.Substring(18, 8)
        End If
        LLeng.Text = TBPembangunan.TextLength
    End Sub

    Private Sub TBLuasRumah_TextChanged(sender As Object, e As EventArgs) Handles TBLuasRumah.TextChanged
        If TBLuasRumah.TextLength = 4 Then
            TBOrang.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub TBOrang_TextChanged(sender As Object, e As EventArgs) Handles TBOrang.TextChanged
        If TBOrang.TextLength = 2 Then
            BTambah.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Function AdaBalita() As Boolean
        Dim tanggall As New List(Of String)
        Dim indeks As Integer = 1

        tanggall.Add(TL1(a))
        tanggall.Add(TL2(a))
        tanggall.Add(TL3(a))
        tanggall.Add(TL4(a))
        tanggall.Add(TL5(a))
        tanggall.Add(TL6(a))
        tanggall.Add(TL7(a))

        For Each item As String In tanggall
            If item <> "" Then
                If TentukanUmur(item) < 6 Then
                    Return True
                End If
            End If
        Next

        Return False
    End Function

    Dim lhook As String

    Private Sub Jeda(lama As Integer)
        System.Threading.Thread.Sleep(lama)
    End Sub
    Public interpal As Integer = 150
    Private Sub Tekan(tombol As Keys, Optional sleep As Boolean = True)
        KeyboardSimulator.KeyDown(tombol)
        KeyboardSimulator.KeyUp(tombol)
        lhook = tombol.ToString
        If sleep Then
            Jeda(interpal)
        End If
    End Sub

    Private Sub IsiNilai(nilai As String)
        teksinfo = nilai
        For Each huruf As Char In nilai
            If huruf <> "'" Then
                Tekan(Asc(huruf.ToString.ToUpper), False)
            Else
                Tekan(Keys.Oem7)
            End If
        Next
    End Sub

    Private Sub IsiNR(nilai As String)
        teksinfo = nilai
        PergiKeCell("AO4")
        IsiNilai(nilai)
    End Sub

    Private Sub IsiTanggal(nilai As String)
        teksinfo = nilai
        Dim a As Integer
        Try
            a = Integer.Parse(nilai.Substring(4, 2))
            IsiNilai(nilai.Substring(0, 2))
            Tekan(Keys.Right)
            IsiNilai(nilai.Substring(2, 2))
            Tekan(Keys.Right)
            Dim tahun As String = Now.Year
            Dim dgit2 As Integer = Integer.Parse(tahun.Substring(2, 2))
            If a <= dgit2 Then
                IsiNilai("20" + nilai.Substring(4, 2))
            Else
                IsiNilai("19" + a.ToString)
            End If
            Tekan(Keys.Right)
        Catch ex As Exception
        End Try
    End Sub

    Dim sudahpindah As Boolean = False

    Private Sub IsiAnggotaKeluarga(nik As String, nama As String, tanggal As String)
        teksinfo = nik
        If Not sudahpindah Then
            IsiNilai(nik)
            Tekan(Keys.Right)
        End If

        teksinfo = nama
        If nama <> "" Then
            sudahpindah = False
            IsiNilai(nama)
            Tekan(Keys.Right)
        Else
            If sudahpindah = False Then
                Tekan(Keys.Right)
                PergiKeCell("C30")
                sudahpindah = True
            End If
            Exit Sub
        End If

        teksinfo = tanggal
        If tanggal <> "" Then
            IsiTanggal(tanggal)
        End If
    End Sub

    Dim posisikan As Boolean = False

    Private Sub PergiKeCell(pos As String)
        teksinfo = "Pergi ke cell: " + pos
        Tekan(Keys.F5)
        IsiNilai(pos)
        Tekan(Keys.Enter)
    End Sub

    Dim offbaris As String() = {"30", "32", "34", "36", "38", "40", "42"}
    Dim offkol As String() = {"C", "H", "K", "S", "AD", "AO", "AS"}

    Private Sub IsiKodeAnggota(nilai As String, off As Integer)
        teksinfo = nilai
        If nilai <> "" Then
            For index = 0 To 6
                For index2 = 0 To Integer.Parse(nilai.Substring(index, 1)) - 1
                    Tekan(Keys.Right)
                Next
                Tekan(Keys.X)
                Tekan(Keys.Right)
                Try
                    PergiKeCell(offkol(index + 1) + offbaris(off))
                Catch ex As Exception
                    Try
                        PergiKeCell(offkol(0) + offbaris(off + 1))
                    Catch ex1 As Exception
                        posisikan = True
                    End Try
                End Try
            Next
        Else
            posisikan = True
        End If
    End Sub

    Private Sub IsiBerKB(nilai As String)
        Dim daftar As List(Of String) = New List(Of String)
        Dim daftarkawin As List(Of String) = New List(Of String)
        Try
            daftar.Add(KD1(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD2(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD3(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD4(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD5(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD6(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD7(a))
        Catch ex As Exception
        End Try

        Tekan(Keys.Right)
        For Each item As String In daftar
            If item <> "" Then
                If item.Substring(0, 1) = 0 And item.Substring(5, 1) > 0 Then
                    PergiKeCell("C61")
                    teksinfo = nilai
                    For index = 0 To Integer.Parse(nilai) - 1
                        Tekan(Keys.Right)
                    Next
                    Tekan(Keys.X)
                End If
            End If
        Next
    End Sub

    Private Sub IsiKontra(nilai As String)
        Tekan(Keys.Right)
        PergiKeCell("C66")
        teksinfo = nilai
        For index = 0 To Integer.Parse(nilai) - 1
            Tekan(Keys.Right)
        Next
        Tekan(Keys.X)
    End Sub

    Private Sub IsiPengenAnak(nilai As String)
        Tekan(Keys.Right)
        PergiKeCell("C76")
        teksinfo = nilai
        For index = 0 To Integer.Parse(nilai) - 1
            Tekan(Keys.Right)
        Next
        Tekan(Keys.X)
    End Sub

    Private Sub IsiAlasan(nilai As String)
        Tekan(Keys.Right)
        PergiKeCell("C81")
        teksinfo = nilai
        If nilai = 0 Then
            Tekan(Keys.X)
        ElseIf nilai = 1
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = 2
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = 3
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = 4
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = 5
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = 6
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = 7
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        End If
        Tekan(Keys.Left)
    End Sub

    Private Sub IsiPelayananKB(nilai As String)
        Tekan(Keys.Right)
        PergiKeCell("C88")
        teksinfo = nilai
        If nilai = "0" Then
            Tekan(Keys.X)
        ElseIf nilai = "1" Then
            Tekan(Keys.Right)
            Tekan(Keys.Down)
            Tekan(Keys.X)
        ElseIf nilai = "2" Then
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Down)
            Tekan(Keys.X)
        ElseIf nilai = "3" Then
            Tekan(Keys.Down)
            Tekan(Keys.X)
        ElseIf nilai = "4" Then
            Tekan(Keys.Right)
            Tekan(Keys.Down)
            Tekan(Keys.Down)
            Tekan(Keys.X)
        ElseIf nilai = "5" Then
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Down)
            Tekan(Keys.Down)
            Tekan(Keys.X)
        ElseIf nilai = "6" Then
            Tekan(Keys.Down)
            Tekan(Keys.Down)
            Tekan(Keys.X)
        ElseIf nilai = "7" Then
            Tekan(Keys.Down)
            Tekan(Keys.Down)
            Tekan(Keys.Down)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = "8" Then
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = "9" Then
            Tekan(Keys.Down)
            Tekan(Keys.Down)
            Tekan(Keys.Down)
            Tekan(Keys.X)
        ElseIf nilai = "10" Then
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = "11" Then
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Down)
            Tekan(Keys.X)
        ElseIf nilai = "12" Then
            Tekan(Keys.Right)
            Tekan(Keys.X)
        ElseIf nilai = "13" Then
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Right)
            Tekan(Keys.Down)
            Tekan(Keys.Down)
            Tekan(Keys.X)
        End If
        Tekan(Keys.Left)
    End Sub

    Private Function AdaRemaja() As Boolean
        Dim hasil As Boolean = False
        Dim daftar As List(Of List(Of String)) = New List(Of List(Of String))
        Dim umur As List(Of Integer) = New List(Of Integer)
        Dim hub As List(Of String) = New List(Of String)
        Dim status As List(Of String) = New List(Of String)
        Dim tahun As List(Of Integer) = New List(Of Integer)

        Try
            umur.Add(TentukanUmur(TL1(a)))
            hub.Add(KD1(a).Substring(0, 1))
            status.Add(KD1(a).Substring(5, 1))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL2(a)))
            hub.Add(KD2(a).Substring(0, 1))
            status.Add(KD2(a).Substring(5, 1))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL3(a)))
            hub.Add(KD3(a).Substring(0, 1))
            status.Add(KD3(a).Substring(5, 1))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL4(a)))
            hub.Add(KD4(a).Substring(0, 1))
            status.Add(KD4(a).Substring(5, 1))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL5(a)))
            hub.Add(KD5(a).Substring(0, 1))
            status.Add(KD5(a).Substring(5, 1))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL6(a)))
            hub.Add(KD6(a).Substring(0, 1))
            status.Add(KD6(a).Substring(5, 1))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL7(a)))
            hub.Add(KD7(a).Substring(0, 1))
            status.Add(KD7(a).Substring(5, 1))
        Catch ex As Exception
        End Try

        Dim ind As Integer = 0
        For Each w As Integer In umur
            If w > 9 And w < 25 And hub(ind) >= 0 And status(ind) = 0 Then
                Return True
            End If
            ind += 1
        Next
        'MessageBox.Show("asdasd" + 123)
        Return hasil
    End Function

    Private Function AdaLansia() As Boolean
        Dim hasil As Boolean = False
        Dim daftar As List(Of List(Of String)) = New List(Of List(Of String))
        Dim umur As List(Of Integer) = New List(Of Integer)
        Dim tahun As List(Of Integer) = New List(Of Integer)

        Try
            umur.Add(TentukanUmur(TL1(a)))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL2(a)))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL3(a)))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL4(a)))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL5(a)))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL6(a)))
        Catch ex As Exception
        End Try
        Try
            umur.Add(TentukanUmur(TL7(a)))
        Catch ex As Exception
        End Try

        For Each w As Integer In umur
            If w > 60 Then
                Return True
            End If
        Next

        Return hasil
    End Function

    Private Function UsiaSubur() As Boolean
        Dim daftar As List(Of String) = New List(Of String)
        Dim daftartl As List(Of List(Of String)) = New List(Of List(Of String))
        Dim statuskawin As List(Of String) = New List(Of String)
        Try
            daftar.Add(KD1(a))
            daftartl.Add(TL1)
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD2(a))
            daftartl.Add(TL2)
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD3(a))
            daftartl.Add(TL3)
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD4(a))
            daftartl.Add(TL4)
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD5(a))
            daftartl.Add(TL5)
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD6(a))
            daftartl.Add(TL6)
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD7(a))
            daftartl.Add(TL7)
        Catch ex As Exception
        End Try

        Dim adaistri As Boolean = False
        Dim adakawin As Boolean = False
        Dim i As Integer = 0
        For Each status As String In daftar
            If status <> "" Then
                If status.Substring(0, 1) = "1" Then
                    adaistri = True
                    Exit For
                End If
            End If
            i += 1
        Next
        If adaistri = False Then
            Return False
        End If

        Dim b As Integer = 0
        For Each status As String In daftar
            If status <> "" Then
                If status.Substring(5, 1) = "1" Then
                    adakawin = True
                    Exit For
                End If
            End If
            b += 1
        Next
        If adakawin = False Then
            Return False
        End If

        If TentukanUmur(daftartl(i)(a)) < 50 Then
            Return True
        Else
            Return False
        End If

        Return False
    End Function

    Private Function AdaNikahorJanda() As Boolean
        Dim hasil As Boolean = False
        Dim daftar As List(Of Integer) = New List(Of Integer)

        If KD1(a) <> "" Then
            daftar.Add(KD1(a).Substring(5, 1))
        End If
        If KD2(a) <> "" Then
            daftar.Add(KD2(a).Substring(5, 1))
        End If
        If KD3(a) <> "" Then
            daftar.Add(KD3(a).Substring(5, 1))
        End If
        If KD4(a) <> "" Then
            daftar.Add(KD4(a).Substring(5, 1))
        End If
        If KD5(a) <> "" Then
            daftar.Add(KD5(a).Substring(5, 1))
        End If
        If KD6(a) <> "" Then
            daftar.Add(KD6(a).Substring(5, 1))
        End If
        If KD7(a) <> "" Then
            daftar.Add(KD7(a).Substring(5, 1))
        End If

        hasil = False
        For index = 1 To daftar.Count
            If daftar(index - 1) > 0 Then
                hasil = True
            End If
        Next

        Return hasil
    End Function

    Private Function AdaKawin() As Boolean
        Dim hasil As Boolean = False
        Dim daftar As List(Of Integer) = New List(Of Integer)

        If KD1(a) <> "" Then
            daftar.Add(KD1(a).Substring(5, 1))
        End If
        If KD2(a) <> "" Then
            daftar.Add(KD2(a).Substring(5, 1))
        End If
        If KD3(a) <> "" Then
            daftar.Add(KD3(a).Substring(5, 1))
        End If
        If KD4(a) <> "" Then
            daftar.Add(KD4(a).Substring(5, 1))
        End If
        If KD5(a) <> "" Then
            daftar.Add(KD5(a).Substring(5, 1))
        End If
        If KD6(a) <> "" Then
            daftar.Add(KD6(a).Substring(5, 1))
        End If
        If KD7(a) <> "" Then
            daftar.Add(KD7(a).Substring(5, 1))
        End If

        hasil = False
        For index = 1 To daftar.Count
            If daftar(index - 1) = 1 Then
                hasil = True
            End If
        Next

        Return hasil
    End Function

    Private Sub IsiKawin()
        Dim daftar, tanggall As New List(Of String)
        Dim suami, istri, anak, lamausia As Integer
        Dim indeks As Integer = 1
        daftar.Add(KD1(a))
        daftar.Add(KD2(a))
        daftar.Add(KD3(a))
        daftar.Add(KD4(a))
        daftar.Add(KD5(a))
        daftar.Add(KD6(a))
        daftar.Add(KD7(a))

        tanggall.Add(TL1(a))
        tanggall.Add(TL2(a))
        tanggall.Add(TL3(a))
        tanggall.Add(TL4(a))
        tanggall.Add(TL5(a))
        tanggall.Add(TL6(a))
        tanggall.Add(TL7(a))

        Dim carianak As Boolean = True

        For Each kode As String In daftar
            If kode <> "" Then
                If kode.Substring(0, 1) = 0 And kode.Substring(1, 1) = 0 And kode.Substring(5, 1) > 0 Then
                    suami = indeks
                ElseIf kode.Substring(0, 1) < 2 And kode.Substring(1, 1) = 1 And kode.Substring(5, 1) > 0 Then
                    istri = indeks
                ElseIf kode.Substring(0, 1) = 2 Then

                    If carianak Then
                        anak = indeks
                        carianak = False
                    End If
                End If
            End If
            indeks += 1
        Next

        If suami > 0 Then
            lamausia = Integer.Parse(UsiaKawinSuami(a))
            If lamausia = 0 Then
                If anak > 0 Then
                    lamausia = TentukanUmur(tanggall(suami - 1)) - (TentukanUmur(tanggall(anak - 1)) * 2)
                    If lamausia < 10 Then
                        lamausia = TentukanUmur(tanggall(suami - 1)) - (TentukanUmur(tanggall(anak - 1)))
                        If lamausia < 10 Then
                            lamausia = 99
                        End If
                    End If
                Else
                    lamausia = 99
                End If
            End If
            IsiNilai(lamausia)
            If istri > 0 Then
                Tekan(Keys.Right)
            End If
        End If

        If istri > 0 Then
            lamausia = Integer.Parse(UsiaKawinIstri(a))
            If lamausia = 0 Then
                If anak > 0 Then
                    lamausia = TentukanUmur(tanggall(istri - 1)) - (TentukanUmur(tanggall(anak - 1)) * 2)
                    If lamausia < 10 Then
                        lamausia = TentukanUmur(tanggall(istri - 1)) - (TentukanUmur(tanggall(anak - 1)))
                        If lamausia < 10 Then
                            lamausia = 99
                        End If
                    End If
                Else
                    lamausia = 99
                End If
            End If
            IsiNilai(lamausia)
        End If
        'MessageBox.Show("asdas" + 123)
    End Sub

    Private Sub IsiAnak()
        Dim daftar As List(Of String) = New List(Of String)
        Dim adanikahperempuan, adajandaperempuan, adakkjandalaki, adanikahlaki As Boolean
        Try
            daftar.Add(KD1(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD2(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD3(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD4(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD5(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD6(a))
        Catch ex As Exception
        End Try
        Try
            daftar.Add(KD7(a))
        Catch ex As Exception
        End Try

        Dim b As Integer = 0
        For Each status As String In daftar
            If status <> "" Then
                If status.Substring(0, 1) = "0" And status.Substring(1, 1) = "0" And status.Substring(5, 1) = "2" Then
                    adakkjandalaki = True
                End If

                If status.Substring(1, 1) = "1" And status.Substring(5, 1) = "1" Then
                    adanikahperempuan = True
                ElseIf status.Substring(1, 1) = "1" And status.Substring(5, 1) = "2" Then
                    adajandaperempuan = True
                ElseIf status.Substring(1, 1) = "0" And status.Substring(5, 1) = "1"
                    adanikahlaki = True
                End If
            End If
            b += 1
        Next

        If Not adakkjandalaki Then
            If adanikahperempuan Or adajandaperempuan Or adanikahlaki Then
                Tekan(Keys.Right)
                PergiKeCell("M55")
                Dim x, y, x1, y1 As Integer

                x = JumlahAnak(a).Substring(0, 2)
                y = JumlahAnak(a).Substring(2, 2)
                x1 = JumlahAnak(a).Substring(4, 2)
                y1 = JumlahAnak(a).Substring(6, 2)

                If x1 > x Then
                    x1 = x
                End If
                If y1 > y Then
                    y1 = y
                End If

                IsiNilai(x)
                Tekan(Keys.Right)
                IsiNilai(y)
                Tekan(Keys.Right)
                IsiNilai(x1)
                Tekan(Keys.Right)
                IsiNilai(y1)
            End If
        End If
    End Sub

    Private Sub IsiPembangunan(nilai As String)
        PergiKeCell("AM99")
        teksinfo = nilai
        Dim pilihan As Integer() = {1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 3, 3, 3, 3, 3, 3, 4, 3}
        Dim posisi As Integer = 0
        Dim geser As Integer
        Dim kondisi As Boolean
        Dim jumlahan As Integer = Integer.Parse(JumlahAnak(a).Substring(4, 2)) + Integer.Parse(JumlahAnak(a).Substring(6, 2))

        For index = 0 To 25
            If index = 6 Then
                If Not UsiaSubur() Or jumlahan < 2 Then
                    kondisi = True
                End If
            ElseIf (index = 12 Or index = 13)
                If Not AdaBalita() Then
                    kondisi = True
                End If
            ElseIf (index = 14 Or index = 15)
                If Not AdaRemaja() Then
                    kondisi = True
                End If
            ElseIf (index = 16)
                If Not AdaLansia() Then
                    kondisi = True
                End If
            ElseIf (index = 18)
                Tekan(Keys.Left)
                Tekan(Keys.Left)
                Tekan(Keys.Left)
            End If
            If Not kondisi Then
                geser = 0
                Dim i As Integer = nilai.Substring(index, 1)

                If index = 6 Then
                    If i = 2 Then
                        i = 1
                    End If
                End If

                If index > 11 And index < 17 Then
                    If i = 2 Then
                        i = 1
                    End If
                End If

                Do While geser < i
                    Tekan(Keys.Right)
                    posisi += 1
                    geser += 1
                Loop
                Tekan(Keys.X)
                If posisi = 1 Then
                    Tekan(Keys.Left)
                ElseIf posisi = 2
                    Tekan(Keys.Left)
                    Tekan(Keys.Left)
                ElseIf posisi = 3
                    Tekan(Keys.Left)
                    Tekan(Keys.Left)
                    Tekan(Keys.Left)
                End If
                posisi = 0
                Tekan(Keys.Down)
            Else
                'skip
                If debug = True Then
                    Tekan(Keys.Down)
                End If
                kondisi = False
            End If
        Next
    End Sub

    Dim debug As Boolean = False
    Dim kedua As Boolean = False
    Dim lamawaktu As String

    Public Sub Proses()
        Dim mulai As DateTime = Now
        For index = a To REF.Count - 1
            pengubah = 0
            If Not debug Then
                sudahpindah = False
                Jeda(500)
                Tekan(Keys.Escape)
                Application.DoEvents()
                IsiNR(NR(a))
                Tekan(Keys.Right)
                IsiNilai(NK(a))
                Tekan(Keys.Right)
                If KREF(a) <> "Tidak Ada" Then
                    IsiNilai(KREF(a))
                End If
                Tekan(Keys.Right)

                IsiAnggotaKeluarga(NIK1(a), NAMA1(a), TL1(a))
                pengubah = 1
                IsiAnggotaKeluarga(NIK2(a), NAMA2(a), TL2(a))
                IsiAnggotaKeluarga(NIK3(a), NAMA3(a), TL3(a))
                pengubah = 2
                IsiAnggotaKeluarga(NIK4(a), NAMA4(a), TL4(a))
                IsiAnggotaKeluarga(NIK5(a), NAMA5(a), TL5(a))
                pengubah = 3
                IsiAnggotaKeluarga(NIK6(a), NAMA6(a), TL6(a))
                IsiAnggotaKeluarga(NIK7(a), NAMA7(a), TL7(a))

                IsiKodeAnggota(KD1(a), 0)
                IsiKodeAnggota(KD2(a), 1)
                pengubah = 4
                IsiKodeAnggota(KD3(a), 2)
                IsiKodeAnggota(KD4(a), 3)
                pengubah = 5
                IsiKodeAnggota(KD5(a), 4)
                IsiKodeAnggota(KD6(a), 5)
                pengubah = 6
                IsiKodeAnggota(KD7(a), 6)
                If posisikan Then
                    PergiKeCell("ax44")
                End If

                IsiNilai(REF(a))
                Tekan(Keys.Right)
                pengubah = 7

                If KREF(a) = "Tidak Ada" Then
                    If AdaNikahorJanda() Then
                        IsiKawin()
                        IsiAnak()
                        If AdaKawin() Then
                        End If
                        If BerKB(a) = 0 Then
                            IsiBerKB(0)
                            IsiKontra(MetodKontrasepsi(a))
                            Tekan(Keys.Right)
                            PergiKeCell("C71")
                            If LamaTKB(a) = "00" Then
                                IsiNilai("99")
                            Else
                                IsiNilai(LamaTKB(a))
                            End If
                            Tekan(Keys.Right)
                            If LamaBKB(a) = "00" Then
                                IsiNilai("99")
                            Else
                                IsiNilai(LamaBKB(a))
                            End If
                            pengubah = 8
                            IsiPengenAnak(PnyaAnakLagi(a))
                            IsiPelayananKB(TmpatLayananKB(a))
                        ElseIf BerKB(a) = 1 Then
                            IsiBerKB(1)
                            IsiKontra(MetodKontrasepsi(a))
                            IsiPengenAnak(PnyaAnakLagi(a))
                            IsiAlasan(AlasanTidakKB(a))
                            pengubah = 8
                        ElseIf BerKB(a) = 2 Then
                            IsiBerKB(2)
                            IsiPengenAnak(PnyaAnakLagi(a))
                            IsiAlasan(AlasanTidakKB(a))
                            pengubah = 8
                        End If
                    Else
                        PergiKeCell("AM92")
                    End If
                End If
            End If

            pengubah = 9
            If KREF(a) = "Tidak Ada" Then
                IsiPembangunan(KodePembangunan(a))
                pengubah = 10
                IsiNilai(Luas(a))
                Tekan(Keys.Right)
                IsiNilai(Orang(a))
                Tekan(Keys.Right)
            End If

            teksinfo = "Simpan"
            If Not adaeror Then
                teksinfo = "Error ?, perbaiki kemudian simpan, kami"
                lhook = "akan melanjutkan"
            End If
            'trkonfirmasi1 = New Threading.Thread(AddressOf adakonfirmasi)
            'trkonfirmasi1.Start()
            'wait()
            adakonfirmasi()
            teksinfo = "Simpan"
            Jeda(200)
            Tekan(Keys.Space)
            'trkonfirmasi2 = New Threading.Thread(AddressOf adakonfirmasi2)
            'trkonfirmasi2.Start()
            'wait()
            adakonfirmasi2()
            Jeda(200)
            Tekan(Keys.Space)
            kedua = True
            a += 1
        Next

        Dim menit, detik As Double
        Dim h, h2 As String
        h = ""
        h2 = ""

        menit = (Now.Subtract(mulai).TotalSeconds) / 60
        detik = (Now.Subtract(mulai).TotalSeconds) Mod 60
        Try
            h = menit.ToString.Remove(menit.ToString.LastIndexOf(","))
            h2 = detik.ToString.Remove(detik.ToString.LastIndexOf(","))
        Catch ex As Exception
        End Try

        lhook = h + " menit, " + h2 + " detik"
        teksinfo = "Sudah Selesai, waktu: "
        MainkanMusikSelesai()
    End Sub

    Public tr As Threading.Thread

    Private Sub BDownExcel_Click(sender As Object, e As EventArgs) Handles BDownExcel.Click
        TUpdate.Enabled = False
        TTimeOut.Enabled = False

        Try
            debugup = ""
            trupdate.Abort()
        Catch ex As Exception
            Try
                trupdate.Abort()
                trupdate = New Threading.Thread(AddressOf CekUpdate)
            Catch ex2 As Exception
                Try
                    trupdate = New Threading.Thread(AddressOf CekUpdate)
                Catch ex3 As Exception
                End Try
            End Try
        End Try

        BacaData()
        Form2.ShowDialog()
    End Sub

    Private Sub BLive_Click(sender As Object, e As EventArgs)
        BTambah.PerformClick()
    End Sub

    Private Sub TBNik1_KeyDown(sender As Object, e As KeyEventArgs) Handles T11.KeyDown
        If e.KeyData = Keys.Enter And TBNik1.TextLength = 0 Then
            TBNama1.Focus()
        End If
    End Sub

    Private Sub TBNik2_KeyDown(sender As Object, e As KeyEventArgs) Handles T21.KeyDown
        If e.KeyData = Keys.Enter And TBNik2.TextLength = 0 Then
            TBNama1.Focus()
        End If
    End Sub

    Private Sub TBNik33KeyDown(sender As Object, e As KeyEventArgs) Handles T31.KeyDown
        If e.KeyData = Keys.Enter And TBNik3.TextLength = 0 Then
            TBNama1.Focus()
        End If
    End Sub

    Private Sub TBNik4_KeyDown(sender As Object, e As KeyEventArgs) Handles T41.KeyDown
        If e.KeyData = Keys.Enter And TBNik4.TextLength = 0 Then
            TBNama1.Focus()
        End If
    End Sub

    Private Sub TBNik5_KeyDown(sender As Object, e As KeyEventArgs) Handles T51.KeyDown
        If e.KeyData = Keys.Enter And TBNik5.TextLength = 0 Then
            TBNama1.Focus()
        End If
    End Sub

    Private Sub TBNik6_KeyDown(sender As Object, e As KeyEventArgs) Handles T61.KeyDown
        If e.KeyData = Keys.Enter And TBNik6.TextLength = 0 Then
            TBNama1.Focus()
        End If
    End Sub

    Private Sub TBNik7_KeyDown(sender As Object, e As KeyEventArgs) Handles T71.KeyDown
        If e.KeyData = Keys.Enter And TBNik7.TextLength = 0 Then
            TBNama1.Focus()
        End If
    End Sub

    Dim tanggalkosong As TextBox

    Private Sub TBNama1_KeyDown(sender As Object, e As KeyEventArgs) Handles TBNama1.KeyDown
        If e.KeyData = Keys.Enter And TBNama1.Text <> "" Then
            PosisikanTanggal()
        End If
        PindahPosisi(e.KeyData, 6)
    End Sub

    Private Sub TBNama2_KeyDown(sender As Object, e As KeyEventArgs) Handles TBNama2.KeyDown
        If e.KeyData = Keys.Enter And TBNama2.Text <> "" Then
            PosisikanTanggal()
        End If
        PindahPosisi(e.KeyData, 5)
    End Sub

    Private Sub TBNama3_KeyDown(sender As Object, e As KeyEventArgs) Handles TBNama3.KeyDown
        If e.KeyData = Keys.Enter And TBNama3.Text <> "" Then
            PosisikanTanggal()
        End If
        PindahPosisi(e.KeyData, 4)
    End Sub

    Private Sub TBNama4_KeyDown(sender As Object, e As KeyEventArgs) Handles TBNama4.KeyDown
        If e.KeyData = Keys.Enter And TBNama4.Text <> "" Then
            PosisikanTanggal()
        End If
        PindahPosisi(e.KeyData, 3)
    End Sub

    Private Sub TBNama5_KeyDown(sender As Object, e As KeyEventArgs) Handles TBNama5.KeyDown
        If e.KeyData = Keys.Enter And TBNama5.Text <> "" Then
            PosisikanTanggal()
        End If
        PindahPosisi(e.KeyData, 2)
    End Sub

    Private Sub TBNama6_KeyDown(sender As Object, e As KeyEventArgs) Handles TBNama6.KeyDown
        If e.KeyData = Keys.Enter And TBNama6.Text <> "" Then
            PosisikanTanggal()
        End If
        PindahPosisi(e.KeyData, 1)
    End Sub

    Private Sub TBNama7_KeyDown(sender As Object, e As KeyEventArgs) Handles TBNama7.KeyDown
        If e.KeyData = Keys.Enter And TBNama7.Text <> "" Then
            PosisikanTanggal()
        End If
        PindahPosisi(e.KeyData, 0)
    End Sub

    Private Sub TBTmpatKb_TextChanged(sender As Object, e As KeyEventArgs) Handles TBTmpatKb.KeyDown
        If e.KeyData = Keys.Enter Then
            TP1.Focus()
        End If
    End Sub

    Dim curr As Integer = 0
    Dim teksinfo As String
    Dim lbug As String

    Dim pengubah As Integer = 0

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If Running.Label4.Visible = False Then
            Running.Label4.Visible = True
        End If
        Running.LCur.Text = teksinfo
        Running.LPosisi.Text = "Ke - " + (a + 1).ToString + ", dari " + REF.Count.ToString
        If a + 1 > REF.Count Then
            Running.LPosisi.Text = "Ke - " + a.ToString + ", dari " + REF.Count.ToString
        End If
        Try
            Running.LKendali.Text = REF(a).ToString
        Catch ex As Exception
        End Try
        Running.LHook.Text = lhook
        Try
            If a = 0 Then
                Running.ProgressBar1.Value = pengubah
            Else
                Running.ProgressBar1.Value = (a * 10) + pengubah
            End If
        Catch ex As Exception
            Running.ProgressBar1.Value = REF.Count * 10
        End Try

        If Running.LCur.Text.Contains("Selesai") Then
            Running.Label4.Visible = False
        End If
    End Sub

    Private Sub FUtama_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If TBKendali.Text = "" Then
            TBKendali.Focus()
        Else
            TBNoRumah.Focus()
        End If
        TUpdate.Enabled = True
    End Sub

    Private Sub BDataBaru_Click(sender As Object, e As EventArgs) Handles BDataBaru.Click
        If BTambah.Text.Contains("Ubah") Then
            KotakPesan.Show(Me, "Jangan, takutnya nanti data yang udah diubah pada masuk ke bundle baru semua.", "Bundle Baru", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Informasi)
        Else
            FNamaBundle.ShowDialog()
            LInfo.Text = "Data terakhir: Kendali=" + "Data Baru" + ", NR=" + "Data Baru" + ", NK=Data Baru, KK=Data Baru"
            ToolStripStatusLabel1.Text = namafile
            baruin = True
            BersihkanControl()
            TBKendali.Focus()
            CBKRef.Items.Clear()
            CBKRef.Items.Add("Tidak Ada")
            CBKRef.SelectedIndex = 0
            KotakPesan.Show(Me, "Bundle baru sudah disiapkan, silakan mulai mengentri data.", "Bundle Baru", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Informasi)
            lPembangunan.Clear()
            lNoKeluarga.Clear()
            Bersihkan()
        End If
    End Sub

    Private Sub ToolStripStatusLabel1_Click(sender As Object, e As EventArgs) Handles ToolStripStatusLabel1.Click
        If BTambah.Text.Contains("Ubah") Then
            KotakPesan.Show(Me, "Jangan, takutnya nanti data yang udah diubah pada masuk ke bundle yang dipilih.", "Pilih Bundle", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Informasi)
        ElseIf FPilihData.CBData.Items.Count = 0 Then
            KotakPesan.Show(Me, "Kamu belum buat bundle apa apa gan.", "Pilih Bundle", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Informasi)
        Else
            FPilihData.ShowDialog(Me)
            Try
                BacaData()
                TBKendali.Text = TentukanKendali(REF(REF.Count - 1))
                LInfo.Text = "Data terakhir: Kendali=" + REF(REF.Count - 1) + ", NR=" + NR(REF.Count - 1) + ", NK=" + NK(REF.Count - 1) + ", KK=" + NAMA1(REF.Count - 1)
                ToolStripStatusLabel1.Text = namafile
            Catch ex As Exception
                LInfo.Text = "Data terakhir: Kendali=" + "Data Baru" + ", NR=" + "Data Baru" + ", NK=Data Baru, KK=Data Baru"
            End Try
        End If
    End Sub

    Private Sub Label26_Click(sender As Object, e As EventArgs) Handles Label26.Click
        System.Diagnostics.Process.Start(CurDir() + "\Tutor")
        System.Diagnostics.Process.Start(CurDir() + "\Tutor\Bantuan.txt")
    End Sub

    Private Sub Label26_MouseEnter(sender As Object, e As EventArgs) Handles Label26.MouseEnter
        Label26.ForeColor = Color.Red
    End Sub

    Private Sub Label26_MouseLeave(sender As Object, e As EventArgs) Handles Label26.MouseLeave
        Label26.ForeColor = Color.Black
    End Sub

    Private Sub Label25_Click(sender As Object, e As EventArgs) Handles Label25.Click
        System.Diagnostics.Process.Start("https://www.facebook.com/dilittlesnoopy")
    End Sub

    Private Sub Label25_MouseEnter(sender As Object, e As EventArgs) Handles Label25.MouseEnter
        Label25.ForeColor = Color.Red
    End Sub

    Private Sub Label25_MouseLeave(sender As Object, e As EventArgs) Handles Label25.MouseLeave
        Label25.ForeColor = Color.Black
    End Sub

    Public tfind As Threading.Thread

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        tfind = New Threading.Thread(AddressOf findwindow)
        trmainkanmusikeror = New Threading.Thread(AddressOf MainkanMusikEror)
        tfind.Priority = Threading.ThreadPriority.Lowest
        tfind.Start()
        Timer2.Enabled = False
    End Sub

    Private Sub TBNama1_TextChanged(sender As Object, e As EventArgs) Handles TBNama1.TextChanged
        If TBTanggal1.Text.Length = 0 And onbersihkan = False And IsNothing(tanggalkosong) Then
            tanggalkosong = TBTanggal1
        End If
    End Sub

    Private Sub TBNama2_TextChanged(sender As Object, e As EventArgs) Handles TBNama2.TextChanged
        If TBTanggal2.Text.Length = 0 And onbersihkan = False And IsNothing(tanggalkosong) Then
            tanggalkosong = TBTanggal2
        End If
    End Sub

    Private Sub TBNama3_TextChanged(sender As Object, e As EventArgs) Handles TBNama3.TextChanged
        If TBTanggal3.Text.Length = 0 And onbersihkan = False And IsNothing(tanggalkosong) Then
            tanggalkosong = TBTanggal3
        End If
    End Sub

    Private Sub TBNama4_TextChanged(sender As Object, e As EventArgs) Handles TBNama4.TextChanged
        If TBTanggal4.Text.Length = 0 And onbersihkan = False And IsNothing(tanggalkosong) Then
            tanggalkosong = TBTanggal4
        End If
    End Sub

    Private Sub TBNama5_TextChanged(sender As Object, e As EventArgs) Handles TBNama5.TextChanged
        If TBTanggal5.Text.Length = 0 And onbersihkan = False And IsNothing(tanggalkosong) Then
            tanggalkosong = TBTanggal5
        End If
    End Sub

    Private Sub TBNama6_TextChanged(sender As Object, e As EventArgs) Handles TBNama6.TextChanged
        If TBTanggal6.Text.Length = 0 And onbersihkan = False And IsNothing(tanggalkosong) Then
            tanggalkosong = TBTanggal6
        End If
    End Sub

    Private Sub TBNama7_TextChanged(sender As Object, e As EventArgs) Handles TBNama7.TextChanged
        If TBTanggal7.Text.Length = 0 And onbersihkan = False And IsNothing(tanggalkosong) Then
            tanggalkosong = TBTanggal7
        End If
    End Sub

    Private Sub BFix_Click(sender As Object, e As EventArgs) Handles BFix.Click
        Dim hasilt, hasilb As Integer
        TBLamaTKb.Text = "00"
        hasilt = Integer.Parse(TBLamaBKb.Text) / 12
        hasilb = Integer.Parse(TBLamaBKb.Text) Mod 12
        Dim nilait As String
        Dim nilaib As String

        If hasilt < 10 Then
            Try
                nilait = "0" + hasilt.ToString.Remove(hasilt.ToString.LastIndexOf(","))
            Catch ex As Exception
                nilait = "0" + hasilt.ToString
            End Try
        Else
            Try
                nilait = hasilt.ToString.Remove(hasilt.ToString.LastIndexOf(","))
            Catch ex As Exception
                nilait = hasilt.ToString
            End Try
        End If

        If hasilb < 10 Then
            Try
                nilaib = "0" + hasilb.ToString.Remove(hasilb.ToString.LastIndexOf(","))
            Catch ex As Exception
                nilaib = "0" + hasilb.ToString
            End Try
        Else
            Try
                nilaib = hasilb.ToString.Remove(hasilb.ToString.LastIndexOf(","))
            Catch ex As Exception
                nilaib = hasilb.ToString
            End Try
        End If

        TBLamaTKb.Text = nilait
        TBLamaBKb.Text = nilaib
        BFix.Visible = False
    End Sub

    Dim adakoneksi As Boolean

    Public Function CekKoneksi2() As Boolean
        Try
            debugup = "Checking Update."
            adakoneksi = False
            Try
                Using client = New WebClient()
                    Using stream = client.OpenRead("https://m.facebook.com/")
                        adakoneksi = True
                        If Not debugup.Contains("Time") Then
                            debugup = "Checking Update.."
                        End If
                        Return True
                    End Using
                End Using
            Catch EX As Exception
                adakoneksi = False
                Return False
            End Try
            adakoneksi = False
            Return False
        Catch ex As Exception
        End Try
    End Function

    Public Client As New Net.WebClient
    Dim semver_a, versek, verbaru As Integer
    Dim sem_upd_a As New List(Of String)
    Dim linkup As String

    Public Sub CekUpdate()
        Try
            If CekKoneksi2() Then
                debugup = "Checking Update..."
                Try
                    Try
                        IO.File.Delete(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\vbkkbn.txt")
                    Catch ex As Exception
                    End Try

                    Client.DownloadFile("https://sites.google.com/site/izarcsoftware/vbkkbn.txt", My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\vbkkbn.txt")
                Catch ex As Exception
                End Try

                debugup = "Checking Update...."
                Dim objReader As New System.IO.StreamReader(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\vbkkbn.txt")
                semver_a = My.Application.Info.Version.ToString
                versek = semver_a.ToString.Replace(".", "")
                sem_upd_a.Clear()
                Do While Not objReader.EndOfStream
                    sem_upd_a.Add(objReader.ReadLine())
                Loop
                objReader.Close()
                verbaru = sem_upd_a.Item(1).ToString.Replace(".", "")
                linkup = sem_upd_a.Item(4).ToString

                If verbaru > versek Then
                    adabaru = True
                    debugup = "Checking Update....."
                ElseIf verbaru = versek Then
                    adabaru = False
                    debugup = "Checking Update....."
                End If
                debugup = ""
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public trupdate As System.Threading.Thread

    Private Sub TUpdate_Tick(sender As Object, e As EventArgs) Handles TUpdate.Tick
        Try
            If adabaru = False Then
                trupdate = New Threading.Thread(AddressOf CekUpdate)
                trupdate.IsBackground = True
                trupdate.Start()
                TTimeOut.Enabled = True
                TUpdate.Enabled = False
            End If
        Catch ex As Exception
            KotakPesan.Show(Me, ex.ToString)
        End Try
    End Sub

    Private Sub TTimeOut_Tick(sender As Object, e As EventArgs) Handles TTimeOut.Tick
        Try
            If adakoneksi = False Then
                Try
                    debugup = "Time Out"
                    trupdate.Abort()
                Catch ex As Exception
                    Try
                        trupdate.Abort()
                        trupdate = New Threading.Thread(AddressOf CekUpdate)
                    Catch ex2 As Exception
                        Try
                            trupdate = New Threading.Thread(AddressOf CekUpdate)
                        Catch ex3 As Exception

                        End Try
                    End Try
                End Try
            End If
            TTimeOut.Enabled = False
            debugup = ""
        Catch ex As Exception
            KotakPesan.Show(Me, ex.ToString)
        End Try
    End Sub


    Private Sub TRefresh_Tick(sender As Object, e As EventArgs) Handles TRefresh.Tick
        Try
            If adabaru Then
                Label27.Text = "Klik, ada update baru"
                TRefresh.Enabled = False
            Else
                Label27.Text = debugup
            End If
        Catch ex As Exception
            KotakPesan.Show(Me, ex.ToString)
        End Try
    End Sub

    Dim adabaru As Boolean = False

    Private Sub Label27_Click(sender As Object, e As EventArgs) Handles Label27.Click
        If linkup <> "" Then
            System.Diagnostics.Process.Start(linkup)
        End If
    End Sub

    Dim indeksdiubah As Integer

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBKendali.SelectedIndexChanged
        indeksdiubah = CBKendali.SelectedIndex
        AturIsi(indeksdiubah)
    End Sub

    Dim LagiAturIsi As Boolean

    Private Sub AturIsi(indeks As Integer)
        T11.Clear()
        T12.Clear()
        T13.Clear()
        T14.Clear()
        T15.Clear()

        T21.Clear()
        T22.Clear()
        T23.Clear()
        T24.Clear()
        T25.Clear()

        T31.Clear()
        T32.Clear()
        T33.Clear()
        T34.Clear()
        T35.Clear()

        T41.Clear()
        T42.Clear()
        T43.Clear()
        T44.Clear()
        T45.Clear()

        T51.Clear()
        T52.Clear()
        T53.Clear()
        T54.Clear()
        T55.Clear()

        T61.Clear()
        T62.Clear()
        T63.Clear()
        T64.Clear()
        T65.Clear()

        T71.Clear()
        T72.Clear()
        T73.Clear()
        T74.Clear()
        T75.Clear()

        TP1.Clear()
        TP2.Clear()

        LagiAturIsi = True
        TBKendali.Text = REF(indeks)
        CBKRef.Text = KREF(indeks)
        TBNoRumah.Text = NR(indeks)
        TBNoKeluarga.Text = NK(indeks)

        TBNik1.Text = NIK1(indeks)
        TBNama1.Text = NAMA1(indeks)
        TBTanggal1.Text = TL1(indeks)
        TBKode1.Text = KD1(indeks)

        TBNik2.Text = NIK2(indeks)
        TBNama2.Text = NAMA2(indeks)
        TBTanggal2.Text = TL2(indeks)
        TBKode2.Text = KD2(indeks)

        TBNik3.Text = NIK3(indeks)
        TBNama3.Text = NAMA3(indeks)
        TBTanggal3.Text = TL3(indeks)
        TBKode3.Text = KD3(indeks)

        TBNik4.Text = NIK4(indeks)
        TBNama4.Text = NAMA4(indeks)
        TBTanggal4.Text = TL4(indeks)
        TBKode4.Text = KD4(indeks)

        TBNik5.Text = NIK5(indeks)
        TBNama5.Text = NAMA5(indeks)
        TBTanggal5.Text = TL5(indeks)
        TBKode5.Text = KD5(indeks)

        TBNik6.Text = NIK6(indeks)
        TBNama6.Text = NAMA6(indeks)
        TBTanggal6.Text = TL6(indeks)
        TBKode6.Text = KD6(indeks)

        TBNik7.Text = NIK7(indeks)
        TBNama7.Text = NAMA7(indeks)
        TBTanggal7.Text = TL7(indeks)
        TBKode7.Text = KD7(indeks)

        TBKawinSu.Text = UsiaKawinSuami(indeks)
        TBKawinIs.Text = UsiaKawinIstri(indeks)
        TBJumlahAnak.Text = JumlahAnak(indeks)
        TBKb.Text = BerKB(indeks)
        TBKontra.Text = MetodKontrasepsi(indeks)
        TBLamaBKb.Text = LamaBKB(indeks)
        TBLamaTKb.Text = LamaTKB(indeks)
        TBPunyaAnak.Text = PnyaAnakLagi(indeks)
        TBAlasanTidakKb.Text = AlasanTidakKB(indeks)
        TBTmpatKb.Text = TmpatLayananKB(indeks)
        TBPembangunan.Text = KodePembangunan(indeks)
        TBLuasRumah.Text = Luas(indeks)
        TBOrang.Text = Orang(indeks)
        LagiAturIsi = False
        CBKendali.Focus()
        MainkanMusik()
    End Sub

    Dim refsebelumnya As String

    Private Sub AturTampilan(ByVal edit As Boolean)
        BacaData()
        If edit Then
            refsebelumnya = TBKendali.Text
            MuatKendali()
            CBKendali.Visible = True
            Button1.Text = "Mode Tambah"
            BTambah.Text = "Ubah"
        Else
            TBKendali.Text = refsebelumnya
            CBKRef.SelectedIndex = 0
            TBNoRumah.Clear()
            TBNoKeluarga.Clear()

            TBNik1.Clear()
            TBNama1.Clear()
            TBTanggal1.Clear()
            TBKode1.Clear()

            TBNik2.Clear()
            TBNama2.Clear()
            TBTanggal2.Clear()
            TBKode2.Clear()

            TBNik3.Clear()
            TBNama3.Clear()
            TBTanggal3.Clear()
            TBKode3.Clear()

            TBNik4.Clear()
            TBNama4.Clear()
            TBTanggal4.Clear()
            TBKode4.Clear()

            TBNik5.Clear()
            TBNama5.Clear()
            TBTanggal5.Clear()
            TBKode5.Clear()

            TBNik6.Clear()
            TBNama6.Clear()
            TBTanggal6.Clear()
            TBKode6.Clear()

            TBNik7.Clear()
            TBNama7.Clear()
            TBTanggal7.Clear()
            TBKode7.Clear()

            TBKawinSu.Clear()
            TBKawinIs.Clear()
            TBJumlahAnak.Clear()
            TBKb.Clear()
            TBKontra.Clear()
            TBLamaTKb.Clear()
            TBLamaBKb.Clear()
            TBPunyaAnak.Clear()
            TBAlasanTidakKb.Clear()
            TBTmpatKb.Clear()
            TBPembangunan.Clear()
            TBLuasRumah.Clear()
            TBOrang.Clear()

            T11.Clear()
            T12.Clear()
            T13.Clear()
            T14.Clear()
            T15.Clear()

            T21.Clear()
            T22.Clear()
            T23.Clear()
            T24.Clear()
            T25.Clear()

            T31.Clear()
            T32.Clear()
            T33.Clear()
            T34.Clear()
            T35.Clear()

            T41.Clear()
            T42.Clear()
            T43.Clear()
            T44.Clear()
            T45.Clear()

            T51.Clear()
            T52.Clear()
            T53.Clear()
            T54.Clear()
            T55.Clear()

            T61.Clear()
            T62.Clear()
            T63.Clear()
            T64.Clear()
            T65.Clear()

            T71.Clear()
            T72.Clear()
            T73.Clear()
            T74.Clear()
            T75.Clear()

            TP1.Clear()
            TP2.Clear()

            BFix.Visible = False
            onbersihkan = False

            CBKendali.Visible = False
            Button1.Text = "Mode Edit"
            BTambah.Text = "Tambah"
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Button1.Text = "Mode Edit" Then
            AturTampilan(True)
        Else
            AturTampilan(False)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles T11.TextChanged
        If T11.Text.Length = 6 Then
            T12.Focus()
        End If
    End Sub

    Private Sub T21_TextChanged(sender As Object, e As EventArgs) Handles T21.TextChanged
        If T21.Text.Length = 6 Then
            T22.Focus()
        End If
    End Sub

    Private Sub T31_TextChanged(sender As Object, e As EventArgs) Handles T31.TextChanged
        If T31.Text.Length = 6 Then
            T32.Focus()
        End If
    End Sub

    Private Sub T41_TextChanged(sender As Object, e As EventArgs) Handles T41.TextChanged
        If T41.Text.Length = 6 Then
            T42.Focus()
        End If
    End Sub

    Private Sub T51_TextChanged(sender As Object, e As EventArgs) Handles T51.TextChanged
        If T51.Text.Length = 6 Then
            T52.Focus()
        End If
    End Sub

    Private Sub T61_TextChanged(sender As Object, e As EventArgs) Handles T61.TextChanged
        If T61.Text.Length = 6 Then
            T62.Focus()
        End If
    End Sub

    Private Sub T71_TextChanged(sender As Object, e As EventArgs) Handles T71.TextChanged
        If T71.Text.Length = 6 Then
            T72.Focus()
        End If
    End Sub

    Private Sub T12_TextChanged(sender As Object, e As EventArgs) Handles T12.TextChanged
        If T12.Text.Length = 2 Then
            T13.Focus()
        End If
    End Sub

    Private Sub T22_TextChanged(sender As Object, e As EventArgs) Handles T22.TextChanged
        If T22.Text.Length = 2 Then
            T23.Focus()
        End If
    End Sub

    Private Sub T32_TextChanged(sender As Object, e As EventArgs) Handles T32.TextChanged
        If T32.Text.Length = 2 Then
            T33.Focus()
        End If
    End Sub

    Private Sub T42_TextChanged(sender As Object, e As EventArgs) Handles T42.TextChanged
        If T42.Text.Length = 2 Then
            T43.Focus()
        End If
    End Sub

    Private Sub T52_TextChanged(sender As Object, e As EventArgs) Handles T52.TextChanged
        If T52.Text.Length = 2 Then
            T53.Focus()
        End If
    End Sub

    Private Sub T62_TextChanged(sender As Object, e As EventArgs) Handles T62.TextChanged
        If T62.Text.Length = 2 Then
            T63.Focus()
        End If
    End Sub

    Private Sub T72_TextChanged(sender As Object, e As EventArgs) Handles T72.TextChanged
        If T72.Text.Length = 2 Then
            T73.Focus()
        End If
    End Sub

    Private Sub T13_TextChanged(sender As Object, e As EventArgs) Handles T13.TextChanged
        If T13.Text.Length = 2 Then
            T14.Focus()
        End If
    End Sub

    Private Sub T23_TextChanged(sender As Object, e As EventArgs) Handles T23.TextChanged
        If T23.Text.Length = 2 Then
            T24.Focus()
        End If
    End Sub

    Private Sub T33_TextChanged(sender As Object, e As EventArgs) Handles T33.TextChanged
        If T33.Text.Length = 2 Then
            T34.Focus()
        End If
    End Sub

    Private Sub T43_TextChanged(sender As Object, e As EventArgs) Handles T43.TextChanged
        If T43.Text.Length = 2 Then
            T44.Focus()
        End If
    End Sub

    Private Sub T53_TextChanged(sender As Object, e As EventArgs) Handles T53.TextChanged
        If T53.Text.Length = 2 Then
            T54.Focus()
        End If
    End Sub

    Private Sub T63_TextChanged(sender As Object, e As EventArgs) Handles T63.TextChanged
        If T63.Text.Length = 2 Then
            T64.Focus()
        End If
    End Sub

    Private Sub T73_TextChanged(sender As Object, e As EventArgs) Handles T73.TextChanged
        If T73.Text.Length = 2 Then
            T74.Focus()
        End If
    End Sub

    Private Sub T14_TextChanged(sender As Object, e As EventArgs) Handles T14.TextChanged
        If T14.Text.Length = 2 Then
            T15.Focus()
        End If
    End Sub

    Private Sub T24_TextChanged(sender As Object, e As EventArgs) Handles T24.TextChanged
        If T24.Text.Length = 2 Then
            T25.Focus()
        End If
    End Sub

    Private Sub T34_TextChanged(sender As Object, e As EventArgs) Handles T34.TextChanged
        If T34.Text.Length = 2 Then
            T35.Focus()
        End If
    End Sub

    Private Sub T44_TextChanged(sender As Object, e As EventArgs) Handles T44.TextChanged
        If T44.Text.Length = 2 Then
            T45.Focus()
        End If
    End Sub

    Private Sub T54_TextChanged(sender As Object, e As EventArgs) Handles T54.TextChanged
        If T54.Text.Length = 2 Then
            T55.Focus()
        End If
    End Sub

    Private Sub T64_TextChanged(sender As Object, e As EventArgs) Handles T64.TextChanged
        If T64.Text.Length = 2 Then
            T65.Focus()
        End If
    End Sub

    Private Sub T74_TextChanged(sender As Object, e As EventArgs) Handles T74.TextChanged
        If T74.Text.Length = 2 Then
            T75.Focus()
        End If
    End Sub

    Private Sub T15_TextChanged(sender As Object, e As EventArgs) Handles T15.TextChanged
        If T15.Text.Length = 4 Then
            T21.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub T25_TextChanged(sender As Object, e As EventArgs) Handles T25.TextChanged
        If T25.Text.Length = 4 Then
            T31.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub T35_TextChanged(sender As Object, e As EventArgs) Handles T35.TextChanged
        If T35.Text.Length = 4 Then
            T41.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub T45_TextChanged(sender As Object, e As EventArgs) Handles T45.TextChanged
        If T45.Text.Length = 4 Then
            T51.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub T55_TextChanged(sender As Object, e As EventArgs) Handles T55.TextChanged
        If T55.Text.Length = 4 Then
            T61.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub T65_TextChanged(sender As Object, e As EventArgs) Handles T65.TextChanged
        If T65.Text.Length = 4 Then
            T71.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub T75_TextChanged(sender As Object, e As EventArgs) Handles T75.TextChanged
        If T75.Text.Length = 4 Then
            TBNama1.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub Nik1_Changed(sender As Object, e As EventArgs) Handles T11.TextChanged, T12.TextChanged, T13.TextChanged, T14.TextChanged, T15.TextChanged
        If Not LagiAturIsi Then
            TBNik1.Text = T11.Text + T12.Text + T13.Text + T14.Text + T15.Text
        End If
    End Sub

    Private Sub Nik2_Changed(sender As Object, e As EventArgs) Handles T21.TextChanged, T22.TextChanged, T23.TextChanged, T24.TextChanged, T25.TextChanged
        If Not LagiAturIsi Then
            TBNik2.Text = T21.Text + T22.Text + T23.Text + T24.Text + T25.Text
        End If
    End Sub

    Private Sub Nik3_Changed(sender As Object, e As EventArgs) Handles T31.TextChanged, T32.TextChanged, T33.TextChanged, T34.TextChanged, T35.TextChanged
        If Not LagiAturIsi Then
            TBNik3.Text = T31.Text + T32.Text + T33.Text + T34.Text + T35.Text
        End If
    End Sub

    Private Sub Nik4_Changed(sender As Object, e As EventArgs) Handles T41.TextChanged, T42.TextChanged, T43.TextChanged, T44.TextChanged, T45.TextChanged
        If Not LagiAturIsi Then
            TBNik4.Text = T41.Text + T42.Text + T43.Text + T44.Text + T45.Text
        End If
    End Sub

    Private Sub Nik5_Changed(sender As Object, e As EventArgs) Handles T51.TextChanged, T52.TextChanged, T53.TextChanged, T54.TextChanged, T55.TextChanged
        If Not LagiAturIsi Then
            TBNik5.Text = T51.Text + T52.Text + T53.Text + T54.Text + T55.Text
        End If
    End Sub

    Private Sub Nik6_Changed(sender As Object, e As EventArgs) Handles T61.TextChanged, T62.TextChanged, T63.TextChanged, T64.TextChanged, T65.TextChanged
        If Not LagiAturIsi Then
            TBNik6.Text = T61.Text + T62.Text + T63.Text + T64.Text + T65.Text
        End If
    End Sub

    Private Sub Nik7_Changed(sender As Object, e As EventArgs) Handles T71.TextChanged, T72.TextChanged, T73.TextChanged, T74.TextChanged, T75.TextChanged
        If Not LagiAturIsi Then
            TBNik7.Text = T71.Text + T72.Text + T73.Text + T74.Text + T75.Text
        End If
    End Sub

    Private Sub HapusHuruf(teksboks As TextBox)
        If teksboks.TextLength <> 0 Then
            teksboks.Text = teksboks.Text.Remove(teksboks.Text.Length - 1)
            teksboks.SelectionStart = teksboks.Text.Length
            teksboks.SelectionLength = 0
        End If
    End Sub

    Private Sub T15_KeyDown(sender As Object, e As KeyEventArgs) Handles T15.KeyDown
        If e.KeyData = Keys.Back And T15.TextLength = 0 Then
            T14.Focus()
            HapusHuruf(T14)
        End If
    End Sub

    Private Sub T25_KeyDown(sender As Object, e As KeyEventArgs) Handles T25.KeyDown
        If e.KeyData = Keys.Back And T25.TextLength = 0 Then
            T24.Focus()
            HapusHuruf(T24)
        End If
    End Sub

    Private Sub T35_KeyDown(sender As Object, e As KeyEventArgs) Handles T35.KeyDown
        If e.KeyData = Keys.Back And T35.TextLength = 0 Then
            T34.Focus()
            HapusHuruf(T34)
        End If
    End Sub

    Private Sub T45_KeyDown(sender As Object, e As KeyEventArgs) Handles T45.KeyDown
        If e.KeyData = Keys.Back And T45.TextLength = 0 Then
            T44.Focus()
            HapusHuruf(T44)
        End If
    End Sub

    Private Sub T55_KeyDown(sender As Object, e As KeyEventArgs) Handles T55.KeyDown
        If e.KeyData = Keys.Back And T55.TextLength = 0 Then
            T54.Focus()
            HapusHuruf(T54)
        End If
    End Sub

    Private Sub T65_KeyDown(sender As Object, e As KeyEventArgs) Handles T65.KeyDown
        If e.KeyData = Keys.Back And T65.TextLength = 0 Then
            T64.Focus()
            HapusHuruf(T64)
        End If
    End Sub

    Private Sub T75_KeyDown(sender As Object, e As KeyEventArgs) Handles T75.KeyDown
        If e.KeyData = Keys.Back And T75.TextLength = 0 Then
            T74.Focus()
            HapusHuruf(T74)
        End If
    End Sub


    Private Sub T14_KeyDown(sender As Object, e As KeyEventArgs) Handles T14.KeyDown
        If e.KeyData = Keys.Back And T14.TextLength = 0 Then
            T13.Focus()
            HapusHuruf(T13)
        End If
    End Sub

    Private Sub T24_KeyDown(sender As Object, e As KeyEventArgs) Handles T24.KeyDown
        If e.KeyData = Keys.Back And T24.TextLength = 0 Then
            T23.Focus()
            HapusHuruf(T23)
        End If
    End Sub

    Private Sub T34_KeyDown(sender As Object, e As KeyEventArgs) Handles T34.KeyDown
        If e.KeyData = Keys.Back And T34.TextLength = 0 Then
            T33.Focus()
            HapusHuruf(T33)
        End If
    End Sub

    Private Sub T44_KeyDown(sender As Object, e As KeyEventArgs) Handles T44.KeyDown
        If e.KeyData = Keys.Back And T44.TextLength = 0 Then
            T43.Focus()
            HapusHuruf(T43)
        End If
    End Sub

    Private Sub T54_KeyDown(sender As Object, e As KeyEventArgs) Handles T54.KeyDown
        If e.KeyData = Keys.Back And T54.TextLength = 0 Then
            T53.Focus()
            HapusHuruf(T53)
        End If
    End Sub

    Private Sub T64_KeyDown(sender As Object, e As KeyEventArgs) Handles T64.KeyDown
        If e.KeyData = Keys.Back And T64.TextLength = 0 Then
            T63.Focus()
            HapusHuruf(T63)
        End If
    End Sub

    Private Sub T74_KeyDown(sender As Object, e As KeyEventArgs) Handles T74.KeyDown
        If e.KeyData = Keys.Back And T74.TextLength = 0 Then
            T73.Focus()
            HapusHuruf(T73)
        End If
    End Sub

    Private Sub T13_KeyDown(sender As Object, e As KeyEventArgs) Handles T13.KeyDown
        If e.KeyData = Keys.Back And T13.TextLength = 0 Then
            T12.Focus()
            HapusHuruf(T12)
        End If
    End Sub

    Private Sub T23_KeyDown(sender As Object, e As KeyEventArgs) Handles T23.KeyDown
        If e.KeyData = Keys.Back And T23.TextLength = 0 Then
            T22.Focus()
            HapusHuruf(T22)
        End If
    End Sub

    Private Sub T33_KeyDown(sender As Object, e As KeyEventArgs) Handles T33.KeyDown
        If e.KeyData = Keys.Back And T33.TextLength = 0 Then
            T32.Focus()
            HapusHuruf(T32)
        End If
    End Sub

    Private Sub T43_KeyDown(sender As Object, e As KeyEventArgs) Handles T43.KeyDown
        If e.KeyData = Keys.Back And T43.TextLength = 0 Then
            T42.Focus()
            HapusHuruf(T42)
        End If
    End Sub

    Private Sub T53_KeyDown(sender As Object, e As KeyEventArgs) Handles T53.KeyDown
        If e.KeyData = Keys.Back And T53.TextLength = 0 Then
            T52.Focus()
            HapusHuruf(T52)
        End If
    End Sub

    Private Sub T63_KeyDown(sender As Object, e As KeyEventArgs) Handles T63.KeyDown
        If e.KeyData = Keys.Back And T63.TextLength = 0 Then
            T62.Focus()
            HapusHuruf(T62)
        End If
    End Sub

    Private Sub T73_KeyDown(sender As Object, e As KeyEventArgs) Handles T73.KeyDown
        If e.KeyData = Keys.Back And T73.TextLength = 0 Then
            T72.Focus()
            HapusHuruf(T72)
        End If
    End Sub

    Private Sub T12_KeyDown(sender As Object, e As KeyEventArgs) Handles T12.KeyDown
        If e.KeyData = Keys.Back And T12.TextLength = 0 Then
            T11.Focus()
            HapusHuruf(T11)
        End If
    End Sub

    Private Sub T22_KeyDown(sender As Object, e As KeyEventArgs) Handles T22.KeyDown
        If e.KeyData = Keys.Back And T22.TextLength = 0 Then
            T21.Focus()
            HapusHuruf(T21)
        End If
    End Sub

    Private Sub T32_KeyDown(sender As Object, e As KeyEventArgs) Handles T32.KeyDown
        If e.KeyData = Keys.Back And T32.TextLength = 0 Then
            T31.Focus()
            HapusHuruf(T31)
        End If
    End Sub

    Private Sub T42_KeyDown(sender As Object, e As KeyEventArgs) Handles T42.KeyDown
        If e.KeyData = Keys.Back And T42.TextLength = 0 Then
            T41.Focus()
            HapusHuruf(T41)
        End If
    End Sub

    Private Sub T52_KeyDown(sender As Object, e As KeyEventArgs) Handles T52.KeyDown
        If e.KeyData = Keys.Back And T52.TextLength = 0 Then
            T51.Focus()
            HapusHuruf(T51)
        End If
    End Sub

    Private Sub T62_KeyDown(sender As Object, e As KeyEventArgs) Handles T62.KeyDown
        If e.KeyData = Keys.Back And T62.TextLength = 0 Then
            T61.Focus()
            HapusHuruf(T61)
        End If
    End Sub

    Private Sub T72_KeyDown(sender As Object, e As KeyEventArgs) Handles T72.KeyDown
        If e.KeyData = Keys.Back And T72.TextLength = 0 Then
            T71.Focus()
            HapusHuruf(T71)
        End If
    End Sub

    Private Sub Label27_MouseEnter(sender As Object, e As EventArgs) Handles Label27.MouseEnter
        Label27.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Bold)
    End Sub

    Private Sub Label27_MouseLeave(sender As Object, e As EventArgs) Handles Label27.MouseLeave
        Label27.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
    End Sub

    Private Sub FUtama_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Try
            tfind.Abort()
        Catch ex As Exception
            tfind = New Threading.Thread(AddressOf findwindow)
        End Try

        Try
            trmainkanmusikeror.Abort()
        Catch ex As Exception
            trmainkanmusikeror = New Threading.Thread(AddressOf MainkanMusikEror)
        End Try


        Try
            tr.Abort()
        Catch ex As Exception
            tr = New Threading.Thread(AddressOf Proses)
        End Try

        Try
            trupdate.Abort()
        Catch ex As Exception
            trupdate = New Threading.Thread(AddressOf Proses)
        End Try
    End Sub

    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        Dim pin As String = "085726411955"

        If Label25.Text.Contains("Tanyakan") Then
            TransText(Label25, "By. Aziz Nur Ariffianto", 800)
        ElseIf Label25.Text.Contains("Aziz") Then
            TransText(Label25, "Tanyakan sesuatu ?, " + pin, 800)
        End If
    End Sub

    Private Sub TP1_TextChanged(sender As Object, e As EventArgs) Handles TP1.TextChanged, TP2.TextChanged
        If Not LagiAturIsi Then
            TBPembangunan.Text = TP1.Text + TP2.Text
        End If
    End Sub

    Dim sudahbunyi As Boolean

    Private Sub TP1_TextChanged2(sender As Object, e As EventArgs) Handles TP1.TextChanged
        If TP1.TextLength = 8 And TP2.Text <> "" Then
            If Not sudahbunyi Then
                MainkanMusikExtra()
                sudahbunyi = True
            End If
        End If

        If TP1.TextLength = 18 Then
            If TP2.Text = "" Then
                TP2.Focus()
            Else
                TBOrang.Focus()
            End If
            MainkanMusik()
            sudahbunyi = False
        End If
    End Sub

    Private Sub TP2_TextChanged2(sender As Object, e As EventArgs) Handles TP2.TextChanged
        If TP2.TextLength = 8 Then
            TBLuasRumah.Focus()
            MainkanMusik()
        End If
    End Sub

    Private Sub BX1_Click(sender As Object, e As EventArgs) Handles BX1.Click
        T11.Clear()
        T12.Clear()
        T13.Clear()
        T14.Clear()
        T15.Clear()

        TBNik1.Clear()
        TBNama1.Clear()
        TBTanggal1.Clear()
        TBKode1.Clear()
    End Sub

    Private Sub BX2_Click(sender As Object, e As EventArgs) Handles BX2.Click
        T21.Clear()
        T22.Clear()
        T23.Clear()
        T24.Clear()
        T25.Clear()

        TBNik2.Clear()
        TBNama2.Clear()
        TBTanggal2.Clear()
        TBKode2.Clear()
    End Sub

    Private Sub BX3_Click(sender As Object, e As EventArgs) Handles BX3.Click
        T31.Clear()
        T32.Clear()
        T33.Clear()
        T34.Clear()
        T35.Clear()

        TBNik3.Clear()
        TBNama3.Clear()
        TBTanggal3.Clear()
        TBKode3.Clear()
    End Sub

    Private Sub BX4_Click(sender As Object, e As EventArgs) Handles BX4.Click
        T41.Clear()
        T42.Clear()
        T43.Clear()
        T44.Clear()
        T45.Clear()

        TBNik4.Clear()
        TBNama4.Clear()
        TBTanggal4.Clear()
        TBKode4.Clear()
    End Sub

    Private Sub BX5_Click(sender As Object, e As EventArgs) Handles BX5.Click
        T51.Clear()
        T52.Clear()
        T53.Clear()
        T54.Clear()
        T55.Clear()

        TBNik5.Clear()
        TBNama5.Clear()
        TBTanggal5.Clear()
        TBKode5.Clear()
    End Sub

    Private Sub BX6_Click(sender As Object, e As EventArgs) Handles BX6.Click
        T61.Clear()
        T62.Clear()
        T63.Clear()
        T64.Clear()
        T65.Clear()

        TBNik6.Clear()
        TBNama6.Clear()
        TBTanggal6.Clear()
        TBKode6.Clear()
    End Sub

    Private Sub BX7_Click(sender As Object, e As EventArgs) Handles BX7.Click
        T71.Clear()
        T72.Clear()
        T73.Clear()
        T74.Clear()
        T75.Clear()

        TBNik7.Clear()
        TBNama7.Clear()
        TBTanggal7.Clear()
        TBKode7.Clear()
    End Sub

    Private Sub Label23_Click(sender As Object, e As EventArgs) Handles Label23.Click
        KotakPesan.Show(Me, "Entri Data BKKBN Versi " + Application.ProductVersion + Environment.NewLine + Environment.NewLine + "Dibuat Oleh: Aziz Nur Ariffianto" + Environment.NewLine + Environment.NewLine + "Jangan lupa saat saat ngentri data manual di excel yah... >.<", "Info Aplikasi", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Informasi)
    End Sub

    Private Sub LInfo_Click(sender As Object, e As EventArgs) Handles LInfo.Click
        System.Diagnostics.Process.Start(AlamatAppData)
    End Sub

    Private Sub keyboardHook_KeyUp(sender As Object, e As KeyEventArgs) Handles keyboardHook.KeyDown
        If e.KeyCode.ToString = "Add" And Me.ContainsFocus Then
            KeyboardSimulator.KeyDown(Keys.Back)
            KeyboardSimulator.KeyUp(Keys.Back)
            KeyboardSimulator.KeyDown(Keys.LShiftKey)
            KeyboardSimulator.KeyDown(Keys.Tab)
            KeyboardSimulator.KeyUp(Keys.LShiftKey)
            KeyboardSimulator.KeyUp(Keys.Tab)
        ElseIf e.KeyCode.ToString = "Subtract" And Me.ContainsFocus Then
            KeyboardSimulator.KeyDown(Keys.Back)
            KeyboardSimulator.KeyUp(Keys.Back)
        ElseIf (e.Modifiers = Keys.Control Or e.Modifiers = Keys.LControlKey Or e.Modifiers = Keys.RControlKey Or e.Modifiers = Keys.ControlKey) And e.KeyCode = Keys.Enter Then
            BTambah.PerformClick()
        End If
    End Sub

    Private Sub keyboardHook_KeyUp2(sender As Object, e As KeyEventArgs) Handles keyboardHook.KeyUp
        If e.KeyCode.ToString = "Add" And Me.ContainsFocus Then
            KeyboardSimulator.KeyDown(Keys.Back)
            KeyboardSimulator.KeyUp(Keys.Back)
        ElseIf e.KeyCode.ToString = "Subtract" And Me.ContainsFocus Then
            KeyboardSimulator.KeyDown(Keys.Back)
            KeyboardSimulator.KeyUp(Keys.Back)
        End If
    End Sub

    Private Sub TBNoKeluarga_DoubleClick(sender As Object, e As EventArgs) Handles TBNoKeluarga.DoubleClick
        Informasi.ListBox1.Items.Clear()
        For Each item As String In lNoKeluarga
            Informasi.ListBox1.Items.Add(item)
        Next
        Informasi.ShowDialog()
    End Sub

    Private Sub TBNoRumah_DoubleClick(sender As Object, e As EventArgs) Handles TBNoRumah.DoubleClick
        Dim temp As String
        Informasi.ListBox1.Items.Clear()
        For Each item As String() In lPembangunan
            temp = item(0) + ", " + item(1) + ", " + item(2)
            Informasi.ListBox1.Items.Add(temp)
        Next
        Informasi.ShowDialog()
    End Sub

    Dim TahunPendataan As String

    Private Sub CBTahunPendataan_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBTahunPendataan.SelectedIndexChanged
        TahunPendataan = CBTahunPendataan.Text
    End Sub

    Private Sub TBNama1_DoubleClick(sender As Object, e As EventArgs) Handles TBNama1.DoubleClick
        Informasi.ListBox1.Items.Clear()
        For Each item As TextBox In lsn
            Informasi.ListBox1.Items.Add(item.Text)
        Next
        Informasi.ShowDialog()
    End Sub

    Private Sub TBKendali_DoubleClick(sender As Object, e As EventArgs) Handles TBKendali.DoubleClick
        FEditKendali.ShowDialog()
    End Sub
End Class

Public Class clsCompareFileInfo
    Implements IComparer

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements IComparer.Compare
        Dim File1 As FileInfo
        Dim File2 As FileInfo

        File1 = DirectCast(y, FileInfo)
        File2 = DirectCast(x, FileInfo)

        Compare = DateTime.Compare(File1.CreationTime, File2.CreationTime)
    End Function
End Class
