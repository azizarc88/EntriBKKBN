﻿Imports MessageHandle

Public Class FEditKendali
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BUbah.Click
        If TBUbahKendali.TextLength <> 8 Then
            KotakPesan.Show(Me, "No Kendali harus berdigit 8 !", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            TBUbahKendali.Focus()
            Exit Sub
        End If

        LBHasil.Items.Clear()
        Dim sudahlewat As Boolean = False
        Dim berubahdariindeks As Integer = LBKendali.SelectedIndex
        Dim nawal As Integer = Integer.Parse(TBUbahKendali.Text)
        Dim ii As Integer = 0

        For Each item As String In LBKendali.Items
            If ii = berubahdariindeks Then
                sudahlewat = True
            End If

            If sudahlewat Then
                Dim temp As Integer = Integer.Parse(nawal)
                Dim str As String = temp
                Do Until str.Length = 8
                    str = "0" + str
                Loop

                LBHasil.Items.Add(str)
                temp = Integer.Parse(nawal) + 1
                nawal = temp
            Else
                LBHasil.Items.Add(item)
            End If
            ii += 1
        Next
        Label2.Text = "Hasil Perubahan, " + LBHasil.Items.Count.ToString
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub FEditKendali_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each item As String In FUtama.REF
            LBKendali.Items.Add(item)
        Next
        Label1.Text = "Data, " + LBKendali.Items.Count.ToString
    End Sub

    Private Sub LBKendali_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBKendali.SelectedIndexChanged
        TBKendali.Text = LBKendali.SelectedItem
    End Sub

    Private Sub BSimpan_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        If LBHasil.Items.Count <> LBKendali.Items.Count Then
            KotakPesan.Show(Me, "Tidak ada hasil yang bisa dianggap perubahan.", "GALAT", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            Exit Sub
        End If

        FUtama.REF.Clear()
        For Each item As String In LBHasil.Items
            FUtama.REF.Add(item)
        Next

        FUtama.UbahSemua()
        FUtama.UpdateKendali(LBHasil.Items(LBHasil.Items.Count - 1))
    End Sub
End Class