﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FPilihData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BLaksanakan = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CBData = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'BLaksanakan
        '
        Me.BLaksanakan.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BLaksanakan.Location = New System.Drawing.Point(248, 42)
        Me.BLaksanakan.Name = "BLaksanakan"
        Me.BLaksanakan.Size = New System.Drawing.Size(75, 23)
        Me.BLaksanakan.TabIndex = 5
        Me.BLaksanakan.Text = "Pilih"
        Me.BLaksanakan.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Pilih berkas data inputan:"
        '
        'CBData
        '
        Me.CBData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CBData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBData.FormattingEnabled = True
        Me.CBData.Location = New System.Drawing.Point(144, 11)
        Me.CBData.Name = "CBData"
        Me.CBData.Size = New System.Drawing.Size(179, 21)
        Me.CBData.TabIndex = 3
        '
        'FPilihData
        '
        Me.AcceptButton = Me.BLaksanakan
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(335, 75)
        Me.ControlBox = False
        Me.Controls.Add(Me.BLaksanakan)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CBData)
        Me.Name = "FPilihData"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pilih Data"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BLaksanakan As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents CBData As ComboBox
End Class
