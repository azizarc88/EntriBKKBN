﻿Public Class Form2
    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CBKendali.Items.Clear()
        For Each item As String In FUtama.REF
            CBKendali.Items.Add(item)
        Next
    End Sub

    Private Sub BLaksanakan_Click(sender As Object, e As EventArgs) Handles BLaksanakan.Click
        Running.LCur.Text = "Memulai..."
        If CBKendali.Text = "" Then
            MessageBox.Show("Pilih no kendali !")
            Exit Sub
        End If
        If Not FUtama.BringWindowToFront() Then
            Exit Sub
        End If
        FUtama.Hide()
        Me.Hide()
        FUtama.Timer1.Enabled = True
        Running.Show()
        FUtama.a = CBKendali.SelectedIndex
        FUtama.tr = Nothing
        FUtama.tr = New Threading.Thread(AddressOf FUtama.Proses)
        FUtama.tr.Priority = Threading.ThreadPriority.Normal
        FUtama.tr.IsBackground = True
        FUtama.tr.Start()
        FUtama.Timer2.Enabled = True
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
        FUtama.Show()
    End Sub

    Private Sub Form2_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        CBKendali.Focus()
    End Sub
End Class