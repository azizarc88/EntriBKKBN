﻿Imports MessageHandle

Public Class FNamaBundle
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If IO.File.Exists(FUtama.AlamatAppData + "\" + TextBox1.Text + ".dft") Then
            KotakPesan.Show(Me, "File dengan nama " + TextBox1.Text + ".dft" + ", sudah ada, silakan cari nama lain.", "Nama Sudah Ada", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            Exit Sub
        End If

        If TextBox1.Text = "" Then
            TextBox1.Text = "Data Keluarga - Bundle " + (FPilihData.CBData.Items.Count + 1).ToString
        End If

        FUtama.namafile = TextBox1.Text + ".dft"
        Me.Close()
    End Sub
End Class