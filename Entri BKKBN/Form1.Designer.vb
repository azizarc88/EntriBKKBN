﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FUtama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FUtama))
        Me.BDownExcel = New System.Windows.Forms.Button()
        Me.BKeluar = New System.Windows.Forms.Button()
        Me.BTambah = New System.Windows.Forms.Button()
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.TBKode7 = New System.Windows.Forms.TextBox()
        Me.TBPembangunan = New System.Windows.Forms.TextBox()
        Me.TBTanggal7 = New System.Windows.Forms.TextBox()
        Me.TBKode6 = New System.Windows.Forms.TextBox()
        Me.TBTanggal6 = New System.Windows.Forms.TextBox()
        Me.TBKode3 = New System.Windows.Forms.TextBox()
        Me.TBTanggal3 = New System.Windows.Forms.TextBox()
        Me.TBKode5 = New System.Windows.Forms.TextBox()
        Me.TBTanggal5 = New System.Windows.Forms.TextBox()
        Me.TBKode2 = New System.Windows.Forms.TextBox()
        Me.TBTanggal2 = New System.Windows.Forms.TextBox()
        Me.TBKode4 = New System.Windows.Forms.TextBox()
        Me.TBTanggal4 = New System.Windows.Forms.TextBox()
        Me.TBKode1 = New System.Windows.Forms.TextBox()
        Me.TBTanggal1 = New System.Windows.Forms.TextBox()
        Me.TBNama7 = New System.Windows.Forms.TextBox()
        Me.TBNama6 = New System.Windows.Forms.TextBox()
        Me.TBNama3 = New System.Windows.Forms.TextBox()
        Me.TBNama5 = New System.Windows.Forms.TextBox()
        Me.TBNama2 = New System.Windows.Forms.TextBox()
        Me.TBNama4 = New System.Windows.Forms.TextBox()
        Me.TBNama1 = New System.Windows.Forms.TextBox()
        Me.TBLamaBKb = New System.Windows.Forms.TextBox()
        Me.TBKawinIs = New System.Windows.Forms.TextBox()
        Me.TBTmpatKb = New System.Windows.Forms.TextBox()
        Me.TBKontra = New System.Windows.Forms.TextBox()
        Me.TBAlasanTidakKb = New System.Windows.Forms.TextBox()
        Me.TBKb = New System.Windows.Forms.TextBox()
        Me.TBPunyaAnak = New System.Windows.Forms.TextBox()
        Me.TBJumlahAnak = New System.Windows.Forms.TextBox()
        Me.TBLamaTKb = New System.Windows.Forms.TextBox()
        Me.TBKawinSu = New System.Windows.Forms.TextBox()
        Me.TBNoKeluarga = New System.Windows.Forms.TextBox()
        Me.TBNoRumah = New System.Windows.Forms.TextBox()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label20 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label12 = New System.Windows.Forms.Label()
        Me.label11 = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.label9 = New System.Windows.Forms.Label()
        Me.label8 = New System.Windows.Forms.Label()
        Me.label7 = New System.Windows.Forms.Label()
        Me.label21 = New System.Windows.Forms.Label()
        Me.label16 = New System.Windows.Forms.Label()
        Me.label6 = New System.Windows.Forms.Label()
        Me.label19 = New System.Windows.Forms.Label()
        Me.label15 = New System.Windows.Forms.Label()
        Me.label18 = New System.Windows.Forms.Label()
        Me.label14 = New System.Windows.Forms.Label()
        Me.label22 = New System.Windows.Forms.Label()
        Me.label17 = New System.Windows.Forms.Label()
        Me.label13 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.TBLuasRumah = New System.Windows.Forms.TextBox()
        Me.TBOrang = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TBKendali = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.LInfo = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.BDataBaru = New System.Windows.Forms.Button()
        Me.TBNik1 = New System.Windows.Forms.TextBox()
        Me.TBNik4 = New System.Windows.Forms.TextBox()
        Me.TBNik2 = New System.Windows.Forms.TextBox()
        Me.TBNik5 = New System.Windows.Forms.TextBox()
        Me.TBNik3 = New System.Windows.Forms.TextBox()
        Me.TBNik6 = New System.Windows.Forms.TextBox()
        Me.TBNik7 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.BFix = New System.Windows.Forms.Button()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TUpdate = New System.Windows.Forms.Timer(Me.components)
        Me.TTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.TRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CBKendali = New System.Windows.Forms.ComboBox()
        Me.T11 = New System.Windows.Forms.TextBox()
        Me.T12 = New System.Windows.Forms.TextBox()
        Me.T13 = New System.Windows.Forms.TextBox()
        Me.T14 = New System.Windows.Forms.TextBox()
        Me.T15 = New System.Windows.Forms.TextBox()
        Me.T21 = New System.Windows.Forms.TextBox()
        Me.T22 = New System.Windows.Forms.TextBox()
        Me.T23 = New System.Windows.Forms.TextBox()
        Me.T24 = New System.Windows.Forms.TextBox()
        Me.T25 = New System.Windows.Forms.TextBox()
        Me.T31 = New System.Windows.Forms.TextBox()
        Me.T32 = New System.Windows.Forms.TextBox()
        Me.T33 = New System.Windows.Forms.TextBox()
        Me.T34 = New System.Windows.Forms.TextBox()
        Me.T35 = New System.Windows.Forms.TextBox()
        Me.T41 = New System.Windows.Forms.TextBox()
        Me.T42 = New System.Windows.Forms.TextBox()
        Me.T43 = New System.Windows.Forms.TextBox()
        Me.T44 = New System.Windows.Forms.TextBox()
        Me.T45 = New System.Windows.Forms.TextBox()
        Me.T51 = New System.Windows.Forms.TextBox()
        Me.T52 = New System.Windows.Forms.TextBox()
        Me.T53 = New System.Windows.Forms.TextBox()
        Me.T54 = New System.Windows.Forms.TextBox()
        Me.T55 = New System.Windows.Forms.TextBox()
        Me.T61 = New System.Windows.Forms.TextBox()
        Me.T62 = New System.Windows.Forms.TextBox()
        Me.T63 = New System.Windows.Forms.TextBox()
        Me.T64 = New System.Windows.Forms.TextBox()
        Me.T65 = New System.Windows.Forms.TextBox()
        Me.T71 = New System.Windows.Forms.TextBox()
        Me.T72 = New System.Windows.Forms.TextBox()
        Me.T73 = New System.Windows.Forms.TextBox()
        Me.T74 = New System.Windows.Forms.TextBox()
        Me.T75 = New System.Windows.Forms.TextBox()
        Me.LLeng = New System.Windows.Forms.Label()
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.TP1 = New System.Windows.Forms.TextBox()
        Me.TP2 = New System.Windows.Forms.TextBox()
        Me.LU1 = New System.Windows.Forms.Label()
        Me.LU2 = New System.Windows.Forms.Label()
        Me.LU3 = New System.Windows.Forms.Label()
        Me.LU4 = New System.Windows.Forms.Label()
        Me.LU5 = New System.Windows.Forms.Label()
        Me.LU6 = New System.Windows.Forms.Label()
        Me.LU7 = New System.Windows.Forms.Label()
        Me.BX1 = New System.Windows.Forms.Button()
        Me.BX2 = New System.Windows.Forms.Button()
        Me.BX3 = New System.Windows.Forms.Button()
        Me.BX4 = New System.Windows.Forms.Button()
        Me.BX5 = New System.Windows.Forms.Button()
        Me.BX6 = New System.Windows.Forms.Button()
        Me.BX7 = New System.Windows.Forms.Button()
        Me.CBKRef = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.CBTahunPendataan = New System.Windows.Forms.ComboBox()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BDownExcel
        '
        Me.BDownExcel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BDownExcel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BDownExcel.Location = New System.Drawing.Point(590, 399)
        Me.BDownExcel.Name = "BDownExcel"
        Me.BDownExcel.Size = New System.Drawing.Size(141, 21)
        Me.BDownExcel.TabIndex = 74
        Me.BDownExcel.Text = "Masukkan ke Excel"
        Me.BDownExcel.UseVisualStyleBackColor = True
        '
        'BKeluar
        '
        Me.BKeluar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BKeluar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BKeluar.Location = New System.Drawing.Point(590, 424)
        Me.BKeluar.Name = "BKeluar"
        Me.BKeluar.Size = New System.Drawing.Size(141, 21)
        Me.BKeluar.TabIndex = 76
        Me.BKeluar.Text = "Keluar"
        Me.BKeluar.UseVisualStyleBackColor = True
        '
        'BTambah
        '
        Me.BTambah.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BTambah.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTambah.Location = New System.Drawing.Point(435, 399)
        Me.BTambah.Name = "BTambah"
        Me.BTambah.Size = New System.Drawing.Size(141, 21)
        Me.BTambah.TabIndex = 73
        Me.BTambah.Text = "Tambah"
        Me.BTambah.UseVisualStyleBackColor = True
        '
        'groupBox1
        '
        Me.groupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox1.Location = New System.Drawing.Point(582, 136)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(5, 203)
        Me.groupBox1.TabIndex = 70
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "groupBox1"
        '
        'TBKode7
        '
        Me.TBKode7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKode7.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKode7.Location = New System.Drawing.Point(590, 317)
        Me.TBKode7.Name = "TBKode7"
        Me.TBKode7.Size = New System.Drawing.Size(124, 22)
        Me.TBKode7.TabIndex = 57
        '
        'TBPembangunan
        '
        Me.TBPembangunan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBPembangunan.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBPembangunan.Location = New System.Drawing.Point(435, 372)
        Me.TBPembangunan.Name = "TBPembangunan"
        Me.TBPembangunan.Size = New System.Drawing.Size(207, 22)
        Me.TBPembangunan.TabIndex = 69
        Me.TBPembangunan.Visible = False
        '
        'TBTanggal7
        '
        Me.TBTanggal7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTanggal7.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBTanggal7.Location = New System.Drawing.Point(435, 316)
        Me.TBTanggal7.Name = "TBTanggal7"
        Me.TBTanggal7.Size = New System.Drawing.Size(125, 22)
        Me.TBTanggal7.TabIndex = 50
        '
        'TBKode6
        '
        Me.TBKode6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKode6.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKode6.Location = New System.Drawing.Point(590, 291)
        Me.TBKode6.Name = "TBKode6"
        Me.TBKode6.Size = New System.Drawing.Size(124, 22)
        Me.TBKode6.TabIndex = 56
        '
        'TBTanggal6
        '
        Me.TBTanggal6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTanggal6.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBTanggal6.Location = New System.Drawing.Point(435, 290)
        Me.TBTanggal6.Name = "TBTanggal6"
        Me.TBTanggal6.Size = New System.Drawing.Size(125, 22)
        Me.TBTanggal6.TabIndex = 49
        '
        'TBKode3
        '
        Me.TBKode3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKode3.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKode3.Location = New System.Drawing.Point(590, 213)
        Me.TBKode3.Name = "TBKode3"
        Me.TBKode3.Size = New System.Drawing.Size(124, 22)
        Me.TBKode3.TabIndex = 53
        '
        'TBTanggal3
        '
        Me.TBTanggal3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTanggal3.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBTanggal3.Location = New System.Drawing.Point(435, 212)
        Me.TBTanggal3.Name = "TBTanggal3"
        Me.TBTanggal3.Size = New System.Drawing.Size(125, 22)
        Me.TBTanggal3.TabIndex = 46
        '
        'TBKode5
        '
        Me.TBKode5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKode5.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKode5.Location = New System.Drawing.Point(590, 265)
        Me.TBKode5.Name = "TBKode5"
        Me.TBKode5.Size = New System.Drawing.Size(124, 22)
        Me.TBKode5.TabIndex = 55
        '
        'TBTanggal5
        '
        Me.TBTanggal5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTanggal5.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBTanggal5.Location = New System.Drawing.Point(435, 264)
        Me.TBTanggal5.Name = "TBTanggal5"
        Me.TBTanggal5.Size = New System.Drawing.Size(125, 22)
        Me.TBTanggal5.TabIndex = 48
        '
        'TBKode2
        '
        Me.TBKode2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKode2.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKode2.Location = New System.Drawing.Point(590, 187)
        Me.TBKode2.Name = "TBKode2"
        Me.TBKode2.Size = New System.Drawing.Size(124, 22)
        Me.TBKode2.TabIndex = 52
        '
        'TBTanggal2
        '
        Me.TBTanggal2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTanggal2.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBTanggal2.Location = New System.Drawing.Point(435, 186)
        Me.TBTanggal2.Name = "TBTanggal2"
        Me.TBTanggal2.Size = New System.Drawing.Size(125, 22)
        Me.TBTanggal2.TabIndex = 45
        '
        'TBKode4
        '
        Me.TBKode4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKode4.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKode4.Location = New System.Drawing.Point(590, 239)
        Me.TBKode4.Name = "TBKode4"
        Me.TBKode4.Size = New System.Drawing.Size(124, 22)
        Me.TBKode4.TabIndex = 54
        '
        'TBTanggal4
        '
        Me.TBTanggal4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTanggal4.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBTanggal4.Location = New System.Drawing.Point(435, 238)
        Me.TBTanggal4.Name = "TBTanggal4"
        Me.TBTanggal4.Size = New System.Drawing.Size(125, 22)
        Me.TBTanggal4.TabIndex = 47
        '
        'TBKode1
        '
        Me.TBKode1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKode1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKode1.Location = New System.Drawing.Point(590, 161)
        Me.TBKode1.Name = "TBKode1"
        Me.TBKode1.Size = New System.Drawing.Size(124, 22)
        Me.TBKode1.TabIndex = 51
        '
        'TBTanggal1
        '
        Me.TBTanggal1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTanggal1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBTanggal1.Location = New System.Drawing.Point(435, 160)
        Me.TBTanggal1.Name = "TBTanggal1"
        Me.TBTanggal1.Size = New System.Drawing.Size(125, 22)
        Me.TBTanggal1.TabIndex = 44
        '
        'TBNama7
        '
        Me.TBNama7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNama7.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.TBNama7.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNama7.Location = New System.Drawing.Point(215, 316)
        Me.TBNama7.Name = "TBNama7"
        Me.TBNama7.Size = New System.Drawing.Size(214, 22)
        Me.TBNama7.TabIndex = 43
        '
        'TBNama6
        '
        Me.TBNama6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNama6.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.TBNama6.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNama6.Location = New System.Drawing.Point(215, 290)
        Me.TBNama6.Name = "TBNama6"
        Me.TBNama6.Size = New System.Drawing.Size(214, 22)
        Me.TBNama6.TabIndex = 42
        '
        'TBNama3
        '
        Me.TBNama3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNama3.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.TBNama3.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNama3.Location = New System.Drawing.Point(215, 212)
        Me.TBNama3.Name = "TBNama3"
        Me.TBNama3.Size = New System.Drawing.Size(214, 22)
        Me.TBNama3.TabIndex = 39
        '
        'TBNama5
        '
        Me.TBNama5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNama5.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.TBNama5.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNama5.Location = New System.Drawing.Point(215, 264)
        Me.TBNama5.Name = "TBNama5"
        Me.TBNama5.Size = New System.Drawing.Size(214, 22)
        Me.TBNama5.TabIndex = 41
        '
        'TBNama2
        '
        Me.TBNama2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNama2.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.TBNama2.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNama2.Location = New System.Drawing.Point(215, 186)
        Me.TBNama2.Name = "TBNama2"
        Me.TBNama2.Size = New System.Drawing.Size(214, 22)
        Me.TBNama2.TabIndex = 38
        '
        'TBNama4
        '
        Me.TBNama4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNama4.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.TBNama4.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNama4.Location = New System.Drawing.Point(215, 238)
        Me.TBNama4.Name = "TBNama4"
        Me.TBNama4.Size = New System.Drawing.Size(214, 22)
        Me.TBNama4.TabIndex = 40
        '
        'TBNama1
        '
        Me.TBNama1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNama1.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.TBNama1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNama1.Location = New System.Drawing.Point(215, 160)
        Me.TBNama1.Name = "TBNama1"
        Me.TBNama1.Size = New System.Drawing.Size(214, 22)
        Me.TBNama1.TabIndex = 37
        '
        'TBLamaBKb
        '
        Me.TBLamaBKb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBLamaBKb.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBLamaBKb.Location = New System.Drawing.Point(385, 347)
        Me.TBLamaBKb.Margin = New System.Windows.Forms.Padding(1, 3, 1, 3)
        Me.TBLamaBKb.MaxLength = 2
        Me.TBLamaBKb.Name = "TBLamaBKb"
        Me.TBLamaBKb.Size = New System.Drawing.Size(44, 22)
        Me.TBLamaBKb.TabIndex = 64
        '
        'TBKawinIs
        '
        Me.TBKawinIs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKawinIs.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKawinIs.Location = New System.Drawing.Point(165, 347)
        Me.TBKawinIs.Margin = New System.Windows.Forms.Padding(1, 3, 1, 3)
        Me.TBKawinIs.MaxLength = 2
        Me.TBKawinIs.Name = "TBKawinIs"
        Me.TBKawinIs.Size = New System.Drawing.Size(45, 22)
        Me.TBKawinIs.TabIndex = 59
        '
        'TBTmpatKb
        '
        Me.TBTmpatKb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTmpatKb.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBTmpatKb.Location = New System.Drawing.Point(337, 425)
        Me.TBTmpatKb.MaxLength = 2
        Me.TBTmpatKb.Name = "TBTmpatKb"
        Me.TBTmpatKb.Size = New System.Drawing.Size(92, 22)
        Me.TBTmpatKb.TabIndex = 68
        '
        'TBKontra
        '
        Me.TBKontra.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKontra.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKontra.Location = New System.Drawing.Point(117, 425)
        Me.TBKontra.MaxLength = 1
        Me.TBKontra.Name = "TBKontra"
        Me.TBKontra.Size = New System.Drawing.Size(92, 22)
        Me.TBKontra.TabIndex = 62
        '
        'TBAlasanTidakKb
        '
        Me.TBAlasanTidakKb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBAlasanTidakKb.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBAlasanTidakKb.Location = New System.Drawing.Point(337, 399)
        Me.TBAlasanTidakKb.MaxLength = 1
        Me.TBAlasanTidakKb.Name = "TBAlasanTidakKb"
        Me.TBAlasanTidakKb.Size = New System.Drawing.Size(92, 22)
        Me.TBAlasanTidakKb.TabIndex = 67
        '
        'TBKb
        '
        Me.TBKb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKb.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKb.Location = New System.Drawing.Point(117, 399)
        Me.TBKb.MaxLength = 1
        Me.TBKb.Name = "TBKb"
        Me.TBKb.Size = New System.Drawing.Size(92, 22)
        Me.TBKb.TabIndex = 61
        '
        'TBPunyaAnak
        '
        Me.TBPunyaAnak.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBPunyaAnak.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBPunyaAnak.Location = New System.Drawing.Point(337, 373)
        Me.TBPunyaAnak.MaxLength = 1
        Me.TBPunyaAnak.Name = "TBPunyaAnak"
        Me.TBPunyaAnak.Size = New System.Drawing.Size(92, 22)
        Me.TBPunyaAnak.TabIndex = 66
        '
        'TBJumlahAnak
        '
        Me.TBJumlahAnak.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBJumlahAnak.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBJumlahAnak.Location = New System.Drawing.Point(117, 373)
        Me.TBJumlahAnak.MaxLength = 8
        Me.TBJumlahAnak.Name = "TBJumlahAnak"
        Me.TBJumlahAnak.Size = New System.Drawing.Size(92, 22)
        Me.TBJumlahAnak.TabIndex = 60
        '
        'TBLamaTKb
        '
        Me.TBLamaTKb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBLamaTKb.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBLamaTKb.Location = New System.Drawing.Point(337, 347)
        Me.TBLamaTKb.Margin = New System.Windows.Forms.Padding(1, 3, 1, 3)
        Me.TBLamaTKb.MaxLength = 2
        Me.TBLamaTKb.Name = "TBLamaTKb"
        Me.TBLamaTKb.Size = New System.Drawing.Size(44, 22)
        Me.TBLamaTKb.TabIndex = 63
        '
        'TBKawinSu
        '
        Me.TBKawinSu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKawinSu.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKawinSu.Location = New System.Drawing.Point(117, 347)
        Me.TBKawinSu.Margin = New System.Windows.Forms.Padding(1, 3, 1, 3)
        Me.TBKawinSu.MaxLength = 2
        Me.TBKawinSu.Name = "TBKawinSu"
        Me.TBKawinSu.Size = New System.Drawing.Size(44, 22)
        Me.TBKawinSu.TabIndex = 58
        '
        'TBNoKeluarga
        '
        Me.TBNoKeluarga.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNoKeluarga.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNoKeluarga.Location = New System.Drawing.Point(92, 113)
        Me.TBNoKeluarga.Name = "TBNoKeluarga"
        Me.TBNoKeluarga.Size = New System.Drawing.Size(100, 22)
        Me.TBNoKeluarga.TabIndex = 1
        '
        'TBNoRumah
        '
        Me.TBNoRumah.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNoRumah.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNoRumah.Location = New System.Drawing.Point(92, 87)
        Me.TBNoRumah.Name = "TBNoRumah"
        Me.TBNoRumah.Size = New System.Drawing.Size(100, 22)
        Me.TBNoRumah.TabIndex = 0
        '
        'label5
        '
        Me.label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label5.BackColor = System.Drawing.Color.Transparent
        Me.label5.Location = New System.Drawing.Point(435, 141)
        Me.label5.Margin = New System.Windows.Forms.Padding(3)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(141, 13)
        Me.label5.TabIndex = 9
        Me.label5.Text = "Tanggal lahir"
        Me.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label4
        '
        Me.label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label4.BackColor = System.Drawing.Color.Transparent
        Me.label4.Location = New System.Drawing.Point(215, 141)
        Me.label4.Margin = New System.Windows.Forms.Padding(3)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(214, 13)
        Me.label4.TabIndex = 10
        Me.label4.Text = "Nama"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label20
        '
        Me.label20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label20.BackColor = System.Drawing.Color.Transparent
        Me.label20.Location = New System.Drawing.Point(593, 141)
        Me.label20.Margin = New System.Windows.Forms.Padding(3)
        Me.label20.Name = "label20"
        Me.label20.Size = New System.Drawing.Size(121, 13)
        Me.label20.TabIndex = 11
        Me.label20.Text = "Kode Anggota Keluarga"
        Me.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label3
        '
        Me.label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label3.BackColor = System.Drawing.Color.Transparent
        Me.label3.Location = New System.Drawing.Point(26, 141)
        Me.label3.Margin = New System.Windows.Forms.Padding(3)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(183, 13)
        Me.label3.TabIndex = 12
        Me.label3.Text = "NIK"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label12
        '
        Me.label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label12.BackColor = System.Drawing.Color.Transparent
        Me.label12.Location = New System.Drawing.Point(6, 319)
        Me.label12.Margin = New System.Windows.Forms.Padding(3)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(24, 17)
        Me.label12.TabIndex = 18
        Me.label12.Text = "7."
        '
        'label11
        '
        Me.label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label11.BackColor = System.Drawing.Color.Transparent
        Me.label11.Location = New System.Drawing.Point(6, 293)
        Me.label11.Margin = New System.Windows.Forms.Padding(3)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(24, 17)
        Me.label11.TabIndex = 13
        Me.label11.Text = "6."
        '
        'label10
        '
        Me.label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label10.BackColor = System.Drawing.Color.Transparent
        Me.label10.Location = New System.Drawing.Point(6, 267)
        Me.label10.Margin = New System.Windows.Forms.Padding(3)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(24, 17)
        Me.label10.TabIndex = 14
        Me.label10.Text = "5."
        '
        'label9
        '
        Me.label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label9.BackColor = System.Drawing.Color.Transparent
        Me.label9.Location = New System.Drawing.Point(6, 241)
        Me.label9.Margin = New System.Windows.Forms.Padding(3)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(24, 17)
        Me.label9.TabIndex = 15
        Me.label9.Text = "4."
        '
        'label8
        '
        Me.label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label8.BackColor = System.Drawing.Color.Transparent
        Me.label8.Location = New System.Drawing.Point(6, 215)
        Me.label8.Margin = New System.Windows.Forms.Padding(3)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(24, 17)
        Me.label8.TabIndex = 16
        Me.label8.Text = "3."
        '
        'label7
        '
        Me.label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label7.BackColor = System.Drawing.Color.Transparent
        Me.label7.Location = New System.Drawing.Point(6, 189)
        Me.label7.Margin = New System.Windows.Forms.Padding(3)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(24, 17)
        Me.label7.TabIndex = 17
        Me.label7.Text = "2."
        '
        'label21
        '
        Me.label21.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label21.BackColor = System.Drawing.Color.Transparent
        Me.label21.Location = New System.Drawing.Point(215, 428)
        Me.label21.Margin = New System.Windows.Forms.Padding(3)
        Me.label21.Name = "label21"
        Me.label21.Size = New System.Drawing.Size(114, 13)
        Me.label21.TabIndex = 20
        Me.label21.Text = "Tempat pelayanan KB"
        '
        'label16
        '
        Me.label16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label16.BackColor = System.Drawing.Color.Transparent
        Me.label16.Location = New System.Drawing.Point(6, 428)
        Me.label16.Margin = New System.Windows.Forms.Padding(3)
        Me.label16.Name = "label16"
        Me.label16.Size = New System.Drawing.Size(114, 13)
        Me.label16.TabIndex = 19
        Me.label16.Text = "Metode Kontrasepsi"
        '
        'label6
        '
        Me.label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label6.BackColor = System.Drawing.Color.Transparent
        Me.label6.Location = New System.Drawing.Point(6, 163)
        Me.label6.Margin = New System.Windows.Forms.Padding(3)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(24, 17)
        Me.label6.TabIndex = 28
        Me.label6.Text = "1."
        '
        'label19
        '
        Me.label19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label19.BackColor = System.Drawing.Color.Transparent
        Me.label19.Location = New System.Drawing.Point(215, 402)
        Me.label19.Margin = New System.Windows.Forms.Padding(3)
        Me.label19.Name = "label19"
        Me.label19.Size = New System.Drawing.Size(114, 13)
        Me.label19.TabIndex = 27
        Me.label19.Text = "Alasan Tidak KB"
        '
        'label15
        '
        Me.label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label15.BackColor = System.Drawing.Color.Transparent
        Me.label15.Location = New System.Drawing.Point(6, 402)
        Me.label15.Margin = New System.Windows.Forms.Padding(3)
        Me.label15.Name = "label15"
        Me.label15.Size = New System.Drawing.Size(114, 13)
        Me.label15.TabIndex = 26
        Me.label15.Text = "Ber KB ?"
        '
        'label18
        '
        Me.label18.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label18.BackColor = System.Drawing.Color.Transparent
        Me.label18.Location = New System.Drawing.Point(215, 376)
        Me.label18.Margin = New System.Windows.Forms.Padding(3)
        Me.label18.Name = "label18"
        Me.label18.Size = New System.Drawing.Size(114, 13)
        Me.label18.TabIndex = 29
        Me.label18.Text = "Punya Anak Lagi ?"
        '
        'label14
        '
        Me.label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label14.BackColor = System.Drawing.Color.Transparent
        Me.label14.Location = New System.Drawing.Point(6, 376)
        Me.label14.Margin = New System.Windows.Forms.Padding(3)
        Me.label14.Name = "label14"
        Me.label14.Size = New System.Drawing.Size(114, 13)
        Me.label14.TabIndex = 24
        Me.label14.Text = "Jumlah Anak"
        '
        'label22
        '
        Me.label22.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label22.BackColor = System.Drawing.Color.Transparent
        Me.label22.Location = New System.Drawing.Point(435, 350)
        Me.label22.Margin = New System.Windows.Forms.Padding(3)
        Me.label22.Name = "label22"
        Me.label22.Size = New System.Drawing.Size(296, 13)
        Me.label22.TabIndex = 23
        Me.label22.Text = "Pembangunan Keluarga"
        Me.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label17
        '
        Me.label17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label17.BackColor = System.Drawing.Color.Transparent
        Me.label17.Location = New System.Drawing.Point(215, 350)
        Me.label17.Margin = New System.Windows.Forms.Padding(3)
        Me.label17.Name = "label17"
        Me.label17.Size = New System.Drawing.Size(114, 13)
        Me.label17.TabIndex = 22
        Me.label17.Text = "Lama Ber-KB"
        '
        'label13
        '
        Me.label13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label13.BackColor = System.Drawing.Color.Transparent
        Me.label13.Location = New System.Drawing.Point(6, 350)
        Me.label13.Margin = New System.Windows.Forms.Padding(3)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(114, 13)
        Me.label13.TabIndex = 21
        Me.label13.Text = "Usia kawin"
        '
        'label1
        '
        Me.label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label1.BackColor = System.Drawing.Color.Transparent
        Me.label1.Location = New System.Drawing.Point(6, 117)
        Me.label1.Margin = New System.Windows.Forms.Padding(3)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(80, 13)
        Me.label1.TabIndex = 25
        Me.label1.Text = "No. Keluarga"
        '
        'label2
        '
        Me.label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label2.BackColor = System.Drawing.Color.Transparent
        Me.label2.Location = New System.Drawing.Point(6, 91)
        Me.label2.Margin = New System.Windows.Forms.Padding(3)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(80, 13)
        Me.label2.TabIndex = 7
        Me.label2.Text = "No. Rumah"
        '
        'TBLuasRumah
        '
        Me.TBLuasRumah.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBLuasRumah.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBLuasRumah.Location = New System.Drawing.Point(665, 372)
        Me.TBLuasRumah.MaxLength = 4
        Me.TBLuasRumah.Name = "TBLuasRumah"
        Me.TBLuasRumah.Size = New System.Drawing.Size(40, 22)
        Me.TBLuasRumah.TabIndex = 71
        '
        'TBOrang
        '
        Me.TBOrang.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBOrang.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBOrang.Location = New System.Drawing.Point(711, 372)
        Me.TBOrang.MaxLength = 2
        Me.TBOrang.Name = "TBOrang"
        Me.TBOrang.Size = New System.Drawing.Size(20, 22)
        Me.TBOrang.TabIndex = 72
        '
        'Label23
        '
        Me.Label23.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.Label23.Location = New System.Drawing.Point(347, 43)
        Me.Label23.Margin = New System.Windows.Forms.Padding(3)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(244, 31)
        Me.Label23.TabIndex = 10
        Me.Label23.Text = "Entri Data BKKBN"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label24
        '
        Me.Label24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Location = New System.Drawing.Point(6, 38)
        Me.Label24.Margin = New System.Windows.Forms.Padding(3)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(80, 13)
        Me.Label24.TabIndex = 7
        Me.Label24.Text = "No. Kendali"
        '
        'TBKendali
        '
        Me.TBKendali.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBKendali.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBKendali.Location = New System.Drawing.Point(92, 34)
        Me.TBKendali.Name = "TBKendali"
        Me.TBKendali.Size = New System.Drawing.Size(100, 22)
        Me.TBKendali.TabIndex = 0
        '
        'Timer1
        '
        Me.Timer1.Interval = 10
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LInfo, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 454)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(740, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 71
        Me.StatusStrip1.Text = "Data Keluarga - Bundle 1.dft"
        '
        'LInfo
        '
        Me.LInfo.AutoSize = False
        Me.LInfo.IsLink = True
        Me.LInfo.LinkColor = System.Drawing.Color.Black
        Me.LInfo.Name = "LInfo"
        Me.LInfo.Size = New System.Drawing.Size(570, 17)
        Me.LInfo.Text = "ToolStripStatusLabel1"
        Me.LInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.AutoSize = False
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ToolStripStatusLabel1.IsLink = True
        Me.ToolStripStatusLabel1.LinkColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(170, 17)
        Me.ToolStripStatusLabel1.Text = "Data Keluarga - Bundle 1.dft"
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolStripStatusLabel1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal
        '
        'BDataBaru
        '
        Me.BDataBaru.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BDataBaru.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BDataBaru.Location = New System.Drawing.Point(435, 424)
        Me.BDataBaru.Name = "BDataBaru"
        Me.BDataBaru.Size = New System.Drawing.Size(141, 21)
        Me.BDataBaru.TabIndex = 75
        Me.BDataBaru.Text = "Bundle Baru"
        Me.BDataBaru.UseVisualStyleBackColor = True
        '
        'TBNik1
        '
        Me.TBNik1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNik1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNik1.Location = New System.Drawing.Point(215, 160)
        Me.TBNik1.Name = "TBNik1"
        Me.TBNik1.Size = New System.Drawing.Size(180, 22)
        Me.TBNik1.TabIndex = 80
        Me.TBNik1.Visible = False
        '
        'TBNik4
        '
        Me.TBNik4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNik4.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNik4.Location = New System.Drawing.Point(215, 238)
        Me.TBNik4.Name = "TBNik4"
        Me.TBNik4.Size = New System.Drawing.Size(180, 22)
        Me.TBNik4.TabIndex = 83
        Me.TBNik4.Visible = False
        '
        'TBNik2
        '
        Me.TBNik2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNik2.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNik2.Location = New System.Drawing.Point(215, 186)
        Me.TBNik2.Name = "TBNik2"
        Me.TBNik2.Size = New System.Drawing.Size(180, 22)
        Me.TBNik2.TabIndex = 81
        Me.TBNik2.Visible = False
        '
        'TBNik5
        '
        Me.TBNik5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNik5.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNik5.Location = New System.Drawing.Point(215, 264)
        Me.TBNik5.Name = "TBNik5"
        Me.TBNik5.Size = New System.Drawing.Size(180, 22)
        Me.TBNik5.TabIndex = 84
        Me.TBNik5.Visible = False
        '
        'TBNik3
        '
        Me.TBNik3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNik3.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNik3.Location = New System.Drawing.Point(215, 212)
        Me.TBNik3.Name = "TBNik3"
        Me.TBNik3.Size = New System.Drawing.Size(180, 22)
        Me.TBNik3.TabIndex = 82
        Me.TBNik3.Visible = False
        '
        'TBNik6
        '
        Me.TBNik6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNik6.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNik6.Location = New System.Drawing.Point(215, 290)
        Me.TBNik6.Name = "TBNik6"
        Me.TBNik6.Size = New System.Drawing.Size(180, 22)
        Me.TBNik6.TabIndex = 85
        Me.TBNik6.Visible = False
        '
        'TBNik7
        '
        Me.TBNik7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBNik7.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TBNik7.Location = New System.Drawing.Point(215, 316)
        Me.TBNik7.Name = "TBNik7"
        Me.TBNik7.Size = New System.Drawing.Size(180, 22)
        Me.TBNik7.TabIndex = 86
        Me.TBNik7.Visible = False
        '
        'Label25
        '
        Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label25.Location = New System.Drawing.Point(342, 11)
        Me.Label25.Margin = New System.Windows.Forms.Padding(3)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(389, 13)
        Me.Label25.TabIndex = 11
        Me.Label25.Text = "By. Aziz Nur Ariffianto"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label26
        '
        Me.Label26.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label26.Location = New System.Drawing.Point(198, 11)
        Me.Label26.Margin = New System.Windows.Forms.Padding(3)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(138, 13)
        Me.Label26.TabIndex = 11
        Me.Label26.Text = "Cara input ?"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Timer2
        '
        Me.Timer2.Interval = 2000
        '
        'BFix
        '
        Me.BFix.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BFix.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BFix.Location = New System.Drawing.Point(435, 345)
        Me.BFix.Name = "BFix"
        Me.BFix.Size = New System.Drawing.Size(28, 24)
        Me.BFix.TabIndex = 65
        Me.BFix.Text = "Fix"
        Me.BFix.UseVisualStyleBackColor = True
        Me.BFix.Visible = False
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label27.ForeColor = System.Drawing.Color.Red
        Me.Label27.Location = New System.Drawing.Point(563, 92)
        Me.Label27.Margin = New System.Windows.Forms.Padding(3)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(170, 13)
        Me.Label27.TabIndex = 11
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TUpdate
        '
        Me.TUpdate.Interval = 3000
        '
        'TTimeOut
        '
        Me.TTimeOut.Interval = 20000
        '
        'TRefresh
        '
        Me.TRefresh.Enabled = True
        Me.TRefresh.Interval = 1
        '
        'Button1
        '
        Me.Button1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Location = New System.Drawing.Point(198, 113)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(93, 23)
        Me.Button1.TabIndex = 74
        Me.Button1.Text = "Mode Edit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CBKendali
        '
        Me.CBKendali.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CBKendali.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBKendali.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.CBKendali.FormattingEnabled = True
        Me.CBKendali.Location = New System.Drawing.Point(92, 34)
        Me.CBKendali.Name = "CBKendali"
        Me.CBKendali.Size = New System.Drawing.Size(100, 22)
        Me.CBKendali.TabIndex = 75
        Me.CBKendali.Visible = False
        '
        'T11
        '
        Me.T11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T11.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T11.Location = New System.Drawing.Point(29, 160)
        Me.T11.Name = "T11"
        Me.T11.Size = New System.Drawing.Size(60, 22)
        Me.T11.TabIndex = 2
        '
        'T12
        '
        Me.T12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T12.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T12.Location = New System.Drawing.Point(89, 160)
        Me.T12.Name = "T12"
        Me.T12.Size = New System.Drawing.Size(22, 22)
        Me.T12.TabIndex = 3
        '
        'T13
        '
        Me.T13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T13.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T13.Location = New System.Drawing.Point(110, 160)
        Me.T13.Name = "T13"
        Me.T13.Size = New System.Drawing.Size(22, 22)
        Me.T13.TabIndex = 4
        '
        'T14
        '
        Me.T14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T14.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T14.Location = New System.Drawing.Point(131, 160)
        Me.T14.Name = "T14"
        Me.T14.Size = New System.Drawing.Size(22, 22)
        Me.T14.TabIndex = 5
        '
        'T15
        '
        Me.T15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T15.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T15.Location = New System.Drawing.Point(153, 160)
        Me.T15.Name = "T15"
        Me.T15.Size = New System.Drawing.Size(56, 22)
        Me.T15.TabIndex = 6
        '
        'T21
        '
        Me.T21.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T21.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T21.Location = New System.Drawing.Point(29, 186)
        Me.T21.Name = "T21"
        Me.T21.Size = New System.Drawing.Size(60, 22)
        Me.T21.TabIndex = 7
        '
        'T22
        '
        Me.T22.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T22.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T22.Location = New System.Drawing.Point(89, 186)
        Me.T22.Name = "T22"
        Me.T22.Size = New System.Drawing.Size(22, 22)
        Me.T22.TabIndex = 8
        '
        'T23
        '
        Me.T23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T23.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T23.Location = New System.Drawing.Point(110, 186)
        Me.T23.Name = "T23"
        Me.T23.Size = New System.Drawing.Size(22, 22)
        Me.T23.TabIndex = 9
        '
        'T24
        '
        Me.T24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T24.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T24.Location = New System.Drawing.Point(131, 186)
        Me.T24.Name = "T24"
        Me.T24.Size = New System.Drawing.Size(22, 22)
        Me.T24.TabIndex = 10
        '
        'T25
        '
        Me.T25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T25.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T25.Location = New System.Drawing.Point(153, 186)
        Me.T25.Name = "T25"
        Me.T25.Size = New System.Drawing.Size(56, 22)
        Me.T25.TabIndex = 11
        '
        'T31
        '
        Me.T31.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T31.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T31.Location = New System.Drawing.Point(29, 213)
        Me.T31.Name = "T31"
        Me.T31.Size = New System.Drawing.Size(60, 22)
        Me.T31.TabIndex = 12
        '
        'T32
        '
        Me.T32.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T32.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T32.Location = New System.Drawing.Point(89, 213)
        Me.T32.Name = "T32"
        Me.T32.Size = New System.Drawing.Size(22, 22)
        Me.T32.TabIndex = 13
        '
        'T33
        '
        Me.T33.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T33.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T33.Location = New System.Drawing.Point(110, 213)
        Me.T33.Name = "T33"
        Me.T33.Size = New System.Drawing.Size(22, 22)
        Me.T33.TabIndex = 14
        '
        'T34
        '
        Me.T34.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T34.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T34.Location = New System.Drawing.Point(131, 213)
        Me.T34.Name = "T34"
        Me.T34.Size = New System.Drawing.Size(22, 22)
        Me.T34.TabIndex = 15
        '
        'T35
        '
        Me.T35.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T35.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T35.Location = New System.Drawing.Point(153, 213)
        Me.T35.Name = "T35"
        Me.T35.Size = New System.Drawing.Size(56, 22)
        Me.T35.TabIndex = 16
        '
        'T41
        '
        Me.T41.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T41.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T41.Location = New System.Drawing.Point(29, 238)
        Me.T41.Name = "T41"
        Me.T41.Size = New System.Drawing.Size(60, 22)
        Me.T41.TabIndex = 17
        '
        'T42
        '
        Me.T42.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T42.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T42.Location = New System.Drawing.Point(89, 238)
        Me.T42.Name = "T42"
        Me.T42.Size = New System.Drawing.Size(22, 22)
        Me.T42.TabIndex = 18
        '
        'T43
        '
        Me.T43.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T43.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T43.Location = New System.Drawing.Point(110, 238)
        Me.T43.Name = "T43"
        Me.T43.Size = New System.Drawing.Size(22, 22)
        Me.T43.TabIndex = 19
        '
        'T44
        '
        Me.T44.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T44.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T44.Location = New System.Drawing.Point(131, 238)
        Me.T44.Name = "T44"
        Me.T44.Size = New System.Drawing.Size(22, 22)
        Me.T44.TabIndex = 20
        '
        'T45
        '
        Me.T45.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T45.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T45.Location = New System.Drawing.Point(153, 238)
        Me.T45.Name = "T45"
        Me.T45.Size = New System.Drawing.Size(56, 22)
        Me.T45.TabIndex = 21
        '
        'T51
        '
        Me.T51.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T51.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T51.Location = New System.Drawing.Point(29, 264)
        Me.T51.Name = "T51"
        Me.T51.Size = New System.Drawing.Size(60, 22)
        Me.T51.TabIndex = 22
        '
        'T52
        '
        Me.T52.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T52.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T52.Location = New System.Drawing.Point(89, 264)
        Me.T52.Name = "T52"
        Me.T52.Size = New System.Drawing.Size(22, 22)
        Me.T52.TabIndex = 23
        '
        'T53
        '
        Me.T53.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T53.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T53.Location = New System.Drawing.Point(110, 264)
        Me.T53.Name = "T53"
        Me.T53.Size = New System.Drawing.Size(22, 22)
        Me.T53.TabIndex = 24
        '
        'T54
        '
        Me.T54.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T54.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T54.Location = New System.Drawing.Point(131, 264)
        Me.T54.Name = "T54"
        Me.T54.Size = New System.Drawing.Size(22, 22)
        Me.T54.TabIndex = 25
        '
        'T55
        '
        Me.T55.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T55.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T55.Location = New System.Drawing.Point(153, 264)
        Me.T55.Name = "T55"
        Me.T55.Size = New System.Drawing.Size(56, 22)
        Me.T55.TabIndex = 26
        '
        'T61
        '
        Me.T61.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T61.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T61.Location = New System.Drawing.Point(29, 290)
        Me.T61.Name = "T61"
        Me.T61.Size = New System.Drawing.Size(60, 22)
        Me.T61.TabIndex = 27
        '
        'T62
        '
        Me.T62.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T62.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T62.Location = New System.Drawing.Point(89, 290)
        Me.T62.Name = "T62"
        Me.T62.Size = New System.Drawing.Size(22, 22)
        Me.T62.TabIndex = 28
        '
        'T63
        '
        Me.T63.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T63.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T63.Location = New System.Drawing.Point(110, 290)
        Me.T63.Name = "T63"
        Me.T63.Size = New System.Drawing.Size(22, 22)
        Me.T63.TabIndex = 29
        '
        'T64
        '
        Me.T64.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T64.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T64.Location = New System.Drawing.Point(131, 290)
        Me.T64.Name = "T64"
        Me.T64.Size = New System.Drawing.Size(22, 22)
        Me.T64.TabIndex = 30
        '
        'T65
        '
        Me.T65.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T65.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T65.Location = New System.Drawing.Point(153, 290)
        Me.T65.Name = "T65"
        Me.T65.Size = New System.Drawing.Size(56, 22)
        Me.T65.TabIndex = 31
        '
        'T71
        '
        Me.T71.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T71.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T71.Location = New System.Drawing.Point(29, 316)
        Me.T71.Name = "T71"
        Me.T71.Size = New System.Drawing.Size(60, 22)
        Me.T71.TabIndex = 32
        '
        'T72
        '
        Me.T72.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T72.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T72.Location = New System.Drawing.Point(89, 316)
        Me.T72.Name = "T72"
        Me.T72.Size = New System.Drawing.Size(22, 22)
        Me.T72.TabIndex = 33
        '
        'T73
        '
        Me.T73.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T73.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T73.Location = New System.Drawing.Point(110, 316)
        Me.T73.Name = "T73"
        Me.T73.Size = New System.Drawing.Size(22, 22)
        Me.T73.TabIndex = 34
        '
        'T74
        '
        Me.T74.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T74.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T74.Location = New System.Drawing.Point(131, 316)
        Me.T74.Name = "T74"
        Me.T74.Size = New System.Drawing.Size(22, 22)
        Me.T74.TabIndex = 35
        '
        'T75
        '
        Me.T75.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.T75.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.T75.Location = New System.Drawing.Point(153, 316)
        Me.T75.Name = "T75"
        Me.T75.Size = New System.Drawing.Size(56, 22)
        Me.T75.TabIndex = 36
        '
        'LLeng
        '
        Me.LLeng.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LLeng.AutoSize = True
        Me.LLeng.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LLeng.Location = New System.Drawing.Point(642, 376)
        Me.LLeng.Name = "LLeng"
        Me.LLeng.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LLeng.Size = New System.Drawing.Size(14, 14)
        Me.LLeng.TabIndex = 87
        Me.LLeng.Text = "0"
        '
        'Timer3
        '
        Me.Timer3.Enabled = True
        Me.Timer3.Interval = 10000
        '
        'TP1
        '
        Me.TP1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TP1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TP1.Location = New System.Drawing.Point(435, 372)
        Me.TP1.Name = "TP1"
        Me.TP1.Size = New System.Drawing.Size(138, 22)
        Me.TP1.TabIndex = 69
        '
        'TP2
        '
        Me.TP2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TP2.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TP2.Location = New System.Drawing.Point(572, 372)
        Me.TP2.Name = "TP2"
        Me.TP2.Size = New System.Drawing.Size(70, 22)
        Me.TP2.TabIndex = 70
        '
        'LU1
        '
        Me.LU1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LU1.AutoSize = True
        Me.LU1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LU1.Location = New System.Drawing.Point(561, 165)
        Me.LU1.Name = "LU1"
        Me.LU1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LU1.Size = New System.Drawing.Size(14, 14)
        Me.LU1.TabIndex = 88
        Me.LU1.Text = "0"
        '
        'LU2
        '
        Me.LU2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LU2.AutoSize = True
        Me.LU2.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LU2.Location = New System.Drawing.Point(561, 191)
        Me.LU2.Name = "LU2"
        Me.LU2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LU2.Size = New System.Drawing.Size(14, 14)
        Me.LU2.TabIndex = 88
        Me.LU2.Text = "0"
        '
        'LU3
        '
        Me.LU3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LU3.AutoSize = True
        Me.LU3.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LU3.Location = New System.Drawing.Point(561, 218)
        Me.LU3.Name = "LU3"
        Me.LU3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LU3.Size = New System.Drawing.Size(14, 14)
        Me.LU3.TabIndex = 88
        Me.LU3.Text = "0"
        '
        'LU4
        '
        Me.LU4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LU4.AutoSize = True
        Me.LU4.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LU4.Location = New System.Drawing.Point(561, 243)
        Me.LU4.Name = "LU4"
        Me.LU4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LU4.Size = New System.Drawing.Size(14, 14)
        Me.LU4.TabIndex = 88
        Me.LU4.Text = "0"
        '
        'LU5
        '
        Me.LU5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LU5.AutoSize = True
        Me.LU5.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LU5.Location = New System.Drawing.Point(561, 269)
        Me.LU5.Name = "LU5"
        Me.LU5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LU5.Size = New System.Drawing.Size(14, 14)
        Me.LU5.TabIndex = 88
        Me.LU5.Text = "0"
        '
        'LU6
        '
        Me.LU6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LU6.AutoSize = True
        Me.LU6.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LU6.Location = New System.Drawing.Point(561, 294)
        Me.LU6.Name = "LU6"
        Me.LU6.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LU6.Size = New System.Drawing.Size(14, 14)
        Me.LU6.TabIndex = 88
        Me.LU6.Text = "0"
        '
        'LU7
        '
        Me.LU7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LU7.AutoSize = True
        Me.LU7.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LU7.Location = New System.Drawing.Point(561, 321)
        Me.LU7.Name = "LU7"
        Me.LU7.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LU7.Size = New System.Drawing.Size(14, 14)
        Me.LU7.TabIndex = 88
        Me.LU7.Text = "0"
        '
        'BX1
        '
        Me.BX1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BX1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BX1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BX1.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Bold)
        Me.BX1.ForeColor = System.Drawing.Color.DimGray
        Me.BX1.Location = New System.Drawing.Point(719, 161)
        Me.BX1.Name = "BX1"
        Me.BX1.Size = New System.Drawing.Size(14, 21)
        Me.BX1.TabIndex = 89
        Me.BX1.Text = "X"
        Me.BX1.UseVisualStyleBackColor = True
        '
        'BX2
        '
        Me.BX2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BX2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BX2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BX2.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Bold)
        Me.BX2.ForeColor = System.Drawing.Color.DimGray
        Me.BX2.Location = New System.Drawing.Point(719, 187)
        Me.BX2.Name = "BX2"
        Me.BX2.Size = New System.Drawing.Size(14, 21)
        Me.BX2.TabIndex = 89
        Me.BX2.Text = "X"
        Me.BX2.UseVisualStyleBackColor = True
        '
        'BX3
        '
        Me.BX3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BX3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BX3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BX3.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Bold)
        Me.BX3.ForeColor = System.Drawing.Color.DimGray
        Me.BX3.Location = New System.Drawing.Point(719, 213)
        Me.BX3.Name = "BX3"
        Me.BX3.Size = New System.Drawing.Size(14, 21)
        Me.BX3.TabIndex = 89
        Me.BX3.Text = "X"
        Me.BX3.UseVisualStyleBackColor = True
        '
        'BX4
        '
        Me.BX4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BX4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BX4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BX4.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Bold)
        Me.BX4.ForeColor = System.Drawing.Color.DimGray
        Me.BX4.Location = New System.Drawing.Point(719, 239)
        Me.BX4.Name = "BX4"
        Me.BX4.Size = New System.Drawing.Size(14, 21)
        Me.BX4.TabIndex = 89
        Me.BX4.Text = "X"
        Me.BX4.UseVisualStyleBackColor = True
        '
        'BX5
        '
        Me.BX5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BX5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BX5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BX5.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Bold)
        Me.BX5.ForeColor = System.Drawing.Color.DimGray
        Me.BX5.Location = New System.Drawing.Point(719, 265)
        Me.BX5.Name = "BX5"
        Me.BX5.Size = New System.Drawing.Size(14, 21)
        Me.BX5.TabIndex = 89
        Me.BX5.Text = "X"
        Me.BX5.UseVisualStyleBackColor = True
        '
        'BX6
        '
        Me.BX6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BX6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BX6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BX6.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Bold)
        Me.BX6.ForeColor = System.Drawing.Color.DimGray
        Me.BX6.Location = New System.Drawing.Point(719, 291)
        Me.BX6.Name = "BX6"
        Me.BX6.Size = New System.Drawing.Size(14, 21)
        Me.BX6.TabIndex = 89
        Me.BX6.Text = "X"
        Me.BX6.UseVisualStyleBackColor = True
        '
        'BX7
        '
        Me.BX7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BX7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BX7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BX7.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Bold)
        Me.BX7.ForeColor = System.Drawing.Color.DimGray
        Me.BX7.Location = New System.Drawing.Point(719, 317)
        Me.BX7.Name = "BX7"
        Me.BX7.Size = New System.Drawing.Size(14, 21)
        Me.BX7.TabIndex = 89
        Me.BX7.Text = "X"
        Me.BX7.UseVisualStyleBackColor = True
        '
        'CBKRef
        '
        Me.CBKRef.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CBKRef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBKRef.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.CBKRef.FormattingEnabled = True
        Me.CBKRef.Location = New System.Drawing.Point(92, 60)
        Me.CBKRef.Name = "CBKRef"
        Me.CBKRef.Size = New System.Drawing.Size(100, 22)
        Me.CBKRef.TabIndex = 91
        '
        'Label28
        '
        Me.Label28.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Location = New System.Drawing.Point(6, 64)
        Me.Label28.Margin = New System.Windows.Forms.Padding(3)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(80, 13)
        Me.Label28.TabIndex = 90
        Me.Label28.Text = "No. Ken. Ref."
        '
        'Label29
        '
        Me.Label29.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Location = New System.Drawing.Point(6, 13)
        Me.Label29.Margin = New System.Windows.Forms.Padding(3)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(80, 13)
        Me.Label29.TabIndex = 7
        Me.Label29.Text = "Th. Pendataan"
        '
        'CBTahunPendataan
        '
        Me.CBTahunPendataan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CBTahunPendataan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBTahunPendataan.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.CBTahunPendataan.FormattingEnabled = True
        Me.CBTahunPendataan.Location = New System.Drawing.Point(92, 8)
        Me.CBTahunPendataan.Name = "CBTahunPendataan"
        Me.CBTahunPendataan.Size = New System.Drawing.Size(100, 22)
        Me.CBTahunPendataan.TabIndex = 75
        '
        'FUtama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(740, 476)
        Me.Controls.Add(Me.CBKRef)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.BX7)
        Me.Controls.Add(Me.BX6)
        Me.Controls.Add(Me.BX5)
        Me.Controls.Add(Me.BX4)
        Me.Controls.Add(Me.BX3)
        Me.Controls.Add(Me.BX2)
        Me.Controls.Add(Me.BX1)
        Me.Controls.Add(Me.groupBox1)
        Me.Controls.Add(Me.LU7)
        Me.Controls.Add(Me.LU6)
        Me.Controls.Add(Me.LU5)
        Me.Controls.Add(Me.LU4)
        Me.Controls.Add(Me.LU3)
        Me.Controls.Add(Me.LU2)
        Me.Controls.Add(Me.LU1)
        Me.Controls.Add(Me.TP2)
        Me.Controls.Add(Me.TP1)
        Me.Controls.Add(Me.LLeng)
        Me.Controls.Add(Me.T75)
        Me.Controls.Add(Me.T74)
        Me.Controls.Add(Me.T65)
        Me.Controls.Add(Me.T64)
        Me.Controls.Add(Me.T55)
        Me.Controls.Add(Me.T54)
        Me.Controls.Add(Me.T45)
        Me.Controls.Add(Me.T44)
        Me.Controls.Add(Me.T73)
        Me.Controls.Add(Me.T35)
        Me.Controls.Add(Me.T63)
        Me.Controls.Add(Me.T34)
        Me.Controls.Add(Me.T53)
        Me.Controls.Add(Me.T25)
        Me.Controls.Add(Me.T43)
        Me.Controls.Add(Me.T72)
        Me.Controls.Add(Me.T24)
        Me.Controls.Add(Me.T62)
        Me.Controls.Add(Me.T33)
        Me.Controls.Add(Me.T52)
        Me.Controls.Add(Me.T15)
        Me.Controls.Add(Me.T42)
        Me.Controls.Add(Me.T71)
        Me.Controls.Add(Me.T23)
        Me.Controls.Add(Me.T61)
        Me.Controls.Add(Me.T32)
        Me.Controls.Add(Me.T51)
        Me.Controls.Add(Me.T14)
        Me.Controls.Add(Me.T41)
        Me.Controls.Add(Me.T22)
        Me.Controls.Add(Me.T31)
        Me.Controls.Add(Me.T13)
        Me.Controls.Add(Me.T21)
        Me.Controls.Add(Me.T12)
        Me.Controls.Add(Me.T11)
        Me.Controls.Add(Me.CBTahunPendataan)
        Me.Controls.Add(Me.CBKendali)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BFix)
        Me.Controls.Add(Me.BDataBaru)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.BDownExcel)
        Me.Controls.Add(Me.BKeluar)
        Me.Controls.Add(Me.BTambah)
        Me.Controls.Add(Me.TBKode7)
        Me.Controls.Add(Me.TBPembangunan)
        Me.Controls.Add(Me.TBTanggal7)
        Me.Controls.Add(Me.TBKode6)
        Me.Controls.Add(Me.TBTanggal6)
        Me.Controls.Add(Me.TBKode3)
        Me.Controls.Add(Me.TBTanggal3)
        Me.Controls.Add(Me.TBKode5)
        Me.Controls.Add(Me.TBTanggal5)
        Me.Controls.Add(Me.TBKode2)
        Me.Controls.Add(Me.TBTanggal2)
        Me.Controls.Add(Me.TBKode4)
        Me.Controls.Add(Me.TBTanggal4)
        Me.Controls.Add(Me.TBKode1)
        Me.Controls.Add(Me.TBTanggal1)
        Me.Controls.Add(Me.TBNik7)
        Me.Controls.Add(Me.TBNik6)
        Me.Controls.Add(Me.TBNama7)
        Me.Controls.Add(Me.TBNik3)
        Me.Controls.Add(Me.TBNama6)
        Me.Controls.Add(Me.TBNik5)
        Me.Controls.Add(Me.TBNama3)
        Me.Controls.Add(Me.TBNik2)
        Me.Controls.Add(Me.TBNama5)
        Me.Controls.Add(Me.TBNik4)
        Me.Controls.Add(Me.TBNama2)
        Me.Controls.Add(Me.TBNik1)
        Me.Controls.Add(Me.TBNama4)
        Me.Controls.Add(Me.TBNama1)
        Me.Controls.Add(Me.TBLamaBKb)
        Me.Controls.Add(Me.TBKawinIs)
        Me.Controls.Add(Me.TBTmpatKb)
        Me.Controls.Add(Me.TBKontra)
        Me.Controls.Add(Me.TBAlasanTidakKb)
        Me.Controls.Add(Me.TBKb)
        Me.Controls.Add(Me.TBPunyaAnak)
        Me.Controls.Add(Me.TBJumlahAnak)
        Me.Controls.Add(Me.TBOrang)
        Me.Controls.Add(Me.TBLuasRumah)
        Me.Controls.Add(Me.TBLamaTKb)
        Me.Controls.Add(Me.TBKawinSu)
        Me.Controls.Add(Me.TBNoKeluarga)
        Me.Controls.Add(Me.TBKendali)
        Me.Controls.Add(Me.TBNoRumah)
        Me.Controls.Add(Me.label5)
        Me.Controls.Add(Me.label4)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.label20)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.label12)
        Me.Controls.Add(Me.label11)
        Me.Controls.Add(Me.label10)
        Me.Controls.Add(Me.label9)
        Me.Controls.Add(Me.label8)
        Me.Controls.Add(Me.label7)
        Me.Controls.Add(Me.label21)
        Me.Controls.Add(Me.label16)
        Me.Controls.Add(Me.label6)
        Me.Controls.Add(Me.label19)
        Me.Controls.Add(Me.label15)
        Me.Controls.Add(Me.label18)
        Me.Controls.Add(Me.label14)
        Me.Controls.Add(Me.label22)
        Me.Controls.Add(Me.label17)
        Me.Controls.Add(Me.label13)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.Label23)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FUtama"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entri Data BKKBN"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents BKeluar As Button
    Private WithEvents groupBox1 As GroupBox
    Private WithEvents TBKode7 As TextBox
    Private WithEvents TBPembangunan As TextBox
    Private WithEvents TBTanggal7 As TextBox
    Private WithEvents TBKode6 As TextBox
    Private WithEvents TBTanggal6 As TextBox
    Private WithEvents TBKode3 As TextBox
    Private WithEvents TBTanggal3 As TextBox
    Private WithEvents TBKode5 As TextBox
    Private WithEvents TBTanggal5 As TextBox
    Private WithEvents TBKode2 As TextBox
    Private WithEvents TBTanggal2 As TextBox
    Private WithEvents TBKode4 As TextBox
    Private WithEvents TBTanggal4 As TextBox
    Private WithEvents TBKode1 As TextBox
    Private WithEvents TBTanggal1 As TextBox
    Private WithEvents TBNama7 As TextBox
    Private WithEvents TBNama6 As TextBox
    Private WithEvents TBNama3 As TextBox
    Private WithEvents TBNama5 As TextBox
    Private WithEvents TBNama2 As TextBox
    Private WithEvents TBNama4 As TextBox
    Private WithEvents TBNama1 As TextBox
    Private WithEvents TBLamaBKb As TextBox
    Private WithEvents TBKawinIs As TextBox
    Private WithEvents TBTmpatKb As TextBox
    Private WithEvents TBKontra As TextBox
    Private WithEvents TBAlasanTidakKb As TextBox
    Private WithEvents TBKb As TextBox
    Private WithEvents TBPunyaAnak As TextBox
    Private WithEvents TBJumlahAnak As TextBox
    Private WithEvents TBLamaTKb As TextBox
    Private WithEvents TBKawinSu As TextBox
    Private WithEvents TBNoKeluarga As TextBox
    Private WithEvents TBNoRumah As TextBox
    Private WithEvents label5 As Label
    Private WithEvents label4 As Label
    Private WithEvents label20 As Label
    Private WithEvents label3 As Label
    Private WithEvents label12 As Label
    Private WithEvents label11 As Label
    Private WithEvents label10 As Label
    Private WithEvents label9 As Label
    Private WithEvents label8 As Label
    Private WithEvents label7 As Label
    Private WithEvents label21 As Label
    Private WithEvents label16 As Label
    Private WithEvents label6 As Label
    Private WithEvents label19 As Label
    Private WithEvents label15 As Label
    Private WithEvents label18 As Label
    Private WithEvents label14 As Label
    Private WithEvents label22 As Label
    Private WithEvents label17 As Label
    Private WithEvents label13 As Label
    Private WithEvents label1 As Label
    Private WithEvents label2 As Label
    Private WithEvents TBLuasRumah As TextBox
    Private WithEvents TBOrang As TextBox
    Private WithEvents Label23 As Label
    Private WithEvents Label24 As Label
    Private WithEvents TBKendali As TextBox
    Public WithEvents BDownExcel As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents LInfo As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Private WithEvents TBNik1 As TextBox
    Private WithEvents TBNik4 As TextBox
    Private WithEvents TBNik2 As TextBox
    Private WithEvents TBNik5 As TextBox
    Private WithEvents TBNik3 As TextBox
    Private WithEvents TBNik6 As TextBox
    Private WithEvents TBNik7 As TextBox
    Private WithEvents Label25 As Label
    Public WithEvents BTambah As Button
    Public WithEvents BDataBaru As Button
    Private WithEvents Label26 As Label
    Friend WithEvents Timer2 As Timer
    Friend WithEvents BFix As Button
    Private WithEvents Label27 As Label
    Friend WithEvents TUpdate As Timer
    Friend WithEvents TTimeOut As Timer
    Friend WithEvents TRefresh As Timer
    Friend WithEvents Button1 As Button
    Friend WithEvents CBKendali As ComboBox
    Friend WithEvents T11 As TextBox
    Friend WithEvents T12 As TextBox
    Friend WithEvents T13 As TextBox
    Friend WithEvents T14 As TextBox
    Friend WithEvents T15 As TextBox
    Friend WithEvents T21 As TextBox
    Friend WithEvents T22 As TextBox
    Friend WithEvents T23 As TextBox
    Friend WithEvents T24 As TextBox
    Friend WithEvents T25 As TextBox
    Friend WithEvents T31 As TextBox
    Friend WithEvents T32 As TextBox
    Friend WithEvents T33 As TextBox
    Friend WithEvents T34 As TextBox
    Friend WithEvents T35 As TextBox
    Friend WithEvents T41 As TextBox
    Friend WithEvents T42 As TextBox
    Friend WithEvents T43 As TextBox
    Friend WithEvents T44 As TextBox
    Friend WithEvents T45 As TextBox
    Friend WithEvents T51 As TextBox
    Friend WithEvents T52 As TextBox
    Friend WithEvents T53 As TextBox
    Friend WithEvents T54 As TextBox
    Friend WithEvents T55 As TextBox
    Friend WithEvents T61 As TextBox
    Friend WithEvents T62 As TextBox
    Friend WithEvents T63 As TextBox
    Friend WithEvents T64 As TextBox
    Friend WithEvents T65 As TextBox
    Friend WithEvents T71 As TextBox
    Friend WithEvents T72 As TextBox
    Friend WithEvents T73 As TextBox
    Friend WithEvents T74 As TextBox
    Friend WithEvents T75 As TextBox
    Friend WithEvents LLeng As Label
    Friend WithEvents Timer3 As Timer
    Private WithEvents TP1 As TextBox
    Private WithEvents TP2 As TextBox
    Friend WithEvents LU1 As Label
    Friend WithEvents LU2 As Label
    Friend WithEvents LU3 As Label
    Friend WithEvents LU4 As Label
    Friend WithEvents LU5 As Label
    Friend WithEvents LU6 As Label
    Friend WithEvents LU7 As Label
    Friend WithEvents BX1 As Button
    Friend WithEvents BX2 As Button
    Friend WithEvents BX3 As Button
    Friend WithEvents BX4 As Button
    Friend WithEvents BX5 As Button
    Friend WithEvents BX6 As Button
    Friend WithEvents BX7 As Button
    Friend WithEvents CBKRef As ComboBox
    Private WithEvents Label28 As Label
    Private WithEvents Label29 As Label
    Friend WithEvents CBTahunPendataan As ComboBox
End Class
