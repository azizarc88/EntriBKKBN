﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CBKendali = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BBatal = New System.Windows.Forms.Button()
        Me.BLaksanakan = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CBKendali
        '
        Me.CBKendali.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CBKendali.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBKendali.FormattingEnabled = True
        Me.CBKendali.Location = New System.Drawing.Point(155, 12)
        Me.CBKendali.Name = "CBKendali"
        Me.CBKendali.Size = New System.Drawing.Size(121, 21)
        Me.CBKendali.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(133, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Mulai input dari no kendali:"
        '
        'BBatal
        '
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(12, 43)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(75, 23)
        Me.BBatal.TabIndex = 2
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = True
        '
        'BLaksanakan
        '
        Me.BLaksanakan.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BLaksanakan.Location = New System.Drawing.Point(201, 43)
        Me.BLaksanakan.Name = "BLaksanakan"
        Me.BLaksanakan.Size = New System.Drawing.Size(75, 23)
        Me.BLaksanakan.TabIndex = 2
        Me.BLaksanakan.Text = "Laksanakan"
        Me.BLaksanakan.UseVisualStyleBackColor = True
        '
        'Form2
        '
        Me.AcceptButton = Me.BLaksanakan
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BBatal
        Me.ClientSize = New System.Drawing.Size(288, 77)
        Me.ControlBox = False
        Me.Controls.Add(Me.BLaksanakan)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CBKendali)
        Me.Name = "Form2"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Masukkan ke Excel"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CBKendali As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents BBatal As Button
    Friend WithEvents BLaksanakan As Button
End Class
