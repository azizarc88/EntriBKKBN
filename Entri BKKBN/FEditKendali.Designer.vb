﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FEditKendali
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LBKendali = New System.Windows.Forms.ListBox()
        Me.TBKendali = New System.Windows.Forms.TextBox()
        Me.LBHasil = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BUbah = New System.Windows.Forms.Button()
        Me.BBatal = New System.Windows.Forms.Button()
        Me.BSimpan = New System.Windows.Forms.Button()
        Me.TBUbahKendali = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LBKendali
        '
        Me.LBKendali.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.LBKendali.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LBKendali.FormattingEnabled = True
        Me.LBKendali.ItemHeight = 14
        Me.LBKendali.Location = New System.Drawing.Point(12, 26)
        Me.LBKendali.Name = "LBKendali"
        Me.LBKendali.Size = New System.Drawing.Size(120, 200)
        Me.LBKendali.TabIndex = 0
        '
        'TBKendali
        '
        Me.TBKendali.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TBKendali.Location = New System.Drawing.Point(12, 233)
        Me.TBKendali.Name = "TBKendali"
        Me.TBKendali.ReadOnly = True
        Me.TBKendali.Size = New System.Drawing.Size(110, 20)
        Me.TBKendali.TabIndex = 1
        '
        'LBHasil
        '
        Me.LBHasil.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.LBHasil.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.LBHasil.FormattingEnabled = True
        Me.LBHasil.ItemHeight = 14
        Me.LBHasil.Location = New System.Drawing.Point(138, 26)
        Me.LBHasil.Name = "LBHasil"
        Me.LBHasil.Size = New System.Drawing.Size(120, 200)
        Me.LBHasil.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Data"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(135, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Hasil Perubahan"
        '
        'BUbah
        '
        Me.BUbah.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BUbah.Location = New System.Drawing.Point(15, 259)
        Me.BUbah.Name = "BUbah"
        Me.BUbah.Size = New System.Drawing.Size(243, 23)
        Me.BUbah.TabIndex = 3
        Me.BUbah.Text = "Ubah"
        Me.BUbah.UseVisualStyleBackColor = True
        '
        'BBatal
        '
        Me.BBatal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BBatal.Location = New System.Drawing.Point(138, 289)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(120, 27)
        Me.BBatal.TabIndex = 3
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = True
        '
        'BSimpan
        '
        Me.BSimpan.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BSimpan.Location = New System.Drawing.Point(15, 289)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Size = New System.Drawing.Size(120, 27)
        Me.BSimpan.TabIndex = 3
        Me.BSimpan.Text = "Simpan"
        Me.BSimpan.UseVisualStyleBackColor = True
        '
        'TBUbahKendali
        '
        Me.TBUbahKendali.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TBUbahKendali.Location = New System.Drawing.Point(148, 233)
        Me.TBUbahKendali.MaxLength = 8
        Me.TBUbahKendali.Name = "TBUbahKendali"
        Me.TBUbahKendali.Size = New System.Drawing.Size(110, 20)
        Me.TBUbahKendali.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(125, 236)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(19, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "-->"
        '
        'FEditKendali
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(270, 328)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.BSimpan)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BUbah)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TBUbahKendali)
        Me.Controls.Add(Me.TBKendali)
        Me.Controls.Add(Me.LBHasil)
        Me.Controls.Add(Me.LBKendali)
        Me.Name = "FEditKendali"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit No Kendali"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LBKendali As ListBox
    Friend WithEvents TBKendali As TextBox
    Friend WithEvents LBHasil As ListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents BUbah As Button
    Friend WithEvents BBatal As Button
    Friend WithEvents BSimpan As Button
    Friend WithEvents TBUbahKendali As TextBox
    Friend WithEvents Label3 As Label
End Class
